#  To Do:

## P0


## P1
- Add generic fetch method to MMBTableViewDatasource
- Add pagination support to MMBTableViewDatasource
- Options - User textFields are editable - Shouldn't be until saving changes possible
- Implement refresh after successfull post && pull to refresh
- Make AddTransactionFlowBank DatePickers UITextFields with toolbar & datepicker as input views
- Filter data in AddTransactionFlow to only show applicable for selections
- Add Description to AddTransactionFlowDetials
- AddTransactionFlow - Switch to TableView with selectable options instead of StackedLabelFieldView 
- Get rid of IntOrString
- MMBTextFieldBorder cuts off long text on sides, Stop that.
- Launch Screen
- Sign Up/Sign In Landing
- Fix "User" implementation
- Subclass UILabel to add padding (not border to UILabels, for OptionsVC at least)
- Highlight/Change color/border Log In button on tap
- LoginVC still moves to Vehicle List if return does not contain vehicles
- Show appropriate Alert when Log In Auth fails (Easily testable by not starting Docker..)
- Accounts View could probably be a Collection View
- Pagination on all necessary screens
- Options/Settings
- Deal with input of non-required fields in AddVehicleVC

Pagination:
Looks like MMBClient is just setting page in Query String as +1 when next page button pressed, but disabling the button if ```this.state._page === parseInt(this.props.total / PAGE_SIZE) + 1``` (getPaginationMarkup() - MMBTable.js) 
