//
//  MMBNavButton.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 7/4/21.
//  Copyright © 2021 Dave Lambe. All rights reserved.
//

import UIKit

class MMBNavButton: UIView {
    private static let labelTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                                              NSAttributedString.Key.foregroundColor: MyMotorBooksColor.pink.rawValue]
    public private(set) var direction: NavDirection
    enum NavDirection {
        case back
        case forward
    }

    private let buttonImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.tintColor = MyMotorBooksColor.pink.rawValue
        iv.backgroundColor = MyMotorBooksColor.black.rawValue
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    private let buttonLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.backgroundColor = MyMotorBooksColor.black.rawValue
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(buttonDirection: NavDirection) {
        self.direction = buttonDirection
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(buttonImage)
        self.addSubview(buttonLabel)
        configureImage()
        configureLabel()
        configureViews()
    }

    private func configureImage() {
        if let directionImage: UIImage = self.direction == .back ? UIImage(systemName: "chevron.left") : UIImage(systemName: "chevron.right") {
            self.buttonImage.image = directionImage
        }
    }

    private func configureLabel() {
        let directionText: String = self.direction == .back ? "Back" : "Forward"
        setText(directionText)
    }

    public func setText(_ text: String) {
        self.buttonLabel.text = text
        self.buttonLabel.attributedText = text.asAttributedText(withAttributes: MMBNavButton.labelTextAttributes)
    }

    private func configureViews() {
        let leftView = (self.direction == .back) ? buttonImage : buttonLabel
        let rightView = (self.direction == .back) ? buttonLabel : buttonImage

        NSLayoutConstraint.activate([
            leftView.topAnchor.constraint(equalTo: self.topAnchor),
            leftView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            leftView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 5),

            rightView.topAnchor.constraint(equalTo: leftView.topAnchor),
            rightView.bottomAnchor.constraint(equalTo: leftView.bottomAnchor),
            rightView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor, constant: 8),
            rightView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -5),
        ])
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureViews()
    }
}
