//
//  MMBPickerTextField.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/25/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class MMBPickerTextField: MMBTextField {
    public private(set) var pickerView: UIPickerView
    public var attribute: DescribedAttribute?

    override init(placeholderText text: String, isDrillable: Bool = true, isRequiredField: Bool = false) {
        self.pickerView = UIPickerView()
        self.pickerView.backgroundColor = MyMotorBooksColor.pink.rawValue
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        super.init(placeholderText: text, isDrillable: isDrillable, isRequiredField: isRequiredField)
        self.inputView = pickerView
//        self.required = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
