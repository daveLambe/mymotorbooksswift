//
//  MMBCheckbox.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 2/17/21.
//  Copyright © 2021 Dave Lambe. All rights reserved.
//

import UIKit

final class MMBCheckbox: UIButton {
    public var isChecked: Bool = false {
        didSet {
            setResizedImage()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = MyMotorBooksColor.pink.rawValue
        clipsToBounds = true
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 4


        setResizedImage()
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setResizedImage() {
        let buttonImage = self.isChecked ? UIImage(systemName: "checkmark.square") : UIImage(systemName: "square")
        guard let originalImage = buttonImage, let resizedImage = originalImage.resizedImage(size: self.frame.size) else {
            print("<<<< Failed to get current image and resize image of MMBCheckbox >>>>")
            return
        }

        let tintableImage = resizedImage.withRenderingMode(.alwaysTemplate)
        self.imageView?.tintColor = UIColor.white

        self.setImage(tintableImage, for: .normal)

        layoutSubviews()
        layoutIfNeeded()
    }
}
