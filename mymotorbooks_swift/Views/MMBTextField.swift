//  MMBTextField.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 8/22/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

protocol MMBToolbarDelegate: AnyObject {
    func doneTapped()
    func cancelTapped()
}

class MMBTextField: UITextField {
    public weak var toolbarDelegate: MMBToolbarDelegate?
    private static let textAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                                         NSAttributedString.Key.foregroundColor: UIColor.white]
    var placeholderText: String
    var isDrillable: Bool
    var selectedOption: String?
    var required: Bool = false

    init(placeholderText text: String, isDrillable: Bool = false, isRequiredField: Bool = false) {
        self.placeholderText = text
        self.isDrillable = isDrillable
        self.required = isRequiredField
        super.init(frame: .zero)
        initCommon()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initCommon() {
        self.placeholder = placeholderText
        self.backgroundColor = MyMotorBooksColor.pink.rawValue
        self.attributedPlaceholder = placeholderText.asAttributedText(withAttributes: MMBTextField.textAttributes)
        self.styleAsMMBField(isDrillable: isDrillable)
        defaultTextAttributes = MyMotorBooksTextAttributes.textFieldTextAttributes
        adjustsFontSizeToFitWidth = true

        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35))
        toolbar.barStyle = .black
        toolbar.barTintColor = MyMotorBooksColor.pink.rawValue
        toolbar.tintColor = UIColor.white
        toolbar.isTranslucent = true

        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.didTapDone))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.didTapCancel))
        toolbar.setItems([cancelButton, flexibleSpace, doneButton], animated: false)
        toolbar.sizeToFit()
        toolbar.isUserInteractionEnabled = true
        self.inputAccessoryView = toolbar
    }

    public func setTextFieldText(_ text: String) {

    }

    public func setSelectedOption(_ selectedOption: String?) {
        self.selectedOption = selectedOption
    }

    public func disableKeyboardInput(cursorColor: UIColor) {
        self.inputView = UIView()
        self.inputAccessoryView = UIView()
        // cursorColor must be provided so so flashing cursor appears in field
        self.tintColor = cursorColor

    }

    @objc func didTapDone() {
        self.toolbarDelegate?.doneTapped()
    }

    @objc func didTapCancel() {
        self.toolbarDelegate?.cancelTapped()
    }
}
