//
//  StackedLabelFieldView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/8/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class StackedLabelFieldView: UIView, StackedLabelFields {

    var labels: [UILabel]
    var fields: [MMBTextField]

    let labelStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .equalSpacing
        sv.spacing = 15
        sv.clipsToBounds = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let fieldStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .equalSpacing
        sv.spacing = 15
        sv.clipsToBounds = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    init(labels: [UILabel], fields: [MMBTextField]) {
        self.labels = labels
        self.fields = fields
        super.init(frame: .zero)

        labels.forEach {
            labelStack.addArrangedSubview($0)
        }
        fields.forEach {
            fieldStack.addArrangedSubview($0)
        }

        self.addSubview(labelStack)
        self.addSubview(fieldStack)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
