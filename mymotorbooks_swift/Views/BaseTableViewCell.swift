//
//  BaseTableViewCell.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

protocol Searchable {
    var query: String { get }
}

protocol Reusable {}

extension Reusable where Self: UITableViewCell {
    static var reuseIdentifier: String { return String(describing: self) }
}

extension UITableViewCell: Reusable {}

class BaseTableViewCell<V>: UITableViewCell {
    var item: V!
}
