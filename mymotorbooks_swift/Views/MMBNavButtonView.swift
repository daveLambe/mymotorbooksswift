//
//  MMBNavButtonVIew.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 7/3/21.
//  Copyright © 2021 Dave Lambe. All rights reserved.
//

import UIKit

final class MMBNavButtonView: UIView {
    public let backButton = MMBNavButton(buttonDirection: .back)
    public let forwardButton = MMBNavButton(buttonDirection: .forward)

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: .zero)
        self.addSubview(backButton)
        self.addSubview(forwardButton)

        self.backgroundColor = MyMotorBooksColor.black.rawValue
        self.setBorder(width: 4, color: MyMotorBooksColor.pink.rawValue)
        configureViews()
    }

    public func setButton(buttonDirection: MMBNavButton.NavDirection, text: String) {
        let button = buttonDirection == .back ? backButton : forwardButton
        button.setText(text)
    }

    public func hideButton(buttonDirection: MMBNavButton.NavDirection, hide: Bool) {
        let button = buttonDirection == .back ? backButton : forwardButton
        button.isHidden = hide
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureViews()
    }

    private func configureViews() {
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            backButton.topAnchor.constraint(equalTo: self.topAnchor),
            backButton.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            forwardButton.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            forwardButton.topAnchor.constraint(equalTo: backButton.topAnchor),
            forwardButton.bottomAnchor.constraint(equalTo: backButton.bottomAnchor),
        ])
    }
}
