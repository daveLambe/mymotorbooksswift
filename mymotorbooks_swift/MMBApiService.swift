//
//  MMBApiService.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 9/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

protocol MMBAPIResponse: Decodable {
    associatedtype T: Decodable

    var results: [T] { get }
    var count: Int { get }
    var next: String? { get }
    var previous: String? { get }
}

struct MMBFetchResponse<T: Decodable>: Decodable {
    var results: [T]
    let count: Int
    let next: String?
    let previous: String?
}

class MMBApiService {

    public enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
    }

    public enum MMBApiServiceError: Error {
        case apiError(detail: String)
        case invalidEndpoint
        case invalidResponse(detail: String)
        case noData
        case decodeError
        case encodeError
        case unauthorized(detail: String)
        case internalError(detail: String)

        public var description: String {
            return "Error: " + String(describing: self)
        }
    }

    enum Endpoint: String, CustomStringConvertible, CaseIterable {
        case auth = "auth/jwt/"
        case accounts = "accounts/"
        case transactions = "accounts/transactions/"
        case users = "users/"
        case vehicles = "vehicles/"

        public var description: String { return rawValue }
    }

    enum AuthEndpoint: String {
        case create = "create/"
        case refresh = "refresh/"
    }

    enum VehiclesEndpoint: String {
        case stats = "stats/"
        case makes = "makes/"
        case models = "models/"
        case purchase = "purchase/"
    }

    enum TransactionsEndpoint {
//        case getTransactionList
        case getTransactionDetail(Int)
        case vatRates
        case transactionsTypes
        case createTransaction
        case updateTransaction(Int)

        var rawValue: String {
            switch self {
//            case .getTransactionList: return ""
            case .getTransactionDetail(let pk): return "\(pk)/"
            case .vatRates: return "VATRates/"
            case .transactionsTypes: return "types/"
            case .createTransaction: return ""
            case .updateTransaction(let pk): return "\(pk)/"
            }
        }
    }

    enum AccountsEndpoint {
        case pk(Int)
        case balance(Int)
        case lineItems
        case VATReturn
        case exportPurchases
        case exportSales
        case documents
        case documentsPk(Int)
        case documentsDownload(Int)
        case documentsEmail(Int)
        case traders
        case tradersPk(Int)
        case transactions
        case transactionsPk(Int)
        case transactionsVATRates
        case transactionsTypes
        case employees
        case employeesPk(Int)
        case employeesPayWages(Int)

        var rawValue: String {
            switch self {
            case .pk(let pk): return "\(pk)/"
            case .balance(let pk): return "\(pk)/balance/"
            case .lineItems: return "lineItems/"
            case .VATReturn: return "VATReturn/"
            case .exportPurchases: return "exportPurchases/"
            case .exportSales: return "exportSales/"
            case .documents: return "documents/"
            case .documentsPk(let pk): return "documents/\(pk)/"
            case .documentsDownload(let pk): return "documents/\(pk)/download/"
            case .documentsEmail(let pk): return "documents/\(pk)/email/"
            case .traders: return "traders/"
            case .tradersPk(let pk): return "traders/\(pk)"
            case .transactions: return "transactions/"
            case .transactionsPk(let pk): return "transactions/\(pk)/"
            case .transactionsVATRates: return "transactions/VATRates/"
            case .transactionsTypes: return "transactions/types/"
            case .employees: return "employees/"
            case .employeesPk(let pk): return "employees/\(pk)"
            case .employeesPayWages(let pk): return "employees/payWages/\(pk)/"
            }
        }
    }

    enum UsersEndpoint: String {
        case contactRequest = "contactRequest/"
    }

    public static let shared = MMBApiService()
    public let usingProduction: Bool!
    private let debug: Bool!

    private init() {
        self.usingProduction = self.baseURL == URL(string: MyMotorBooksBaseURL.production.rawValue)!
        self.debug = false
        setRetryCount(to: 3)
        print("<<<< Using Production: \(String(describing: usingProduction)) >>>>")
        print("<<<< Debug: \(String(describing: debug)) >>>>")
    }
    private let urlSession = URLSession.shared
    private let testURL = URL(string: MyMotorBooksBaseURL.develop.rawValue)!
    public let baseURL = URL(string: MyMotorBooksBaseURL.production.rawValue)!
//    public let baseURL = URL(string: MyMotorBooksBaseURL.develop.rawValue)!

    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .customISO8601
        return jsonDecoder
    }()

    private let fromSnakeCaseDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .customISO8601
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }()

    private let jsonEncoder: JSONEncoder = {
        let jsonEncoder = JSONEncoder()
        return jsonEncoder
    }()

    private let toSnakeCaseEncoder: JSONEncoder = {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
        return jsonEncoder
    }()

    private func setRetryCount(to count: Int) {
        AuthManager.shared.setAuthRetryCount(count)
    }

    public func fetch<T: Decodable>(url: URL, page: Int? = nil, decoder: JSONDecoder = MMBApiService.shared.fromSnakeCaseDecoder, completion: @escaping (Result<T, MMBApiServiceError>) -> Void) {
        guard let request = page != nil ? url.asAuthedGetRequest(forPage: page!) : url.asAuthedGetRequest() else {
            completion(.failure(.internalError(detail: "<<<< Failed to create Get Request (Likely accessToken not found) >>>>")))
            return
        }

        if self.debug {
            print("<<<< Request: \(request.description) >>>>")
        }

        urlSession.dataTask(with: request) { (result) in
            OperationQueue.main.addOperation {
                switch result {
                case .success(let (response, data)):
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    guard 200..<299 ~= statusCode else {
                        if statusCode == 401 {
                            guard let retryCount = AuthManager.shared.getAuthRetryCount(), retryCount > 0 else {
                                completion(.failure(.internalError(detail: "<<<< Retry Count has not been set or attempts are depleted. Will not attempt Re-Auth. >>>>")))
                                AuthManager.shared.logOut()
                                return
                            }
                            print("<<<< Unauthorized. Status code: \(statusCode). Attempting to Re-Auth >>>>")
                            MMBApiService.shared.refresh() { [weak self] (authResult) in
                                switch authResult {
                                case .success(let tokens):
                                    AuthManager.shared.storeAccessToken(tokens.access)

                                    print("<<<< Successfully Re-Authed. Retrying Request >>>>")
                                    self?.fetch(url: url, completion: completion)
                                case .failure(let error):
                                    print("<<<< Failed to re-auth with error: \(error.localizedDescription) >>>>")
                                    print("<<<< Setting retryCount to: \(retryCount - 1) >>>>")
                                    self?.setRetryCount(to: retryCount - 1)
                                }
                            }
                        }
                        completion(.failure(.invalidResponse(detail: "Invalid status code: \(statusCode)")))
                        return
                    }
                    do {
                        if self.debug {
                            print("<<<< Response: \(data.prettyPrintedJSONString ?? String(describing: data) as NSString) >>>>")
                        }

                        let values = try decoder.decode(T.self, from: data)
                        completion(.success(values))
                    } catch {
                        completion(.failure(.decodeError))
                    }
                case .failure(let error):
                    completion(.failure(.apiError(detail: "<<<< Data Task Failed with error: \(error.localizedDescription) >>>>")))
                }
            }
        }.resume()
    }

    private func post<T: Decodable, U: Encodable>(url: URL, parameters: U, signInRequired: Bool = false, encoder: JSONEncoder = MMBApiService.shared.toSnakeCaseEncoder, decoder: JSONDecoder = MMBApiService.shared.fromSnakeCaseDecoder, completion: @escaping (Result<T, MMBApiServiceError>) -> Void) {
        guard let request = signInRequired ? url.asPostRequest(parameters: parameters) : url.asAuthedPostRequest(parameters: parameters, jsonEncoder: encoder) else {
            completion(.failure(.internalError(detail: "<<<< Failed to create Post Request >>>>")))
            return
        }

        if self.debug {
            print("<<<< Request: \(request.description) >>>>")
        }

        urlSession.dataTask(with: request) { (result) in
            OperationQueue.main.addOperation {
                switch result {
                case .success(let (response, data)):
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    guard 200..<299 ~= statusCode else {
                        print("<<<< Unexpected status code: \(statusCode) >>>>")
                        if statusCode == 401 && signInRequired == false {
                            guard let retryCount = AuthManager.shared.getAuthRetryCount(), retryCount > 0 else {
                                completion(.failure(.internalError(detail: "<<<< Retry Count has not been set or attempts are depleted. Will not attempt Re-Auth. >>>>")))
                                AuthManager.shared.logOut()
                                return
                            }
                            print("<<<< Unauthorized. Status code: \(statusCode). Attempting to Re-Auth >>>>")
                            MMBApiService.shared.refresh() { [weak self] (authResult) in
                                switch authResult {
                                case .success(let tokens):
                                    print("<<<< Successfully Re-Authed. Retrying Request >>>>")
                                    AuthManager.shared.storeAccessToken(tokens.access)

                                    self?.post(url: url, parameters: parameters, completion: completion)
                                case .failure(let error):
                                    print("<<<< Failed to re-auth with error: \(error.localizedDescription) >>>>")
                                    print("<<<< Setting retryCount to: \(retryCount - 1) >>>>")
                                    self?.setRetryCount(to: retryCount - 1)
                                }
                            }
                        }
                        completion(.failure(.invalidResponse(detail: "Invalid status code: \(statusCode)")))
                        return
                    }
                    do {
                        if self.debug {
                            print("<<<< Response: \(data.prettyPrintedJSONString ?? String(describing: data) as NSString) >>>>")
                        }

                        let values = try decoder.decode(T.self, from: data)
                        completion(.success(values))
                    } catch {
                        completion(.failure(.decodeError))
                    }
                case .failure(let error):
                    completion(.failure(.apiError(detail: "<<<< Data Task Failed with error: \(error.localizedDescription) >>>>")))

                }
            }
        }.resume()
    }

    // MARK: - Auth
    public func auth(credentials: AuthManager.Credentials, result: @escaping (Result<AuthManager.Tokens, MMBApiServiceError>) -> Void) {
        let authURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.auth.rawValue)
            .appendingPathComponent(AuthEndpoint.create.rawValue)
        post(url: authURL, parameters: credentials, signInRequired: true, completion: result)
    }

    public func refresh(result: @escaping (Result<AuthManager.Tokens, MMBApiServiceError>) -> Void) {
        guard let refreshToken = AuthManager.shared.getRefreshToken() else {
            print("<<<< Failed to retrieve refresh token >>>>")
            return
        }
        let refreshURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.auth.rawValue)
            .appendingPathComponent(MMBApiService.AuthEndpoint.refresh.rawValue)
        post(url: refreshURL, parameters: ["refresh": refreshToken], completion: result)
    }

    // MARK: - Vehicles
    public func fetchVehicles(result: @escaping (Result<Vehicles, MMBApiServiceError>) -> Void) {
        let vehiclesURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
        fetch(url: vehiclesURL, completion: result)
    }

    public func fetchVehicles(result: @escaping (Result<[Vehicle], MMBApiServiceError>) -> Void) {
        let vehiclesURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
        fetch(url: vehiclesURL, completion: result)
    }

    public func fetchVehiclesPage(page: Int, result: @escaping (Result<Vehicles, MMBApiServiceError>) -> Void) {
        let vehiclesURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
        fetch(url: vehiclesURL, page: page, completion: result)
    }

    public func fetchVehicleStats(result: @escaping (Result<VehicleStats, MMBApiServiceError>) -> Void) {
        let statsURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
            .appendingPathComponent(VehiclesEndpoint.stats.rawValue)
        fetch(url: statsURL, completion: result)
    }

    public func fetchVehicleMakes(result: @escaping (Result<[VehicleMake], MMBApiServiceError>) -> Void) {
        let makesURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
            .appendingPathComponent(VehiclesEndpoint.makes.rawValue)
        fetch(url: makesURL, completion: result)
    }

    public func fetchVehicleModels(vehicleMake: VehicleMake, result: @escaping (Result<[VehicleModel], MMBApiServiceError>) -> Void) {
        let modelsURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
            .appendingPathComponent(VehiclesEndpoint.models.rawValue)
            .appendingPathComponent("\(vehicleMake.pk)/")
        fetch(url: modelsURL, completion: result)
    }

    public func purchaseVehicle(vehicleData: VehicleData, result: @escaping (Result<VehicleUpdateResponse, MMBApiServiceError>) -> Void) {
        let purchaseURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.vehicles.rawValue)
            .appendingPathComponent(VehiclesEndpoint.purchase.rawValue)
        post(url: purchaseURL, parameters: ["vehicle": vehicleData], completion: result)
    }

    // MARK: - Transactions
    public func fetchTransactions(result: @escaping (Result<Transactions, MMBApiServiceError>) -> Void) {
        let transactionsURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.transactions.rawValue)
        fetch(url: transactionsURL, completion: result)
    }

    public func fetchVATRates(result: @escaping (Result<[VatRate], MMBApiServiceError>) -> Void) {
        let vatRatesURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.transactions.rawValue).appendingPathComponent(MMBApiService.TransactionsEndpoint.vatRates.rawValue)
        fetch(url: vatRatesURL, completion: result)
    }

    public func createTransaction(transactionData: TransactionPost, result: @escaping (Result<TransactionData, MMBApiServiceError>) -> Void) {
        let createTransactionURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.transactions.rawValue)

//        print("<<<< About to send transactionData: \(transactionData) >>>>")

        post(url: createTransactionURL, parameters: transactionData, encoder: MMBApiService.shared.jsonEncoder, decoder: MMBApiService.shared.jsonDecoder, completion: result)

    }

    // MARK: - Accounts
    public func fetchAccounts(result: @escaping (Result<Accounts, MMBApiServiceError>) -> Void) {
        let accountsURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.accounts.rawValue)
        fetch(url: accountsURL, completion: result)
    }

    public func fetchTraders(result: @escaping (Result<Traders, MMBApiServiceError>) -> Void) {
        let tradersURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.accounts.rawValue)
            .appendingPathComponent(MMBApiService.AccountsEndpoint.traders.rawValue)
        fetch(url: tradersURL, completion: result)
    }

    public func fetchDocuments(result: @escaping (Result<Documents, MMBApiServiceError>) -> Void) {
        let documentsURL = baseURL
            .appendingPathComponent(MMBApiService.Endpoint.accounts.rawValue)
            .appendingPathComponent(MMBApiService.AccountsEndpoint.documents.rawValue)
        fetch(url: documentsURL, completion: result)
    }

    // MARK: - Users
    public func fetchUser(result: @escaping (Result<User, MMBApiServiceError>) -> Void) {
        let usersURL = baseURL.appendingPathComponent(MMBApiService.Endpoint.users.rawValue)
        fetch(url: usersURL, completion: result)
    }
}

extension URLSession {
    func dataTask(with url: URL, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionTask {
        let request = URLRequest(url)
        return dataTask(with: request) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }

            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }

    func dataTask(with request: URLRequest, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionTask {
        return dataTask(with: request) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }

            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }
}

extension URL {
    func asAuthedGetRequest() -> URLRequest? {
        guard let primedURL = self.primedURL() else {
            print("<<<< Failed to create primed HTTP URL >>>>")
            return nil
        }

        var request = URLRequest(url: primedURL)
        request.httpMethod = MMBApiService.HTTPMethod.get.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        guard let accessToken = AuthManager.shared.getAccessToken() else {
            print("<<<< Failed to retrieve access token >>>>")
            return nil
        }
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        return request
    }

    func asAuthedGetRequest(forPage page: Int) -> URLRequest? {
        guard var primedURL = self.primedURL() else {
            print("<<<< Failed to create primed HTTP URL >>>>")
            return nil
        }
        primedURL.appendQueryItem(name: "page", value: String(page))

        var request = URLRequest(url: primedURL)
        request.httpMethod = MMBApiService.HTTPMethod.get.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        guard let accessToken = AuthManager.shared.getAccessToken() else {
            print("<<<< Failed to retrieve access token >>>>")
            return nil
        }
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        return request
    }

    func asAuthedPostRequest<Body: Encodable>(parameters: Body, jsonEncoder: JSONEncoder) -> URLRequest? {

        guard let primedURL = self.primedURL() else {
            print("<<<< Failed to create primed HTTP URL >>>>")
            return nil
        }

        var request = URLRequest(url: primedURL)
        request.httpMethod = MMBApiService.HTTPMethod.post.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let accessToken = AuthManager.shared.getAccessToken() else {
            print("<<<< Failed to retrieve access token >>>>")
            return nil
        }
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")

        do {
            request.httpBody = try jsonEncoder.encode(parameters)
        } catch let error {
            print("<<<< Failed to encode paramters with error: \(error) >>>>")
            return nil
        }
        return request
    }

    func asPostRequest<Body: Encodable>(parameters: Body) -> URLRequest? {
        guard let primedURL = self.primedURL() else {
            print("<<<< Failed to create primed HTTP URL >>>>")
            return nil
        }

        var request = URLRequest(url: primedURL)
        request.httpMethod = MMBApiService.HTTPMethod.post.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        do {
            let jsonEncoder = JSONEncoder()
            jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
            request.httpBody = try jsonEncoder.encode(parameters)
        } catch let error {
            print("<<<< Failed to encode paramters with error: \(error) >>>>")
            return nil
        }
        return request
    }

    private func primedURL(forHTTP: Bool = !MMBApiService.shared.usingProduction) -> URL? {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            print("<<<< Failed to assemble URLComponents >>>>")
            return nil
        }

        if forHTTP {
            urlComponents.scheme = "http"
        }

        let queryItems = [URLQueryItem(name: "Content-Type", value: "application/json")]
        urlComponents.queryItems = queryItems

        guard let url = urlComponents.url else {
            print("<<<< Failed to get url from urlComponents.url >>>>")
            return nil
        }
        return url
    }

    private mutating func appendQueryItem(name: String, value: String?) {

        guard var urlComponents = URLComponents(string: absoluteString) else { return }

        var queryItems: [URLQueryItem] = urlComponents.queryItems ?? []
        let queryItem = URLQueryItem(name: name, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems
        self = urlComponents.url!
    }
}

fileprivate extension URLRequest {
    init(_ url: URL) {
        self.init(url: url)
        self.httpMethod = "GET"
        self.addValue("application/json", forHTTPHeaderField: "Content-Type")

        guard let accessToken = AuthManager.shared.getAccessToken() else {
            print("<<<< Failed to retrieve access token >>>>")
            return
        }
        self.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
    }
}
