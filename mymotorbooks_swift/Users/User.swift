//
//  User.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/12/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

struct User: Decodable {
    let pk: Int
    let username: String?
    let email: String
    let firstName: String
    let lastName: String
    let dateJoined: Date
    let company: Company
}
