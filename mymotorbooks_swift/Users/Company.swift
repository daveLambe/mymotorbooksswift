//
//  Company.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/12/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

struct Company: Decodable {
    let pk: Int
    let name: String
    let registrationTime: Date
    let country: String
    let addressLine1: String
    let addressLine2: String
    let addressLine3: String
    let addressLine4: String
    let bankAccountNumber: String
    let vatNumber: String
    let email: String
    let currency: String
}
