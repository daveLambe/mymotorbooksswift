//
//  AppDelegate.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/20/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        #if targetEnvironment(simulator)
        let setHardwareLayout = NSSelectorFromString("setHardwareLayout:")
        UITextInputMode.activeInputModes
            .filter({ $0.responds(to: setHardwareLayout) })
            .forEach { $0.perform(setHardwareLayout, with: nil) }
        #endif

        window = UIWindow(frame: UIScreen.main.bounds)

        if #available(iOS 15, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
            navBarAppearance.backgroundColor = MyMotorBooksColor.pink.rawValue
            UINavigationBar.appearance().standardAppearance = navBarAppearance
            UINavigationBar.appearance().compactAppearance = navBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance

            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithOpaqueBackground()
            tabBarAppearance.backgroundColor = MyMotorBooksColor.pink.rawValue
            UITabBar.appearance().barTintColor = MyMotorBooksColor.pink.rawValue
            UITabBar.appearance().tintColor = UIColor.white
            UITabBar.appearance().barStyle = .black
            UITabBar.appearance().standardAppearance = tabBarAppearance
            UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
        } else {
            UINavigationBar.appearance().barTintColor = MyMotorBooksColor.pink.rawValue
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().isTranslucent = false

            UINavigationBar.appearance().titleTextAttributes = [
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
        }

        if AuthManager.shared.isUserLoggedIn {
            let mainTabbedVC = MMBTabBarController()
               mainTabbedVC.modalPresentationStyle = .fullScreen
               window?.rootViewController = mainTabbedVC
        } else {
            let loginVC = LoginViewController()
            window?.rootViewController = loginVC
        }

        window?.makeKeyAndVisible()

        return true
    }

    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

