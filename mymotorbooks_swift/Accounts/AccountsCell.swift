//
//  AccountsCell.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/4/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class AccountCell: BaseTableViewCell<Account> {
    override var item: Account? {
        didSet {
            guard let account = item else { return }
            codeLabel.text = "#: \(account.code)"
            descriptionLabel.text = "Acc: \(account.description)"
        }
    }

    private let codeLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let accountImageView: UIImageView = {
        let imageView = UIImageView()
        let defaultImage = UIImage(systemName: "book.fill")
        imageView.image = defaultImage
        imageView.contentMode = .scaleAspectFit
        imageView.setCorner(radius: 10)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let cellSeperator: UIView = {
        let seperatorView = UIView()
        seperatorView.backgroundColor = MyMotorBooksColor.pink.rawValue
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        return seperatorView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = MyMotorBooksColor.black.rawValue
        contentView.addSubview(accountImageView)
        contentView.addSubview(codeLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(cellSeperator)
        configureLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureLayout() {

        NSLayoutConstraint.activate([
            accountImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            accountImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
            accountImageView.heightAnchor.constraint(equalToConstant: 65),
            accountImageView.widthAnchor.constraint(equalToConstant: 65),

            descriptionLabel.leadingAnchor.constraint(equalTo: accountImageView.trailingAnchor, constant: 4),
            descriptionLabel.centerYAnchor.constraint(equalTo: accountImageView.centerYAnchor),

            codeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            codeLabel.centerYAnchor.constraint(equalTo: accountImageView.centerYAnchor),

            cellSeperator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left),
            cellSeperator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -self.separatorInset.right),
            cellSeperator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            cellSeperator.heightAnchor.constraint(equalToConstant: 1)

        ])
    }
}
