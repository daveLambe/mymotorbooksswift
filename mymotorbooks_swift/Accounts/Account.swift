//
//  Accounts.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/4/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation


struct Accounts: MMBAPIResponse {
    typealias T = Account

    let results: [Account]
    let count: Int
    let next: String?
    let previous: String?
}

struct Account: Decodable, Searchable {
    let pk: Int
    let objOwner: Int
    let accountType: String
    let description: String
    let code: String
    let hasCustomerSubAccounts: Bool
    let hasSupplierSubAccounts: Bool
    let hasCountrySubAccounts: Bool
    let hasVehicleSubAccounts: Bool
    let isVatAccount: Bool

    var query: String { return self.accountType }
}



//CREDITORS_CONTROL = 0
//STOCK_CONTROL = 1
//IE_VAT_CONTROL = 2
//BANK_ACCOUNT = 3
//SALES_LEDGER_CONTROL = 4
//VEHICLE_SALES = 5
//VEHICLE_COST_OF_SALES = 6
//DIRECT_COSTS = 7
//VRT_CONTROL = 8
//UK_VAT_CONTROL = 9
//WAGES_NET_PAY_CONTROL = 10
//WAGES_EMPLOYEE_TAX_CONTROL = 11
//WAGES_EMPLOYER_TAX_CONTROL = 12
//
//labels = {
//    CREDITORS_CONTROL: 'Creditors Control',
//    STOCK_CONTROL: 'Stock Control',
//    IE_VAT_CONTROL: 'IE VAT Control',
//    BANK_ACCOUNT: 'Bank Account',
//    SALES_LEDGER_CONTROL: 'Sales Ledger Control',
//    VEHICLE_SALES: 'Vehicle Sales',
//    VEHICLE_COST_OF_SALES: 'Vehicle Cost of Sales',
//    DIRECT_COSTS: 'Direct Costs',
//    VRT_CONTROL: 'VRT Control',
//    UK_VAT_CONTROL: 'HMRC VAT Control',
//    WAGES_NET_PAY_CONTROL: 'Wages - Net Pay Control',
//    WAGES_EMPLOYEE_TAX_CONTROL: 'Wages - Employee Tax Control',
//    WAGES_EMPLOYER_TAX_CONTROL: 'Wages - Employer Tax Control'
//}
