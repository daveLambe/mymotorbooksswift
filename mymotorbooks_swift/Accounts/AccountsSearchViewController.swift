//
//  AccountsSearchViewController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

final class AccountsSearchViewController: MMBTableViewSearchController<AccountCell, Account> {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Accounts"
        tableView.estimatedRowHeight = 100
        fetchAccounts()
    }

    func fetchAccounts() {
        MMBApiService.shared.fetchAccounts() { [weak self] (result: Result<Accounts, MMBApiService.MMBApiServiceError>) in
            switch result {
            case .success(let accountResults):
                print("<<<< Successfully fetched Accounts! >>>>")
                self?.models = accountResults.results
                _ = self?.models.filter { account in
                    return account.accountType == "Bank Account" || account.accountType == "VRT Control"
                }
                self?.tableView.reloadData()
            case .failure(let error):
                print("<<<< Error fetching accounts: \(error.localizedDescription) >>>>")
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
