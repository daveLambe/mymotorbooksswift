//
//  Transaction.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 9/13/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

struct Transactions: Decodable, MMBAPIResponse {
    typealias T = Transaction

    var results: [Transaction]
    let count: Int
    let next: String?
    let previous: String?

//    private enum CodingKeys: String, CodingKey {
//        case count
//        case next
//        case previous
//        case results
//        case transactions = "results"
//    }
}

struct TransactionPost: Codable {
    let transaction: TransactionData
    let createChild: Bool

    private enum CodingKeys: String, CodingKey {
        case transaction = "transactionData"
        case createChild
    }

    init(transaction: TransactionData, createChild: Bool) {
        self.transaction = transaction
        self.createChild = createChild
    }
}

struct TransactionData: Codable {
    let objOwner: Int?
    let transactionType: Int
    let transactionSubtype: Int
    var trader: Int?
    var vatRate: Int?
    let expenseType: String?
    var description: String?
    var vehicle: Int?
    //    let valueExVat: Double?
    let valueExVat: String?
    var parents: [Int]?
    //    let parents: [Int?]
    var date: String?
    var periodDate: String?
    let isProcessed: Bool?
    var isEstimate: Bool?
    var isVatDeductibleForVehicle: Bool?
    var fromAccount: Int?
    var toAccount: Int?

    init(objOwner: Int? = nil,
         transactionType: Int,
         transactionSubtype: Int,
         trader: Int? = nil,
         vatRate: Int? = nil,
         expenseType: String? = "OTHER",
         description: String? = nil,
         vehicle: Int? = nil,
         valueExVat: String,
         parents: [Int]? = nil,
         date: String? = Date.today.formatDateForAPI(),
         periodDate: String? = Date.today.formatDateForAPI(),
         isProcessed: Bool? = nil,
         isEstimate: Bool? = nil,
         isVatDeductibleForVehicle: Bool? = nil,
         fromAccount: Int? = nil,
         toAccount: Int? = nil) {
        self.objOwner = objOwner
        self.transactionType = transactionType
        self.transactionSubtype = transactionSubtype
        self.trader = trader
        self.vatRate = vatRate
        self.expenseType = expenseType
        self.description = description
        self.vehicle = vehicle
        self.valueExVat = valueExVat
        self.parents = parents
        self.date = date
        self.periodDate = periodDate
        self.isProcessed = isProcessed
        self.isEstimate = isEstimate
        self.isVatDeductibleForVehicle = isVatDeductibleForVehicle
        self.fromAccount = fromAccount
        self.toAccount = toAccount
    }

    private enum CodingKeys: String, CodingKey {
        case objOwner = "obj_owner"
        case transactionType = "transaction_type"
        case transactionSubtype = "sub_type"
        case trader
        case vatRate = "vat_rate"
        case expenseType = "expense_type"
        case description
        case vehicle
        case valueExVat = "value_ex_vat"
        case parents
        case date
        case periodDate = "period_date"
        case isProcessed = "is_processed"
        case isEstimate = "is_estimate"
        case isVatDeductibleForVehicle = "is_vat_deductible_for_vehicle"
        case fromAccount = "from_account"
        case toAccount = "to_account"
    }
}

struct Transaction: Codable, Searchable {
    let pk: Int
    let objOwner: Int
    let number: Int
    let transactionType: TransactionType
    let transactionSubtype: TransactionType
    let trader: Trader?
    let vatRate: Int?
    let expenseType: String
    let description: String?
    let vehicle: Int?
    let valueExVat: String?
    let parents: [Transaction]?
    let date: Date
    let periodDate: Date?
    let isProcessed: Bool
    let isEstimate: Bool
    let fromAccount: Int?
    let toAccount: Int?

    var query: String { return self.transactionType.name }

    init(
        pk: Int,
        objOwner: Int, number: Int, transactionType: TransactionType, transactionSubtype: TransactionType, trader: Trader?,
        vatRate: Int?, expenseType: String, description: String?, vehicle: Int?, valueExVat: String?, parents: [Transaction]?,
        date: Date, periodDate: Date? = nil, isProcessed: Bool, isEstimate: Bool, fromAccount: Int?, toAccount: Int?) {

        self.pk = pk
        self.objOwner = objOwner
        self.number = number
        self.transactionType = transactionType
        self.transactionSubtype = transactionSubtype
        self.trader = trader
        self.vatRate = vatRate
        self.expenseType = expenseType
        self.description = description
        self.vehicle = vehicle
        self.valueExVat = valueExVat
        self.parents = parents
        self.date = date
        self.periodDate = periodDate
        self.isProcessed = isProcessed
        self.isEstimate = isEstimate
        self.fromAccount = fromAccount
        self.toAccount = toAccount
    }

    private enum CodingKeys: String, CodingKey {
        case pk
        case objOwner
        case number
        case transactionType
        case transactionSubtype = "subType"
        case trader
        case vatRate
        case expenseType
        case description
        case vehicle
        case valueExVat
        case parents
        case date
        case periodDate
        case isProcessed
        case isEstimate
        case fromAccount
        case toAccount
    }
}

class TransactionType: Codable {
    let pk: Int
    let name: String
    let label: String
    let direction: TransactionDirection
    let isSubType: Bool
    let needsTrader: Bool?
    let needsFromAccount: Bool
    let needsToAccount: Bool

    init(pk: Int, name: String, label: String,
         direction: TransactionDirection, isSubType: Bool,
         needsTrader: Bool? = nil, needsFromAccount: Bool, needsToAccount: Bool) {
        self.pk = pk
        self.name = name
        self.label = label
        self.direction = direction
        self.isSubType = isSubType
        self.needsTrader = needsTrader
        self.needsFromAccount = needsFromAccount
        self.needsToAccount = needsToAccount
    }
}
/*
Use this to try fix everything

 https://stackoverflow.com/questions/44549310/how-to-decode-a-nested-json-struct-with-swift-decodable-protocol

 https://app.quicktype.io/

 https://docs.swift.org/swift-book/LanguageGuide/NestedTypes.html
 */

struct NewTransactionType: Codable {
//    let type: TransactionTypes

    enum TypeName: Int, CaseIterable {
        case purchase = 1
        case sale
        case payment
        case receipt
        case vehiclePurchase
        case vehicleSale
        case vehicleExpense
        case generalExpense
        case generalSale
        case VATPayment
        case VRTPayment
        case transfer
        case tradeIn
        case VATReceipt
        case wagesNetPayment
        case wagesEmployeeTax
        case wagesEmployerTax
    }
}

enum TransactionTypes: Int, CaseIterable {
    case purchase = 1
    case sale
    case payment
    case receipt
    case vehiclePurchase
    case vehicleSale
    case vehicleExpense
    case generalExpense
    case generalSale
    case VATPayment
    case VRTPayment
    case transfer
    case tradeIn
    case VATReceipt
    case wagesNetPayment
    case wagesEmployeeTax
    case wagesEmployerTax

    var pk: Int { return self.rawValue }

//    var name: Int { return self.rawValue - 1 }

    var label: String {
        switch self {
        case .purchase: return "Purchase"
        case .sale: return "Sale"
        case .payment: return "Payment"
        case .receipt: return "Receipt"
        case .vehiclePurchase: return "Vehicle Purchase"
        case .vehicleSale: return "Vehicle Sale"
        case .vehicleExpense: return "Vehicle Expense"
        case .generalExpense: return "General Expense"
        case .generalSale: return "General Sale"
        case .VATPayment: return "VAT Payment"
        case .VRTPayment: return "VRT Payment"
        case .transfer: return "Transfer"
        case .tradeIn: return "Trade In"
        case .VATReceipt: return "VAT Receipt"
        case .wagesNetPayment: return "Wages - Net Payment"
        case .wagesEmployeeTax: return "Wages - Employee Tax"
        case .wagesEmployerTax: return "Wages - Employer Tax"
        }
    }

    var name: String { return self.label.replacingOccurrences(of: "\\W+", with: "_", options: .regularExpression).uppercased() }

    var type: TransactionType {
        switch self {

        case .purchase: return TransactionType(pk: pk, name: name, label: label,
                                               direction: TransactionDirection.IN, isSubType: false,
                                               needsFromAccount: false, needsToAccount: false)

        case .sale: return TransactionType(pk: pk, name: name, label: label,
                                           direction: TransactionDirection.IN, isSubType: false,
                                           needsFromAccount: false, needsToAccount: false)

        case .payment: return TransactionType(pk: pk, name: name, label: label,
                                              direction: TransactionDirection.IN, isSubType: false,
                                              needsFromAccount: true, needsToAccount: true)

        case .receipt: return TransactionType(pk: pk, name: name, label: label,
                                              direction: TransactionDirection.OUT, isSubType: false,
                                              needsFromAccount: true, needsToAccount: true)

        case .vehiclePurchase: return TransactionType(pk: pk, name: name, label: label,
                                                      direction: TransactionDirection.IN, isSubType: true,
                                                      needsTrader: true, needsFromAccount: true, needsToAccount: false)

        case .vehicleSale: return TransactionType(pk: pk, name: name, label: label,
                                                  direction: TransactionDirection.OUT, isSubType: true,
                                                  needsTrader: true, needsFromAccount: false, needsToAccount: true)

        case .vehicleExpense: return TransactionType(pk: pk, name: name, label: label,
                                                     direction: TransactionDirection.IN, isSubType: true,
                                                     needsTrader: true, needsFromAccount: true, needsToAccount: false)

        case .generalExpense: return TransactionType(pk: pk, name: name, label: label,
                                                     direction: TransactionDirection.IN, isSubType: true,
                                                     needsTrader: true, needsFromAccount: true, needsToAccount: false)

        case .generalSale: return TransactionType(pk: pk, name: name, label: label,
                                                  direction: TransactionDirection.OUT, isSubType: true,
                                                  needsTrader: true, needsFromAccount: false, needsToAccount: true)

        case .VATPayment: return TransactionType(pk: pk, name: name, label: label,
                                                 direction: TransactionDirection.IN, isSubType: true,
                                                 needsTrader: false, needsFromAccount: true, needsToAccount: true)

        case .VRTPayment: return TransactionType(pk: pk, name: name, label: label,
                                                 direction: TransactionDirection.IN, isSubType: true,
                                                 needsTrader: false, needsFromAccount: true, needsToAccount: false)

        case .transfer: return TransactionType(pk: pk, name: name, label: label,
                                               direction: TransactionDirection.IN, isSubType: true,
                                               needsTrader: false, needsFromAccount: true, needsToAccount: true)

        case .tradeIn: return TransactionType(pk: pk, name: name, label: label,
                                              direction: TransactionDirection.OUT, isSubType: true,
                                              needsTrader: true, needsFromAccount: false, needsToAccount: false)

        case .VATReceipt: return TransactionType(pk: pk, name: name, label: label,
                                                 direction: TransactionDirection.OUT, isSubType: true,
                                                 needsTrader: false, needsFromAccount: true, needsToAccount: true)

        case .wagesNetPayment: return TransactionType(pk: pk, name: name, label: label,
                                                      direction: TransactionDirection.IN, isSubType: true,
                                                      needsTrader: false, needsFromAccount: true, needsToAccount: false)

        case .wagesEmployeeTax: return TransactionType(pk: pk, name: name, label: label,
                                                       direction: TransactionDirection.IN, isSubType: true,
                                                       needsTrader: false, needsFromAccount: true, needsToAccount: false)

        case .wagesEmployerTax: return TransactionType(pk: pk, name: name, label: label,
                                                       direction: TransactionDirection.IN, isSubType: true,
                                                       needsTrader: false, needsFromAccount: true, needsToAccount: false)
        }
    }
}

enum TransactionDirection: Int, CaseIterable, Codable {
    case IN = 0
    case OUT

    var label: String {
        switch self {
        case .IN: return "IN"
        case .OUT: return "OUT"
        }
    }
}

struct VatRate: Codable {
    let pk: Int
    let name: String
    let rate: String
    let country: Int

    init(pk: Int, name: String, rate: String, country: Int) {
        self.pk = pk
        self.name = name
        self.rate = rate
        self.country = country
    }

    private enum CodingKeys: String, CodingKey {
        case pk
        case name
        case rate
        case country
    }
}
