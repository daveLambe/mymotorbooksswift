//
//  AddTransactionSplitViewController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/8/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

protocol StackedLabelFields {
    var labelStack: UIStackView { get }
    var fieldStack: UIStackView { get }
}

protocol AddTransactionNextButtonDelegate: AnyObject {
    func nextTapped()
}

class AddTransactionSplitFieldViewController: UIViewController {

    public weak var addTransactionFlowDelegate: AddTransactionNextButtonDelegate?
    var topFieldContainingView: UIView & StackedLabelFields
    var bottomTableContainingView: UITableView
    var activeTextField: MMBTextField?
    var currentStep: AddTransactionStep

    init(textFieldContainedView: UIView & StackedLabelFields, tableViewContainedView: UITableView, step: AddTransactionStep) {
        self.topFieldContainingView = textFieldContainedView
        self.bottomTableContainingView = tableViewContainedView
        self.currentStep = step
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Creating Transaction"
        self.view.backgroundColor = MyMotorBooksColor.black.rawValue
        cancelButton.addTarget(self, action: #selector(cancelSheetTapped), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
//        let viewTappedRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        //        view.addGestureRecognizer(viewTappedRecognizer)

        containingStackView.addArrangedSubview(topFieldContainingView)
        containingStackView.addArrangedSubview(bottomTableContainingView)
        self.view.addSubview(createTransactionLabel)
        self.view.addSubview(stepLabel)
        self.view.addSubview(cancelButton)
        self.view.addSubview(nextButton)
        self.view.addSubview(containingStackView)
//        nextButton.isEnabled = false
        nextButton.isEnabled = true
        updateStepLabel()
        setupLayout()
    }

    // MARK: - Elements

    let containingStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.clipsToBounds = true
        sv.distribution = .fillEqually
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let createTransactionLabel: UILabel = {
        let label = UILabel()
        label.text = "Creating Transaction"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let stepLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let cancelButton: UIButton = {
        let button = UIButton()
        let buttonAttributedText = "Cancel".asAttributedText(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),
                                                                              NSAttributedString.Key.foregroundColor: MyMotorBooksColor.pink.rawValue])
        button.setTitle("Cancel", for: .normal)
        button.setAttributedTitle(buttonAttributedText, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let nextButton: UIButton = {
        let button = UIButton()
        let buttonAttributedText = "Next".asAttributedText(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                                                                            NSAttributedString.Key.foregroundColor: UIColor.white])
        button.setTitle("Next", for: .normal)
        button.setAttributedTitle(buttonAttributedText, for: .normal)
        button.backgroundColor = MyMotorBooksColor.black.rawValue
        button.setBorder(width: 4, color: MyMotorBooksColor.pink.rawValue)
        button.setShadow(color: UIColor.white, radius: 4)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: - Methods

    public func updateStepLabel() {
        let step = AddTransactionFlowManager.shared.currentStep
        stepLabel.text = "Step \(step.rawValue)/\(AddTransactionStep.allCases.count): \(step.description)"
    }

    // MARK: - Textfield Methods
    @objc func textFieldDidEndEditing(_ textField: UITextField) {
        guard let activeField = activeTextField else {
            print("<<<< NO ACTIVE TEXT FIELD >>>>")
            self.view.endEditing(true)
            return
        }

        if let text = activeField.text, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            setActiveFieldText(text)
            //            setActiveFieldSelectedOption()
        } else {
            print("<<<< SETTING PLACEHOLDER TEXT FOR FIELD \(activeField) >>>>")
            setActiveFieldPlaceholder()
        }
        activeTextField = nil
        self.view.endEditing(true)
    }

    func setActiveFieldText(_ newText: String) {
        activeTextField?.text = newText
    }

    func setActiveFieldPlaceholder() {
        activeTextField?.placeholder = activeTextField?.placeholderText
    }

    // MARK: - TableView Methods
    func hideTableView(_ hide: Bool) {
        self.bottomTableContainingView.isHidden = hide
    }

    @objc func cancelSheetTapped() {
        print("<<<< Cancel Button pressed! >>>>")
        AddTransactionFlowManager.shared.endFlow()
        self.dismiss(animated: true)
    }

    @objc func nextTapped() {
        print("<<<< Next tapped in \(#file) >>>>")
        AddTransactionFlowManager.shared.printCurrentStep()
        AddTransactionFlowManager.shared.next()
    }

    // MARK: - Gesture Recognizers
    @objc func viewTapped() {
        print("<<<< VIEW TAPPED!!!! \(#function) >>>>")
        // Might have to change to field?
        //        self.view.resignFirstResponder()
        activeTextField?.resignFirstResponder()
    }
}

extension AddTransactionSplitFieldViewController {
    func setupLayout() {
        NSLayoutConstraint.activate([

            createTransactionLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 45),
            createTransactionLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            createTransactionLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            stepLabel.topAnchor.constraint(equalTo: createTransactionLabel.bottomAnchor, constant: 10),
            stepLabel.leadingAnchor.constraint(equalTo: createTransactionLabel.leadingAnchor),
            stepLabel.trailingAnchor.constraint(equalTo: createTransactionLabel.trailingAnchor),

            cancelButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
            cancelButton.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10),

            nextButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            nextButton.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 80),
            nextButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -80),

            // Containing Stack View
            containingStackView.topAnchor.constraint(equalTo: stepLabel.bottomAnchor, constant: 20),
            containingStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            containingStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10),
            containingStackView.bottomAnchor.constraint(equalTo: nextButton.topAnchor, constant: -20),

            // topFieldContainingView
            topFieldContainingView.labelStack.topAnchor.constraint(equalTo: topFieldContainingView.topAnchor),
            topFieldContainingView.labelStack.leadingAnchor.constraint(equalTo: topFieldContainingView.leadingAnchor),
            topFieldContainingView.labelStack.trailingAnchor.constraint(equalTo: topFieldContainingView.leadingAnchor, constant: self.view.frame.size.width / 2),

            topFieldContainingView.fieldStack.topAnchor.constraint(equalTo: topFieldContainingView.topAnchor),
            topFieldContainingView.fieldStack.leadingAnchor.constraint(equalTo: topFieldContainingView.labelStack.trailingAnchor),
            topFieldContainingView.fieldStack.trailingAnchor.constraint(equalTo: topFieldContainingView.trailingAnchor),

            // bottomTableContainingView
            bottomTableContainingView.leadingAnchor.constraint(equalTo: containingStackView.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            bottomTableContainingView.trailingAnchor.constraint(equalTo: containingStackView.safeAreaLayoutGuide.trailingAnchor, constant: -20),

        ])
    }
}

extension AddTransactionSplitFieldViewController: AddTransactionNextButtonDelegate {
    
}

extension AddTransactionSplitFieldViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DispatchQueue.main.async() {
            scrollView.scrollIndicators.vertical?.backgroundColor = MyMotorBooksColor.pink.rawValue
        }
    }
}

extension AddTransactionSplitFieldViewController: MMBToolbarDelegate {
    func doneTapped() {
        print("<<<< Toolbar: Done button tapped >>>>")
        self.view.endEditing(true)
    }

    func cancelTapped() {
        print("<<<< Toolbar: Cancel button tapped >>>>")
        self.view.endEditing(true)
    }
}
