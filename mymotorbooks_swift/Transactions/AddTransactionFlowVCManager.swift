//
//  AddTransactionFlowVCManager.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/21.
//  Copyright © 2021 Dave Lambe. All rights reserved.
//

import UIKit

class AddTransactionFlowVCManager: UIViewController {
    let containerView = UIView()
    let navButtonView = MMBNavButtonView()

    lazy var step1Details: AddTransactionFlowDetails = {
        let vc = AddTransactionFlowDetails()
        return vc
    }()

    lazy var step2Vehicle: AddTransactionFlowVehicle = {
        let vc = AddTransactionFlowVehicle()
        return vc
    }()

    lazy var step3Parent: AddTransactionFlowParent = {
        let vc = AddTransactionFlowParent()
        return vc
    }()

    lazy var step4Value: AddTransactionFlowValue = {
        let vc = AddTransactionFlowValue()
        return vc
    }()

    lazy var step5Bank: AddTransactionFlowBank = {
        let vc = AddTransactionFlowBank()
        return vc
    }()

    public func showSuccessAlert() {
        let successAlertController = UIAlertController(title: "Transaction created", message: "Your Transaction was created successfully", preferredStyle: .alert)

        successAlertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            AddTransactionFlowManager.shared.endFlow()
        }))

        self.present(successAlertController, animated: true)
    }

    public func showFailedAlert() {
        let failAlertController = UIAlertController(title: "Something went wrong", message: "Could not create Transaction. Please try again later.", preferredStyle: .alert)
        failAlertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

        self.present(failAlertController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyMotorBooksColor.black.rawValue
        setupViews()
        configureViews()
        addChildVC(step1Details)
        navButtonView.hideButton(buttonDirection: .back, hide: true)
        navButtonView.hideButton(buttonDirection: .forward, hide: true)
        navButtonView.backButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backNavTapped)))
        navButtonView.forwardButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(forwardNavTapped)))
        navButtonView.isHidden = true
        updateNavButtons()
        // TODO: Disable forward button
    }

    private func setupViews() {
        view.addSubview(navButtonView)
        view.addSubview(containerView)
    }

    private func configureViews() {
        navButtonView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            navButtonView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            navButtonView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            navButtonView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
            navButtonView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 50),

            containerView.leadingAnchor.constraint(equalTo: navButtonView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: navButtonView.trailingAnchor),
            containerView.topAnchor.constraint(equalTo: navButtonView.bottomAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }

    private func addChildVC(_ childVC: UIViewController) {
        self.addChild(childVC)
        childVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(childVC.view)
        childVC.didMove(toParent: self)
        reconfigureChildView(childVC.view)
    }

    private func currentVC() -> UIViewController {
        switch AddTransactionFlowManager.shared.currentStep {
            case .details: return step1Details
            case .vehicle: return step2Vehicle
            case .parent: return step3Parent
            case .value: return step4Value
            case .bank: return step5Bank
        }
    }

    public func previousView() {
        currentVC().remove()
        updateNavButtons()
        navButtonView.hideButton(buttonDirection: .forward, hide: false)
    }

    public func nextView() {
        addChildVC(currentVC())
        updateNavButtons()
    }

    public func endFlow() {
        self.dismiss(animated: true)
    }

    private func reconfigureChildView(_ childView: UIView) {
        NSLayoutConstraint.activate([
            childView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            childView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            childView.topAnchor.constraint(equalTo: containerView.topAnchor),
            childView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }

    public func updateNavButtons() {
        if let previousStep = AddTransactionFlowManager.shared.previousStep {
            navButtonView.isHidden = false
            navButtonView.hideButton(buttonDirection: .back, hide: false)
            navButtonView.setButton(buttonDirection: .back, text: "Step \(previousStep.rawValue): \(previousStep.description)")
        } else {
            navButtonView.hideButton(buttonDirection: .back, hide: true)
        }

        if let nextStep = AddTransactionFlowManager.shared.nextStep, let current = currentVC() as? AddTransactionFlowStep, current.requiredFieldsSatisfied {
            navButtonView.setButton(buttonDirection: .forward, text: "Step \(nextStep.rawValue): \(nextStep.description)")
        } else {
            navButtonView.hideButton(buttonDirection: .forward, hide: true)
        }
    }

    // Not used
    private func moveBackTo(view: UIView) {
        containerView.bringSubviewToFront(step1Details.view)
    }

    // Navigation buttons
    @objc func backNavTapped() {
        print("<<<< Back nav button tapped >>>>")
        AddTransactionFlowManager.shared.previous()
    }

    @objc func forwardNavTapped() {
        print("<<<<  Forward nav button tapped >>>>")
        AddTransactionFlowManager.shared.next()
    }
}

fileprivate extension UIViewController {
    func remove() {
        guard parent != nil else {
            print("<<<<  No parent view set! >>>>")
            return
        }
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
