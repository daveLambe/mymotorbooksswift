//
//  AddTransactionView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 10/17/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class AddTransactionView: UIView, StackedLabelFields {
    enum TypeField: String, CaseIterable {
        case purchase = "Purchase"
        case sale = "Sale"
        case payment = "Payment"
        case receipt = "Receipt"
    }

    let labelStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .equalSpacing
        sv.spacing = 15
        sv.clipsToBounds = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let fieldStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .equalSpacing
        sv.spacing = 15
        sv.clipsToBounds = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let typeLabel: UILabel = {
        let label = UILabel(withText: "Transaction Type")
        return label
    }()

    let subtypeLabel: UILabel = {
        let label = UILabel(withText: "Transaction Subtype")
        return label
    }()

    let expenseTypeLabel: UILabel = {
        let label = UILabel(withText: "Expense Type")
        return label
    }()

    let descriptionLabel: UILabel = {
        let label = UILabel(withText: "Description")
        return label
    }()

    let contactLabel: UILabel = {
        let label = UILabel(withText: "Contact")
        return label
    }()

    let typePickerField: MMBPickerTextField = {
        let pickerField = MMBPickerTextField(placeholderText: "", isRequiredField: true)
        pickerField.tag = 1
        return pickerField
    }()

    let subTypePickerField: MMBPickerTextField = {
        let pickerField = MMBPickerTextField(placeholderText: "", isRequiredField: true)
        pickerField.tag = 2
        return pickerField
    }()

    let expenseTypePickerField: MMBPickerTextField = {
        let pickerField = MMBPickerTextField(placeholderText: "")
        pickerField.tag = 3
        return pickerField
    }()

    let descriptionTextField: MMBTextField = {
        let textField = MMBTextField(placeholderText: "")
        textField.tag = 4
        return textField
    }()

    // Need to check if hidden AND required
    let contactTextField: MMBTextField = {
        let textField = MMBTextField(placeholderText: "No Contact Selected", isRequiredField: true)
        textField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
        textField.tag = 5
        return textField
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        labelStack.addArrangedSubview(typeLabel)
        labelStack.addArrangedSubview(subtypeLabel)
        labelStack.addArrangedSubview(expenseTypeLabel)
        labelStack.addArrangedSubview(descriptionLabel)
        labelStack.addArrangedSubview(contactLabel)

        fieldStack.addArrangedSubview(typePickerField)
        fieldStack.addArrangedSubview(subTypePickerField)
        fieldStack.addArrangedSubview(expenseTypePickerField)
        fieldStack.addArrangedSubview(descriptionTextField)
        fieldStack.addArrangedSubview(contactTextField)

        self.addSubview(labelStack)
        self.addSubview(fieldStack)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    func getAllTextFields(includingType: Bool = true) -> [MMBTextField] {
        return includingType ? [typePickerField, subTypePickerField, expenseTypePickerField, descriptionTextField, contactTextField] : [subTypePickerField, expenseTypePickerField, descriptionTextField, contactTextField]
    }

    private func getAllNonPickerFields() -> [MMBTextField] {
        return getAllTextFields().filter { !($0 is MMBPickerTextField) }
    }

    private func getAllPickerTextFields() -> [MMBPickerTextField] {
        return getAllTextFields().compactMap { $0 as? MMBPickerTextField }
    }

    func expenseTypeFieldHidden(_ hidden: Bool) {
        [expenseTypeLabel, expenseTypePickerField].forEach {
            $0.isHidden = hidden
        }
    }

    func descriptionFieldHidden(_ hidden: Bool) {
        [descriptionLabel, descriptionTextField].forEach {
            $0.isHidden = hidden
        }
    }

    func contactFieldHidden(_ hidden: Bool) {
        [contactLabel, contactTextField].forEach {
            $0.isHidden = hidden
        }
    }

    func optionalInformationFieldsHidden(_ isHidden: Bool) {
        print("<<<< Setting Optional Information Fields Hidden to: \(isHidden) >>>>")
        [descriptionLabel, descriptionTextField,
         contactLabel, contactTextField].forEach {
            $0.isHidden = isHidden
         }
    }
}

fileprivate extension UILabel {
    convenience init(withText: String) {
        self.init()
        text = withText
        textColor = MyMotorBooksColor.pink.rawValue
        font = UIFont.boldSystemFont(ofSize: 18)
    }
}
