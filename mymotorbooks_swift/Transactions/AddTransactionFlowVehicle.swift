//
//  AddTransactionFlowVehicle.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/8/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class AddTransactionFlowVehicle: AddTransactionSplitFieldViewController, AddTransactionFlowStep,  UITextFieldDelegate {

    var addVehicleView: StackedLabelFieldView!
    var vehicleTableView: VehiclesTableView!
//    var vehicleTableView = MMBTableView<Vehicle, Vehicles, VehicleCell>(configure: { (cell: VehicleCell, vehicle) in
//        cell.vehicle = vehicle
//    })
    var selectedVehicle: Vehicle?
    var requiredFieldsSatisfied: Bool { return self.selectedVehicle != nil }

    // MARK: - AddTransactionFlowStep Protocol Variables
    var step: AddTransactionStep = .vehicle

    override var activeTextField: MMBTextField? {
        didSet {
            super.hideTableView(self.activeTextField != vehicleTextField)
        }
    }

    var allTextFields: [MMBTextField] { return [vehicleTextField] }

    let vehicleLabel: UILabel = {
        let label = UILabel()
        label.text = "Vehicle"
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    let vehicleTextField: MMBTextField = {
        let textField = MMBTextField(placeholderText: "", isRequiredField: true)
        textField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
        textField.sizeToFit()
        return textField
    }()

    init() {
        addVehicleView = StackedLabelFieldView(labels: [vehicleLabel], fields: [vehicleTextField])
        self.vehicleTableView = VehiclesTableView()
        super.init(textFieldContainedView: addVehicleView, tableViewContainedView: vehicleTableView, step: step)
        setupFields()
        setActiveTextField(nil)
        configureStackLayouts()

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomTableContainingView.delegate = self
        super.hideTableView(true)
    }

    override func nextTapped() {
        if requiredFieldsSatisfied {
            AddTransactionFlowManager.shared.vehicle = self.selectedVehicle
            super.nextTapped()
        } else {
            let ac = UIAlertController.forUnsatisfiedFields()
            present(ac, animated: true)
        }
    }

    func setupFields() {
        super.activeTextField = self.activeTextField
        allTextFields.forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
        }
    }

    func resetAllFields() {
        addVehicleView.fields.forEach {
            $0.selectedOption = nil
            $0.text = nil
        }
    }

    func setActiveTextField(_ field: MMBTextField?) {
        super.activeTextField = activeTextField
        activeTextField = field
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
    }

    private func setSelectedVehicle(_ vehicle: Vehicle) {
        let vehicleDescription = "#\(vehicle.stockNumber): \(vehicle.make.asString()) - \(vehicle.regNumberIn ?? "")"

        setActiveFieldText(vehicleDescription)
        self.selectedVehicle = vehicle
        vehicleTextField.endEditing(true)
        vehicleTextField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
    }

    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vehicle = vehicleTableView.data[indexPath.row]
//        setSelectedVehicle(vehicle)
    }

    func configureStackLayouts() {
        NSLayoutConstraint.activate([
            self.addVehicleView.labelStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            self.addVehicleView.labelStack.trailingAnchor.constraint(equalTo: addVehicleView.leadingAnchor, constant: self.view.frame.size.width / 3),
            self.addVehicleView.fieldStack.leadingAnchor.constraint(equalTo: self.addVehicleView.labelStack.trailingAnchor),
            self.addVehicleView.fieldStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10)
        ])
    }
}
