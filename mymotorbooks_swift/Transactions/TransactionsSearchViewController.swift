//
//  TransactionsSearchViewController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

final class TransactionsSearchViewController: MMBTableViewSearchController<TransactionCell, Transaction> {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Transactions"
        tableView.estimatedRowHeight = 120
        let addTransactionButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTransactionPressed))
        navigationItem.rightBarButtonItem = addTransactionButton
        fetchTransactions()
    }

    func fetchTransactions(filterByTrader: Trader? = nil, filterByVehicle: Int? = nil) {
        MMBApiService.shared.fetchTransactions() { [weak self] (transactionsResult) in
            switch transactionsResult {
            case .success(let transactions):
                print("<<<< Successfully fetched Transactions! >>>>")
                self?.models = transactions.results

                if let trader = filterByTrader {
                    self?.filterTransactionsBy(trader: trader)
                }

                if let vehicle = filterByVehicle {
                    self?.filterTransactionsBy(vehicle: vehicle)
                }
                self?.tableView.reloadData()
            case .failure(let error):
                print("<<<< Failed to fetch Transactions with error: \(error) >>>>")
            }
        }
    }

    // Revisit, Maybe try handle in MMBTableViewSearchController
    private func filterTransactionsBy(trader: Trader) {
        let filteredTransactions: [Transaction] = self.models.filter { $0.trader != nil ? ($0.trader!.pk == trader.pk) : false }
        self.models = filteredTransactions
        tableView.reloadData()
    }

    // Revisit, Maybe try handle in MMBTableViewSearchController
    private func filterTransactionsBy(vehicle vehiclePk: Int) {
        let filteredTransactions: [Transaction] = self.models.filter { $0.vehicle != nil ? ($0.vehicle! == vehiclePk) : false }
        self.models = filteredTransactions
        tableView.reloadData()
    }

    @objc private func addTransactionPressed() {
        let addTransactionViewController = AddTransactionFlowManager.shared.startFlow()
        addTransactionViewController.modalPresentationStyle = .fullScreen
        present(addTransactionViewController, animated: true)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
