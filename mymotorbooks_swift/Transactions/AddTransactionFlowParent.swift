//
//  AddTransactionFlowParent.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 12/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class AddTransactionFlowParent: AddTransactionSplitFieldViewController, AddTransactionFlowStep, UITextFieldDelegate {

    // Decouple AddTransactionFlowStep & fields

    var addTransactionsView: StackedLabelFieldView!
    var matchingTransactionsTableView: TransactionsTableView!

    var selectedTransactions: [Transaction]?
    // Add check for if transactions needed
    var requiredFieldsSatisfied: Bool { return !transactionDescription.isEmpty }
    var transactionDescription: String = ""

    // MARK: - AddTransactionFlowStep Protocol Variables
    var step: AddTransactionStep = .parent

    override var activeTextField: MMBTextField? {
        didSet {
            super.hideTableView(self.activeTextField != transactionTextField)
        }
    }

    var allTextFields: [MMBTextField] { return [transactionTextField] }

    func setupFields() {
        super.activeTextField = self.activeTextField
        allTextFields.forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
        }
    }

    func resetAllFields() {
        addTransactionsView.fields.forEach {
            $0.selectedOption = nil
            $0.text = nil
        }
    }

    func setActiveTextField(_ field: MMBTextField?) {
        super.activeTextField = activeTextField
        activeTextField = field
    }

    let transactionLabel: UILabel = {
        let label = UILabel()
        label.text = "Transaction"
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    let transactionTextField: MMBTextField = {
        // I think not required??
        let textField = MMBTextField(placeholderText: "", isRequiredField: false)
        textField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
        textField.sizeToFit()
        return textField
    }()

    init() {
        addTransactionsView = StackedLabelFieldView(labels: [transactionLabel], fields: [transactionTextField])
        matchingTransactionsTableView = TransactionsTableView()

        super.init(textFieldContainedView: addTransactionsView, tableViewContainedView: matchingTransactionsTableView, step: step)
        setupFields()
        setActiveTextField(nil)

        MMBApiService.shared.fetchTransactions() { [weak self] (transactionsResult) -> Void in
            switch transactionsResult {
            case .success(let transactions):
                self?.matchingTransactionsTableView.datasource.data = (self?.filterTransactions(transactions.results))!
                self?.matchingTransactionsTableView.reloadData()
            case .failure(let error):
                print("<<<< Failed to fetch Transactions with error: \(error.localizedDescription) >>>>")
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomTableContainingView.delegate = self
        super.hideTableView(true)
        configureTableView()
        configureStackLayouts()
    }

    override func nextTapped() {
        if requiredFieldsSatisfied {
            AddTransactionFlowManager.shared.transactions = self.selectedTransactions
            super.nextTapped()
        } else {
            let ac = UIAlertController.forUnsatisfiedFields()
            present(ac, animated: true)
        }
    }

    // MARK: Filter Transactions

    private func filterTransactions(_ transactions: [Transaction]) -> [Transaction] {
        var filteredTransactions = self.filterTransactionsByTrader(transactions: transactions)
        filteredTransactions = self.filterTransactionByVehicle(transactions: filteredTransactions)

        if AddTransactionFlowManager.shared.transaction?.transactionType.name == TransactionTypes.payment.name {
            filteredTransactions = self.filterTransactionsForPayment(filteredTransactions)
        } else if AddTransactionFlowManager.shared.transaction?.transactionType.name == TransactionTypes.receipt.name {
            filteredTransactions = self.filterTransactionsForReceipt(filteredTransactions)
        }
//        print("<<<< Transactions before filtering: \(transactions.count) Transactions after filtering: \(filteredTransactions.count) >>>>")
        return filteredTransactions
    }

    private func filterTransactionsByTrader(transactions: [Transaction]) -> [Transaction] {
        guard let trader = AddTransactionFlowManager.shared.contact else {
            print("<<<<\(#function)>>>>")
            print("<<<< No Trader/Contact selected. Failed to filter >>>>")
            return transactions
        }
        let filteredTransactions: [Transaction] = transactions.filter { $0.trader != nil ? ($0.trader!.pk == trader.pk) : false }
        return filteredTransactions
    }

    private func filterTransactionByVehicle(transactions: [Transaction]) -> [Transaction] {
        guard let vehicle = AddTransactionFlowManager.shared.vehicle else {
            print("<<<<\(#function)>>>>")
            print("<<<< No Vehicle selected. Failed to filter >>>>")
            return transactions
        }
        let filteredTransactions: [Transaction] = transactions.filter { $0.vehicle != nil ? ($0.vehicle! == vehicle.pk) : false }
        return filteredTransactions
    }

    func filterTransactionsForPayment(_ transactions: [Transaction]) -> [Transaction] {
        return transactions.filter { $0.transactionType.name == TransactionTypes.purchase.name }
    }

    func filterTransactionsForReceipt(_ transactions: [Transaction]) -> [Transaction] {
        return transactions.filter { $0.transactionType.name == TransactionTypes.sale.name }
    }

    private func setSelectedTransaction(_ transaction: Transaction) {
        var valueExVAT: String {
            if let value = transaction.valueExVat, value != "" {
                return "€\(value)"
            } else {
                return "Value Unknown"
            }
        }

        transactionDescription = "#\(transaction.number): \(transaction.date.asString(dateStyle: .medium)) - \(valueExVAT)"

        setActiveFieldText(transactionDescription)
        self.selectedTransactions?.append(transaction)
        transactionTextField.endEditing(true)
        transactionTextField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
    }

    // MARK: - TextField Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
        setActiveFieldText("")
        transactionDescription = ""
    }

    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transaction = matchingTransactionsTableView.datasource.data[indexPath.row]
        setSelectedTransaction(transaction)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }

    func configureTableView() {
        matchingTransactionsTableView.rowHeight = UITableView.automaticDimension
        matchingTransactionsTableView.estimatedRowHeight = 110
    }

    func configureStackLayouts() {
        NSLayoutConstraint.activate([
            self.addTransactionsView.labelStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            self.addTransactionsView.labelStack.trailingAnchor.constraint(equalTo: addTransactionsView.leadingAnchor, constant: self.view.frame.size.width / 3),
            self.addTransactionsView.fieldStack.leadingAnchor.constraint(equalTo: self.addTransactionsView.labelStack.trailingAnchor),
            self.addTransactionsView.fieldStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10)
        ])
    }
}
