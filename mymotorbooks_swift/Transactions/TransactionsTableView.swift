//
//  TransactionsTableView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 12/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class TransactionsTableView: UITableView {
    var datasource = TransactionsDatasource()

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.dataSource = datasource
        self.register(TransactionCell.self, forCellReuseIdentifier: MyMotorBooksCellIdentifier.transaction.rawValue)
        fetchTransactions()
        self.reloadData()
    }

    convenience init(trader: Trader? = nil, vehicle: Int? = nil) {
        self.init()
        fetchTransactions(filterByTrader: trader, filterByVehicle: vehicle)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fetchTransactions(filterByTrader: Trader? = nil, filterByVehicle: Int? = nil) {
        MMBApiService.shared.fetchTransactions() { [weak self] (transactionsResult) in
            switch transactionsResult {
            case .success(let transactions):
                print("<<<< Successfully fetched Transactions! >>>>")
                self?.datasource.data = transactions.results

                if let trader = filterByTrader {
                    self?.filterTransactionsBy(trader: trader)
                }

                if let vehicle = filterByVehicle {
                    self?.filterTransactionsBy(vehicle: vehicle)
                }
                self?.reloadData()
            case .failure(let error):
                print("<<<< Failed to fetch Transactions with error: \(error) >>>>")
            }
        }
    }

    private func filterTransactionsBy(trader: Trader) {
        let filteredTransactions: [Transaction] = self.datasource.data.filter { $0.trader != nil ? ($0.trader!.pk == trader.pk) : false }
        self.datasource.data = filteredTransactions
        self.reloadData()
    }

    private func filterTransactionsBy(vehicle vehiclePk: Int) {
        let filteredTransactions: [Transaction] = self.datasource.data.filter { $0.vehicle != nil ? ($0.vehicle! == vehiclePk) : false }
        self.datasource.data = filteredTransactions
        self.reloadData()
    }
}
