//
//  TransactionsDatasource.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 9/20/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class TransactionsDatasource: NSObject, UITableViewDataSource {
    var data = [Transaction]()

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count == 0 {
            tableView.setEmptyMessage("No Matching Transactions Found")
        } else {
            tableView.restore()
        }
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyMotorBooksCellIdentifier.transaction.rawValue, for: indexPath) as! TransactionCell
        cell.item = data[indexPath.row]
        return cell
    }
}
