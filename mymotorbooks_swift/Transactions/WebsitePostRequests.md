#  WebsitePostRequests


## Purchase
### Vehicle
{
"pk": 4060,
"obj_owner": 1,
"transaction_type": 1,
"trader": 95,
"vat_rate": 1,
"sub_type": 7,
"expense_type": "OTHER",
"description": null,
"vehicle": 575,
"value_ex_vat": "123.10",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": null,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

### General
{
"pk": 4061,
"obj_owner": 1,
"transaction_type": 1,
"trader": 95,
"vat_rate": 1,
"sub_type": 8,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.20",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": null,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}


## Sale
### General
{
"pk": 4062,
"obj_owner": 1,
"transaction_type": 2,
"trader": 95,
"vat_rate": 1,
"sub_type": 9,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.30",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": null,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

## Payment
### vehiclePurchase
{
"pk": 4063,
"obj_owner": 1,
"transaction_type": 3,
"trader": 95,
"vat_rate": null,
"sub_type": 5,
"expense_type": "OTHER",
"description": null,
"vehicle": 527,
"value_ex_vat": "123.40",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

### vehicleExpense
{
"pk": 4064,
"obj_owner": 1,
"transaction_type": 3,
"trader": 95,
"vat_rate": null,
"sub_type": 7,
"expense_type": "OTHER",
"description": null,
"vehicle": 521,
"value_ex_vat": "123.50",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

### generalExpense
{
"pk": 4065,
"obj_owner": 1,
"transaction_type": 3,
"trader": 95,
"vat_rate": null,
"sub_type": 8,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.60",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

### VATPayment
{
"pk": 4066,
"obj_owner": 1,
"transaction_type": 3,
"trader": null,
"vat_rate": null,
"sub_type": 10,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.70",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": 10,
"is_vat_deductible_for_vehicle": false
}

### VRTPayment
{
"pk": 4067,
"obj_owner": 1,
"transaction_type": 3,
"trader": null,
"vat_rate": null,
"sub_type": 11,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.80",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 9,
"to_account": null,
"is_vat_deductible_for_vehicle": true
}

### transfer
{
"pk": 4068,
"obj_owner": 1,
"transaction_type": 3,
"trader": null,
"vat_rate": null,
"sub_type": 12,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "123.90",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": 4,
"is_vat_deductible_for_vehicle": false
}

### wagesEmployeeTax
{
"pk": 4069,
"obj_owner": 1,
"transaction_type": 3,
"trader": null,
"vat_rate": null,
"sub_type": 16,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "124.10",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}

### wagesEmployerTax
{
"pk": 4070,
"obj_owner": 1,
"transaction_type": 3,
"trader": null,
"vat_rate": null,
"sub_type": 17,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "124.20",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 4,
"to_account": null,
"is_vat_deductible_for_vehicle": false
}


## Receipt
### vehicleSale
{
"pk": 4071,
"obj_owner": 1,
"transaction_type": 4,
"trader": 95,
"vat_rate": null,
"sub_type": 6,
"expense_type": "OTHER",
"description": null,
"vehicle": 575,
"value_ex_vat": "124.30",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": null,
"to_account": 4,
"is_vat_deductible_for_vehicle": false
}

### generalSale
{
"pk": 4072,
"obj_owner": 1,
"transaction_type": 4,
"trader": 95,
"vat_rate": null,
"sub_type": 9,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "124.40",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": null,
"to_account": 4,
"is_vat_deductible_for_vehicle": false
}

### VATReceipt
{
"pk": 4073,
"obj_owner": 1,
"transaction_type": 4,
"trader": null,
"vat_rate": null,
"sub_type": 14,
"expense_type": "OTHER",
"description": null,
"vehicle": null,
"value_ex_vat": "124.50",
"parents": [],
"date": "2021-03-21",
"period_date": "2021-03-21",
"is_processed": true,
"is_estimate": false,
"from_account": 3,
"to_account": 4,
"is_vat_deductible_for_vehicle": false
}

