//
//  AddTransactionFlowManager.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/14/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

protocol AddTransactionFlowStep {
    func setupFields()
    func resetAllFields()
    func setActiveTextField(_ field: MMBTextField?)

    var allTextFields: [MMBTextField] { get }
    var activeTextField: MMBTextField? { get }
    var step: AddTransactionStep { get }
    var requiredFieldsSatisfied: Bool { get }
}

class AddTransactionFlowManager {

    private let startingVC = AddTransactionFlowDetails()
    private let viewManager = AddTransactionFlowVCManager()
    public var currentStep: AddTransactionStep = .details {
        willSet {
            guard currentStep != newValue else {
                print("<<<< currentStep - willSet: This is already the currentStep >>>>")
                return

            }
        }
    }
    // Details
    var transaction: Transaction?
    var transactionType: TransactionTypes?
    var transactionSubtype: TransactionTypes?
    // Vehicle
    var vehicle: Vehicle?
    // Transactions
    var transactions: [Transaction]?
    var value: Double?
    var createPaymentNow: Bool = false
    var valueIsEstimated: Bool?
    var vatDeductableFromVehicle: Bool?
    var vatRate: VatRate?
    // Bank
    var fromAccount: Account?
    var toAccount: Account?
    var date: Date?
    var vatPeriodDate: Date?
    var expenseType: ExpenseType?
    var description: String?
    var contact: Trader?

    var needsVehicleStep: Bool { return [TransactionTypes.vehiclePurchase, TransactionTypes.vehicleSale,
                                         TransactionTypes.vehicleExpense, TransactionTypes.VRTPayment, TransactionTypes.tradeIn]
        .contains(self.transactionSubtype)
    }
    var needsParentStep: Bool { return [TransactionTypes.payment, TransactionTypes.receipt].contains(self.transactionType) &&

        ![TransactionTypes.transfer, TransactionTypes.VRTPayment, TransactionTypes.VATPayment,
          TransactionTypes.VATReceipt, TransactionTypes.wagesEmployeeTax, TransactionTypes.wagesEmployerTax]
        .contains(self.transactionSubtype)
    }
    var needsVATRate: Bool { return self.transactionType == TransactionTypes.purchase && self.transactionSubtype != TransactionTypes.vehiclePurchase ||
        self.transactionType == TransactionTypes.sale && self.transactionSubtype != TransactionTypes.vehiclePurchase
    }
    var needsFromAccount: Bool {
        guard let type = AddTransactionFlowManager.shared.transactionType,
              let subtype = AddTransactionFlowManager.shared.transactionSubtype else { return false }
        return (subtype.type.needsFromAccount) && (type.type.needsFromAccount || AddTransactionFlowManager.shared.createPaymentNow)
    }
    var needsToAccount: Bool {
        guard let type = AddTransactionFlowManager.shared.transactionType,
              let subtype = AddTransactionFlowManager.shared.transactionSubtype else { return false }
        return (subtype.type.needsToAccount) && (type.type.needsToAccount || AddTransactionFlowManager.shared.createPaymentNow)
    }

    public let mainTransactionTypes: [TransactionTypes] = TransactionTypes.allCases.filter { !$0.type.isSubType }

    public let allowedPurchaseTypes: [TransactionTypes] = [TransactionTypes.vehicleExpense, TransactionTypes.generalExpense]

    public let allowedSaleTypes: [TransactionTypes] = [TransactionTypes.generalSale]

    public let allowedPaymentTypes: [TransactionTypes] = [TransactionTypes.vehiclePurchase, TransactionTypes.vehicleExpense, TransactionTypes.generalExpense, TransactionTypes.VATPayment, TransactionTypes.VRTPayment, TransactionTypes.transfer, TransactionTypes.wagesEmployeeTax, TransactionTypes.wagesEmployerTax]

    public let allowedReceiptTypes: [TransactionTypes] = [TransactionTypes.vehicleSale, TransactionTypes.generalSale, TransactionTypes.VATReceipt]

    public var allowedSubtypes = [TransactionTypes: [TransactionTypes]]()

    public var vatRates: [VatRate]?

    public static var shared = AddTransactionFlowManager()

    public var previousStep: AddTransactionStep? {
        switch currentStep {
            case .bank:
                return .value

            case .value:
                if self.needsParentStep {
                    return .parent
                } else if needsVehicleStep {
                    return .vehicle
                } else {
                    return .details
                }

            case .parent:
                if self.needsVehicleStep {
                    return .vehicle
                } else {
                    return .details
                }

            case .vehicle:
                return .details

            case .details:
                return nil
        }
    }

    public var nextStep: AddTransactionStep? {
        switch currentStep {
            case .details:
                if self.needsVehicleStep {
                    return .vehicle
                } else if self.needsParentStep {
                    return .parent
                } else {
                    return .value
                }

            case .vehicle:
                if needsParentStep {
                    return .parent
                } else {
                    return .value
                }

            case .parent:
                return .value

            case .value:
                return .bank

            case .bank:
                return nil
        }
    }

    private init() {
        getVatRates()
        allowedSubtypes = [
            TransactionTypes.purchase: allowedPurchaseTypes,
            TransactionTypes.sale: allowedSaleTypes,
            TransactionTypes.payment: allowedPaymentTypes,
            TransactionTypes.receipt: allowedReceiptTypes
        ]
        print("<<<< AddTransactionFlowManager has been init!!! >>>>")
    }

    public func printCurrentStep() {
        print("<<<< currentStep: \(currentStep.rawValue) - \(currentStep.description) >>>>")
    }

    private func setCurrentStep(to step: AddTransactionStep) {
        UserDefaults.standard.set(step.rawValue, forKey: String(describing: AddTransactionStep.self))
    }

    public func getCurrentStep() -> AddTransactionStep {
        let step = UserDefaults.standard.integer(forKey: String(describing: AddTransactionStep.self))
        guard let currentStep = AddTransactionStep.init(rawValue: step) else {
            print("<<<< Failed to init AddTransactionStep with rawValue. Returning step 1 (Details) >>>>")
            return AddTransactionStep.details
        }
        return currentStep
    }

    public func startFlow() -> UIViewController {
        return viewManager
    }

    public func endFlow() {
        // Leaving this here because I should probably do other stuff, reinit this maybe.
        AddTransactionFlowManager.shared.viewManager.endFlow()
        AddTransactionFlowManager.shared = AddTransactionFlowManager()
    }

    public func previous() {
        if let previous = self.previousStep {
            viewManager.previousView()
            self.currentStep = previous
            viewManager.updateNavButtons()
        }
    }

    public func next() {
        if let next = self.nextStep {
            self.currentStep = next
            viewManager.nextView()
        } else {
            postTransaction()
        }
    }

    func checkRequirements(forStep step: AddTransactionFlowStep) {
        print("<<<< step.requiredFieldsSatisfied \(step.requiredFieldsSatisfied) >>>>")
    }

    public func createTransactionPost() -> TransactionPost? {
        guard let type = self.transactionType, let subtype = self.transactionSubtype, let value = self.value else {
            return nil
        }

        var transactionData: TransactionData = TransactionData(transactionType: type.pk,
                                                               transactionSubtype: subtype.pk,
                                                               valueExVat: String(value)
        )

        if let trader = self.contact {
            transactionData.trader = trader.pk
        }

        if let rate = self.vatRate {
            transactionData.vatRate = rate.pk
        }

        if let vehicle = self.vehicle {
            transactionData.vehicle = vehicle.pk
        }

        if let parents = self.transactions {
            transactionData.parents = parents.map { $0.pk }
        }

        if self.valueIsEstimated ?? false {
            transactionData.isEstimate = self.valueIsEstimated
        }

        if self.vatDeductableFromVehicle ?? false {
            transactionData.isVatDeductibleForVehicle = self.vatDeductableFromVehicle
        }

        if let fromAccount = self.fromAccount {
            transactionData.fromAccount = fromAccount.pk
        }

        if let toAccount = self.toAccount {
            transactionData.toAccount = toAccount.pk
        }

        if let date = self.date {
            transactionData.date = date.formatDateForAPI()
        }

        if let vatDate = self.vatPeriodDate {
            transactionData.periodDate = vatDate.formatDateForAPI()
        }

        let transactionPost = TransactionPost(transaction: transactionData, createChild: self.createPaymentNow)

        return transactionPost
    }

    private func getVatRates() {
        MMBApiService.shared.fetchVATRates() { [weak self]
            (vatRatesResult) -> Void in
            switch vatRatesResult {
                case .success(let vatRates):
                    self?.vatRates = vatRates
                    print("<<<< VAT Rates: \(vatRates) >>>>")
                case .failure(let error):
                    print("<<<< Failed to fetch VAT Rates with error: \(error) >>>>")
            }
        }
    }

    private func postTransaction() {
        if let transactionPost = AddTransactionFlowManager.shared.createTransactionPost() {
            MMBApiService.shared.createTransaction(transactionData: transactionPost) { [weak self]
                (transactionResponse) -> Void in
                switch transactionResponse {
                    case let .success(newTransaction):
                        print("<<<< Successfully posted Transaction! Transaction: \(newTransaction) >>>>")
                        self?.viewManager.showSuccessAlert()
                    case let .failure(error):
                        print("<<<< Failed to post Transaction with error: \(error.localizedDescription) >>>>")
                        self?.viewManager.showFailedAlert()
                }
            }
        }
    }
}
