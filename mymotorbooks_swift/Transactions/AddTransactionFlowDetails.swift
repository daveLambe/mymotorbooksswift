//  AddTransactionFlowDetails.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/8/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class AddTransactionFlowDetails: AddTransactionSplitFieldViewController, AddTransactionFlowStep, UITextFieldDelegate, UIPickerViewDelegate , UIPickerViewDataSource {

    var addTransactionView: AddTransactionView!
    var tradersTableView: TradersTableView!

    var selectedType: TransactionTypes? {
        didSet {
            resetAllFields()
            addTransactionView.typePickerField.selectedOption = selectedType?.label
        }
    }

    var selectedSubtype: TransactionTypes?

    var selectedExpenseType: ExpenseType?

    var selectedContact: Trader?

    // MARK: - AddTransactionFlowStep Protocol Variables
    var step: AddTransactionStep = .details

    override var activeTextField: MMBTextField? {
        didSet {
            super.hideTableView(activeTextField != addTransactionView.contactTextField)
        }
    }
    var allTextFields: [MMBTextField] { return addTransactionView.getAllTextFields() }

    var requiredFieldsSatisfied: Bool {
        guard let _ = selectedType, let _ = selectedSubtype else { return false }
        if let needsContact = selectedSubtype!.type.needsTrader {
            return needsContact == (self.selectedContact != nil)
        }
        return true
    }
        // Add .needsTrader from new Subtype object

    init() {
        self.addTransactionView = AddTransactionView()
        self.tradersTableView = TradersTableView()
        super.init(textFieldContainedView: addTransactionView, tableViewContainedView: tradersTableView, step: step)
        fetchTraders()
        setupFields()
        setActiveTextField(nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bottomTableContainingView.delegate = self
        super.hideTableView(true)
    }

    override func nextTapped() {
        if self.requiredFieldsSatisfied {
            AddTransactionFlowManager.shared.printCurrentStep()
            AddTransactionFlowManager.shared.transactionType = self.selectedType
            AddTransactionFlowManager.shared.transactionSubtype = self.selectedSubtype
            AddTransactionFlowManager.shared.contact = self.selectedContact
            super.nextTapped()
        } else {
            let ac = UIAlertController.forUnsatisfiedFields()
            present(ac, animated: true)
        }
    }

    private func fetchTraders() {
        MMBApiService.shared.fetchTraders() { [weak self] (result: Result<Traders, MMBApiService.MMBApiServiceError>) in
            switch result {
            case .success(let tradersResult):
                print("<<<< Successfully fetched Traders! >>>>")
                self?.tradersTableView.models = tradersResult.results
            case .failure(let error):
                print("<<<< Error fetching Traders: \(error.localizedDescription) >>>>")
            }
        }
    }

    // MARK: - Textfield Methods
    func setupFields() {
        super.activeTextField = self.activeTextField
        addTransactionView.subTypePickerField.isEnabled = false
        addTransactionView.expenseTypeFieldHidden(true)
        addTransactionView.optionalInformationFieldsHidden(true)
        // Maybe not needed with activeTextField didSet
        //        addTransactionView.contactTextField.addTarget(self, action: #selector(contactFieldTapped), for: .allTouchEvents)
        addTransactionView.getAllTextFields().forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
            if let pickerView = $0.inputView as? UIPickerView {
                pickerView.delegate = self
                pickerView.dataSource = self
                pickerView.tag = $0.tag
                pickerView.reloadAllComponents()
            }
        }
    }

    func setActiveTextField(_ field: MMBTextField?) {
        super.activeTextField = activeTextField
        activeTextField = field
    }

    func resetAllFields() {
        addTransactionView.getAllTextFields(includingType: false).forEach {
            $0.selectedOption = nil
            $0.text = nil
        }
        addTransactionView.subTypePickerField.isEnabled = true
        addTransactionView.expenseTypeFieldHidden(true)
        addTransactionView.optionalInformationFieldsHidden(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = nil
        if let pickerField = textField as? MMBPickerTextField {
            activeTextField = pickerField
            if pickerField.selectedOption == nil {
                pickerView(pickerField.pickerView, didSelectRow: 0, inComponent: 0)
            }
        }
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
    }

    // MARK: - PickerView Methods
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            selectedType = AddTransactionFlowManager.shared.mainTransactionTypes[row]
            setActiveFieldText(selectedType!.label)
        } else {
            guard let type = selectedType else { return }

            switch pickerView.tag {
            case 2:
                guard let subtypes = AddTransactionFlowManager.shared.allowedSubtypes[type] else { return }
                let subtype = subtypes[row]

                selectedSubtype = subtype
                setActiveFieldText(subtype.label)
                addTransactionView.descriptionFieldHidden(false)

                if subtype == TransactionTypes.purchase &&
                    [TransactionTypes.vehicleExpense, TransactionTypes.generalExpense].contains(subtype) {
                    addTransactionView.expenseTypeFieldHidden(false)
                }

                if let needsTrader = subtype.type.needsTrader {
                    addTransactionView.contactFieldHidden(!needsTrader)
                }
            case 3:
                guard let _ = self.selectedType, let _ = self.selectedSubtype else {
                    print("<<<< Type and/or subtype not set. Returning >>>>")
                    return
                }
                selectedExpenseType = ExpenseType.init(rawValue: row)
                setActiveFieldText(selectedExpenseType!.label)
            case 4: return
            case 5: return
            default: return
            }
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1: return AddTransactionFlowManager.shared.mainTransactionTypes.count
        case 2:
            guard let type = selectedType else { return 0 }
            switch type {
            case .purchase: return AddTransactionFlowManager.shared.allowedPurchaseTypes.count
            case .sale: return AddTransactionFlowManager.shared.allowedSaleTypes.count
            case .payment: return AddTransactionFlowManager.shared.allowedPaymentTypes.count
            case .receipt: return AddTransactionFlowManager.shared.allowedReceiptTypes.count
            default: return 0
            }
        case 3: return ExpenseType.allCases.count
        default: return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1: return AddTransactionFlowManager.shared.mainTransactionTypes[row].label
        case 2:
            guard let type = selectedType else { return "" }
            switch type {
            case .purchase: return AddTransactionFlowManager.shared.allowedPurchaseTypes[row].label
            case .sale: return AddTransactionFlowManager.shared.allowedSaleTypes[row].label
            case .payment: return AddTransactionFlowManager.shared.allowedPaymentTypes[row].label
            case .receipt: return AddTransactionFlowManager.shared.allowedReceiptTypes[row].label
            default: return ""
            }
        case 3: return ExpenseType.allCases[row].description
        default: return ""
        }
    }

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView.tag {
        case 1: return AddTransactionFlowManager.shared.mainTransactionTypes[row].label.asMMBAttributedText(.pickerView)
        case 2:
            guard let type = selectedType else { return "".asMMBAttributedText(.pickerView) }
            switch type {
            case .purchase: return AddTransactionFlowManager.shared.allowedPurchaseTypes[row].label.asMMBAttributedText(.pickerView)
            case .sale: return AddTransactionFlowManager.shared.allowedSaleTypes[row].label.asMMBAttributedText(.pickerView)
            case .payment: return AddTransactionFlowManager.shared.allowedPaymentTypes[row].label.asMMBAttributedText(.pickerView)
            case .receipt: return AddTransactionFlowManager.shared.allowedReceiptTypes[row].label.asMMBAttributedText(.pickerView)
            default: return "".asMMBAttributedText(.pickerView)
            }
        case 3: return ExpenseType.allCases[row].description.asMMBAttributedText(.pickerView)
        default: return "".asMMBAttributedText(.pickerView)
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let trader = tradersTableView.datasource.data[indexPath.row]
        let trader = tradersTableView.models[indexPath.row]
        self.selectedContact = trader

        setActiveFieldText(trader.name)
        addTransactionView.contactTextField.endEditing(true)
        addTransactionView.contactTextField.disableKeyboardInput(cursorColor: MyMotorBooksColor.pink.rawValue)
    }
}
