//
//  AddTransactionFlowBank.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 12/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class AddTransactionFlowBank: AddTransactionViewController, AddTransactionFlowStep,  UITextFieldDelegate {

    var fromAccounts = [Account]()
    var toAccounts = [Account]()
    var selectedDate: Date = Date.today
    var selectedVATPeriodDate: Date = Date.today
    var selectedFromAccount: Account?
    var selectedToAccount: Account?

    static let fromAccountFieldTag = 0
    static let toAccountFieldTag = 1

    private enum textFieldTag: Int {
        case fromAccount = 0
        case toAccount = 1
    }

    // MARK: - AddTransactionFlowStep Protocol Variables
    var requiredFieldsSatisfied: Bool { return (AddTransactionFlowManager.shared.needsFromAccount == (self.selectedFromAccount != nil)) &&
        (AddTransactionFlowManager.shared.needsToAccount == (self.selectedToAccount != nil)) }

    var step: AddTransactionStep = .bank

    var allTextFields: [MMBTextField] { return [fromAccountTextField, toAccountTextField] }

    let datePickerLabel: UILabel = {
        let label = UILabel()
        label.text = "Date"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // Make UITextField with toolbar & datepicker input
    let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.tintColor = MyMotorBooksColor.pink.rawValue
        datePicker.datePickerMode = .date
        datePicker.tag = 1
        return datePicker
    }()

    let vatPeriodDateLabel: UILabel = {
        let label = UILabel()
        label.text = "VAT Period Date"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // Make UITextField with toolbar & datepicker input
    let vatPeriodDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.tintColor = MyMotorBooksColor.pink.rawValue
        datePicker.datePickerMode = .date
        datePicker.tag = 2
        return datePicker
    }()

    let fromAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "From Account"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let fromAccountTextField: MMBPickerTextField = {
        let textField = MMBPickerTextField(placeholderText: "", isRequiredField: true)
        textField.tag = textFieldTag.fromAccount.rawValue
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    let toAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "To Account"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let toAccountTextField: MMBPickerTextField = {
        let textField = MMBPickerTextField(placeholderText: "", isRequiredField: true)
        textField.tag = textFieldTag.toAccount.rawValue
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    init() {
        super.init(step: .value)
        fetchAccounts()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let viewTappedRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(viewTappedRecognizer)

        self.view.addSubview(datePickerLabel)
        self.view.addSubview(datePicker)
        self.view.addSubview(vatPeriodDateLabel)
        self.view.addSubview(vatPeriodDatePicker)
        self.view.addSubview(fromAccountLabel)
        self.view.addSubview(fromAccountTextField)
        self.view.addSubview(toAccountLabel)
        self.view.addSubview(toAccountTextField)

        self.nextButton.setTitle("Save", for: .normal)
        self.nextButton.setAttributedTitle(
            "Save".asAttributedText(withAttributes:
                                        [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                                         NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)

        self.datePicker.addTarget(self, action: #selector(dateChanged(picker:)), for: .valueChanged)
        self.vatPeriodDatePicker.addTarget(self, action: #selector(dateChanged(picker:)), for: .valueChanged)

        setupFields()
        setActiveTextField(nil)
        configureLayout()
        hideUnnecessaryFields()
    }

    override func nextTapped() {
        if requiredFieldsSatisfied {
            AddTransactionFlowManager.shared.fromAccount = self.selectedFromAccount
            AddTransactionFlowManager.shared.toAccount = self.selectedToAccount
            AddTransactionFlowManager.shared.date = self.selectedDate
            AddTransactionFlowManager.shared.vatPeriodDate = self.selectedVATPeriodDate
            super.nextTapped()
        } else {
            let ac = UIAlertController.forUnsatisfiedFields()
            present(ac, animated: true)
        }
    }

    // MARK: - Gesture Recognizers
    @objc override func viewTapped() {
        print("<<<< VIEW TAPPED!!!! \(#function) >>>>")
        //        activeTextField?.resignFirstResponder()
    }

    @objc func dateChanged(picker: UIDatePicker) {
        switch picker.tag {
        case 1: self.selectedDate = picker.date
        default: self.selectedVATPeriodDate = picker.date
        }
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Textfield Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("<<<<\(#function)>>>>")
        textField.placeholder = nil
        if let pickerField = textField as? MMBPickerTextField {
            activeTextField = pickerField
            if pickerField.selectedOption == nil {
                pickerView(pickerField.pickerView, didSelectRow: 0, inComponent: 0)
            }
        }
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
    }

    override func textFieldDidEndEditing(_ textField: UITextField) {
        print("<<<<\(#function)>>>>")
        guard let activeField = activeTextField else {
            print("<<<< NO ACTIVE TEXT FIELD >>>>")
            self.view.endEditing(true)
            return
        }

        if let text = activeField.text, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            setActiveFieldText(text)
            // See if you need this
            //            setActiveFieldSelectedOption()
        } else {
            print("<<<< SETTING PLACEHOLDER TEXT FOR FIELD \(activeField) >>>>")
            setActiveFieldPlaceholder()
        }
        activeTextField = nil
        self.view.endEditing(true)
    }

    func setupFields() {
        super.activeTextField = self.activeTextField
        allTextFields.forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
            if let pickerView = $0.inputView as? UIPickerView {
                pickerView.delegate = self
                pickerView.dataSource = self
                pickerView.tag = $0.tag
                pickerView.reloadAllComponents()
            }
        }
    }

    func resetAllFields() {
        allTextFields.forEach {
            $0.selectedOption = nil
            $0.text = nil
        }
    }

    func setActiveTextField(_ field: MMBTextField?) {
        super.activeTextField = activeTextField
        activeTextField = field
    }

    private func hideUnnecessaryFields() {
//        [fromAccountLabel, fromAccountTextField].forEach { $0.isHidden = (AddTransactionFlowManager.shared.needsFromAccount) }

        if !AddTransactionFlowManager.shared.needsFromAccount {
            self.fromAccountLabel.isHidden = true
            self.fromAccountTextField.isHidden = true
        }

        if !AddTransactionFlowManager.shared.needsToAccount {
            self.toAccountLabel.isHidden = true
            self.toAccountTextField.isHidden = true
        }

//        [toAccountLabel, toAccountTextField].forEach { $0.isHidden = (AddTransactionFlowManager.shared.needsToAccount) }

        [vatPeriodDateLabel, vatPeriodDatePicker].forEach { $0.isHidden =
            AddTransactionFlowManager.shared.transactionSubtype?.name != TransactionTypes.VATPayment.name }

        if AddTransactionFlowManager.shared.transactionSubtype?.name != TransactionTypes.VATPayment.name {
            vatPeriodDateLabel.isHidden = true
            vatPeriodDatePicker.isHidden = true
        }
    }

    private func fetchAccounts() {
        MMBApiService.shared.fetchAccounts() { [weak self] (accountsResult) in
            switch accountsResult {
            case .success(let allAccounts):
                self?.filterAccounts(accounts: allAccounts.results)
            case .failure(let error):
                print("<<<< Failed to fetch Accounts: \(error.localizedDescription) >>>>")
            }
        }
    }

    private func filterAccounts(accounts: [Account]) {
        let fromAccounts = accounts.filter() { account in
            switch AddTransactionFlowManager.shared.transactionSubtype {
            case .VRTPayment: return account.accountType == "VRT Control"
            case .VATReceipt: return account.isVatAccount
            default: return account.accountType == "Bank Account"
            }
        }

        let toAccounts = accounts.filter() { account in
            switch AddTransactionFlowManager.shared.transactionSubtype {
            case .VATPayment: return account.isVatAccount
            case .transfer: return account.accountType == "VRT Control" || account.accountType == "Bank Account"
            default: return account.accountType == "Bank Account"
            }
        }
        self.fromAccounts = fromAccounts
        self.toAccounts = toAccounts
    }

    func configureLayout() {
        datePickerLabel.translatesAutoresizingMaskIntoConstraints = false
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        vatPeriodDateLabel.translatesAutoresizingMaskIntoConstraints = false
        vatPeriodDatePicker.translatesAutoresizingMaskIntoConstraints = false
        fromAccountLabel.translatesAutoresizingMaskIntoConstraints = false
        fromAccountTextField.translatesAutoresizingMaskIntoConstraints = false
        toAccountLabel.translatesAutoresizingMaskIntoConstraints = false
        toAccountTextField.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            datePickerLabel.topAnchor.constraint(equalTo: stepLabel.bottomAnchor, constant: 20),
            datePickerLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20),

            datePicker.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            datePicker.centerYAnchor.constraint(equalTo: datePickerLabel.centerYAnchor),

            vatPeriodDateLabel.topAnchor.constraint(equalTo: datePickerLabel.bottomAnchor, constant: 40),
            vatPeriodDateLabel.leadingAnchor.constraint(equalTo: datePickerLabel.leadingAnchor),

            vatPeriodDatePicker.trailingAnchor.constraint(equalTo: datePicker.trailingAnchor),
            vatPeriodDatePicker.centerYAnchor.constraint(equalTo: vatPeriodDateLabel.centerYAnchor),

            fromAccountLabel.topAnchor.constraint(equalTo: vatPeriodDateLabel.bottomAnchor, constant: 40),
            fromAccountLabel.leadingAnchor.constraint(equalTo: datePickerLabel.leadingAnchor),

            fromAccountTextField.trailingAnchor.constraint(equalTo: datePicker.trailingAnchor),
            fromAccountTextField.centerYAnchor.constraint(equalTo: fromAccountLabel.centerYAnchor),
            fromAccountTextField.widthAnchor.constraint(equalToConstant: 200),

            toAccountLabel.topAnchor.constraint(equalTo: fromAccountLabel.bottomAnchor, constant: 40),
            toAccountLabel.leadingAnchor.constraint(equalTo: datePickerLabel.leadingAnchor),

            toAccountTextField.trailingAnchor.constraint(equalTo: datePicker.trailingAnchor),
            toAccountTextField.centerYAnchor.constraint(equalTo: toAccountLabel.centerYAnchor),
            toAccountTextField.widthAnchor.constraint(equalToConstant: 200),
        ])
    }
}

extension AddTransactionFlowBank: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case textFieldTag.fromAccount.rawValue: return fromAccounts.count
        default: return toAccounts.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let accounts = pickerView.tag == textFieldTag.fromAccount.rawValue ? fromAccounts: toAccounts
        guard let account = accounts[safe: row] else {
            return "No Bank Accounts Found"
        }
        return account.description
    }

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) ->
    NSAttributedString? {
        let accounts = pickerView.tag == textFieldTag.fromAccount.rawValue ? fromAccounts: toAccounts
        guard let account = accounts[safe: row] else {
            return "No Bank Accounts Found".asMMBAttributedText(.pickerView)
        }
        return account.description.asMMBAttributedText(.pickerView)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectingFromAccount: Bool = pickerView.tag == textFieldTag.fromAccount.rawValue
        let accounts: [Account] = selectingFromAccount ? fromAccounts: toAccounts

        guard let account = accounts[safe: row] else {
            print("<<<< Index out of range. Not setting field text >>>>")
            return
        }

        setActiveFieldText(account.description)
        if selectingFromAccount {
            self.selectedFromAccount = account
        } else {
            self.selectedToAccount = account
        }
    }
}
