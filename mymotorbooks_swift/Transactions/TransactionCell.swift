//
//  TransactionCell.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 9/20/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

enum TransactionLabelPrefix: String {
    case type = "Type: "
    case subtype = "Subtype: "
    case contact = "Contact: "
    case date = "Date: "
    case value = "Value: "
    case number = "# "
}

class TransactionCell: BaseTableViewCell<Transaction> {
    var sideBySideValues: Bool = false

    override var item: Transaction? {
        didSet {
            guard let transaction = item else { return }
            setTransactionLabelsText(transaction)
            setTransactionLabelsStyle(transaction)
        }
    }

    private let typeLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let subtypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let contactLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let valueLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let numberLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let containingStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 4
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private let infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 2
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private let leftInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private let rightInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private let transactionImageView: UIImageView = {
        let imageView = UIImageView()
        let defaultImage = UIImage(systemName: "creditcard")
        imageView.image = defaultImage
        imageView.contentMode = .scaleAspectFit
        imageView.setCorner(radius: 10)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let cellSeperator: UIView = {
        let seperatorView = UIView()
        seperatorView.backgroundColor = MyMotorBooksColor.pink.rawValue
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        return seperatorView
    }()

    //    required init(asSmallCell: Bool) {
    //        self.smallCell = asSmallCell
    //        super.init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    //    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = MyMotorBooksColor.black.rawValue
        infoStackView.addArrangedSubview(typeLabel)
        infoStackView.addArrangedSubview(subtypeLabel)
        infoStackView.addArrangedSubview(contactLabel)
        infoStackView.addArrangedSubview(dateLabel)
        infoStackView.addArrangedSubview(valueLabel)

        //        leftInfoStackView.addArrangedSubview(typeLabel)
        //        leftInfoStackView.addArrangedSubview(subtypeLabel)
        //
        //
        //        rightInfoStackView.addArrangedSubview(contactLabel)
        //        rightInfoStackView.addArrangedSubview(dateLabel)
        //        rightInfoStackView.addArrangedSubview(valueLabel)
        //
        //        containingStackView.addArrangedSubview(leftInfoStackView)
        //        containingStackView.addArrangedSubview(rightInfoStackView)

        contentView.addSubview(transactionImageView)
        contentView.addSubview(infoStackView)
        //        contentView.addSubview(containingStackView)
        contentView.addSubview(numberLabel)
        contentView.addSubview(cellSeperator)
        //        containingStackView.isHidden = true
        configureLayout()
    }

    convenience init(asSmallCell: Bool) {
        self.init()
        if asSmallCell {
            setFontTo(UIFont.boldSystemFont(ofSize: 12))
            infoStackView.removeFromSuperview()


            leftInfoStackView.addArrangedSubview(numberLabel)
            leftInfoStackView.addArrangedSubview(subtypeLabel)


            rightInfoStackView.addArrangedSubview(contactLabel)
            rightInfoStackView.addArrangedSubview(dateLabel)
            rightInfoStackView.addArrangedSubview(valueLabel)

            containingStackView.addArrangedSubview(leftInfoStackView)
            containingStackView.addArrangedSubview(rightInfoStackView)
            contentView.addSubview(containingStackView)
            //            infoStackView.isHidden = asSmallCell
            //            containingStackView.isHidden = !asSmallCell
            //            leftInfoStackView.addArrangedSubview(typeLabel)
            //            leftInfoStackView.addArrangedSubview(subtypeLabel)
            //
            //
            //            rightInfoStackView.addArrangedSubview(contactLabel)
            //            rightInfoStackView.addArrangedSubview(dateLabel)
            //            rightInfoStackView.addArrangedSubview(valueLabel)
            //
            //            containingStackView.addArrangedSubview(leftInfoStackView)
            //            containingStackView.addArrangedSubview(rightInfoStackView)
            //            leftInfoStackView.arrangedSubviews.forEach {
            //                leftInfoStackView.removeArrangedSubview($0)
            //                $0.removeFromSuperview()
            //            }
            //            rightInfoStackView.arrangedSubviews.forEach {
            //                leftInfoStackView.removeArrangedSubview($0)
            //                $0.removeFromSuperview()
            //            }
            //            leftInfoStackView.addArrangedSubview(subtypeLabel)
            //            leftInfoStackView.addArrangedSubview(contactLabel)
            //            rightInfoStackView.addArrangedSubview(dateLabel)
            //            rightInfoStackView.addArrangedSubview(valueLabel)
            //            contentView.reloadInputViews()
            configureLayout(asSmallCell: true)
        }

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setFontTo(_ font: UIFont) {
        [typeLabel,
         subtypeLabel,
         contactLabel,
         dateLabel,
         valueLabel,
         numberLabel].forEach {
            $0.font = font
         }
    }

    private func setTransactionLabelsText(_ transaction: Transaction) {
        typeLabel.text = TransactionLabelPrefix.type.rawValue + "\(transaction.transactionType.label)"
        subtypeLabel.text = TransactionLabelPrefix.subtype.rawValue + "\(transaction.transactionSubtype.label)"
        contactLabel.text = TransactionLabelPrefix.contact.rawValue + "\(transaction.trader?.name ?? "Unknown")"
        dateLabel.text = TransactionLabelPrefix.date.rawValue + "\(transaction.date.asString(dateStyle: .medium))"
        valueLabel.text = TransactionLabelPrefix.value.rawValue + (transaction.valueExVat != nil ? "\(String(transaction.valueExVat!))" : "Unknown")
        numberLabel.text = TransactionLabelPrefix.number.rawValue + "\(String(transaction.number))"
    }

    private func setTransactionLabelsStyle(_ transaction: Transaction) {
        // Sets amount text as green if money in & red if money out
        guard let value = transaction.valueExVat else { return }
        let valueTextColor: UIColor = transaction.transactionType.direction == .OUT ? .red : .green
        let prefixAttributedText = TransactionLabelPrefix.value.rawValue.asAttributedText(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)])
        let valueAttributedText = String(value).asAttributedText(withAttributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                                                                                  NSAttributedString.Key.foregroundColor: valueTextColor])
        let valueLabelAttributedString = NSMutableAttributedString()
        valueLabelAttributedString.append(prefixAttributedText)
        valueLabelAttributedString.append(valueAttributedText)

        valueLabel.attributedText = valueLabelAttributedString
    }

    private func configureLayout(asSmallCell: Bool = false) {
        NSLayoutConstraint.activate([
            transactionImageView.centerYAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.centerYAnchor),
            transactionImageView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 4),
            transactionImageView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 4),
            transactionImageView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -4),
            transactionImageView.widthAnchor.constraint(equalToConstant: 45),

            numberLabel.centerYAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.centerYAnchor),
            numberLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -4),

            cellSeperator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left),
            cellSeperator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: self.separatorInset.right),
            cellSeperator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            cellSeperator.heightAnchor.constraint(equalToConstant: 1)
        ])
        if asSmallCell {
            NSLayoutConstraint.activate([
                                            containingStackView.centerYAnchor.constraint(equalTo: transactionImageView.centerYAnchor),
                                            containingStackView.leadingAnchor.constraint(equalTo: transactionImageView.trailingAnchor, constant: 4)])
        } else {
            NSLayoutConstraint.activate([
                infoStackView.centerYAnchor.constraint(equalTo: transactionImageView.centerYAnchor),
                infoStackView.leadingAnchor.constraint(equalTo: transactionImageView.trailingAnchor, constant: 4)
            ])
        }
    }
}
