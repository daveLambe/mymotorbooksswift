//
//  AddTransactionFlowValue.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 12/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

final class AddTransactionFlowValue: AddTransactionViewController, AddTransactionFlowStep,  UITextFieldDelegate {

    // Find out when this is hidden. Definitely appears for Type: Purchase. Subtype: Vehicle Expense
    let vatDeductableFromVehicleCheckbox = MMBCheckbox(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
    let createPaymentNowCheckbox = MMBCheckbox(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
    let valueIsEstimatedCheckbox = MMBCheckbox(frame: CGRect(x: 0, y: 0, width: 28, height: 28))

    var enteredValue: Double?

    var selectedVATRate: VatRate?

    var vatDeductableFromVehicle: Bool { return vatDeductableFromVehicleCheckbox.isChecked && !self.vatDeductableFromVehicleCheckbox.isHidden }

    var createPaymentNow: Bool {
        set {
            self.valueIsEstimatedLabel.isHidden = newValue
            self.valueIsEstimatedCheckbox.isHidden = newValue
        }
        get {
            return createPaymentNowCheckbox.isChecked &&
                !createPaymentNowCheckbox.isHidden && !createPaymentNowLabel.isHidden
        }
    }

    var valueIsEstimated: Bool { return valueIsEstimatedCheckbox.isChecked && !self.createPaymentNow }

//    var vatDeductibleForVehicle: Bool { return va}

    // MARK: - AddTransactionFlowStep Protocol Variables
    var requiredFieldsSatisfied: Bool {
        return self.enteredValue != nil && ((self.selectedVATRate != nil) == AddTransactionFlowManager.shared.needsVATRate)

    }

    var step: AddTransactionStep = .value

    var allTextFields: [MMBTextField] { return [totalPaidTextField, totalPaidTextField, vatRateTextField] }

    let totalPaidLabel: UILabel = {
        let label = UILabel()
        var labelText: String {
            if [TransactionTypes.purchase, TransactionTypes.sale].contains(AddTransactionFlowManager.shared.transactionType) && AddTransactionFlowManager.shared.transactionSubtype != TransactionTypes.vehiclePurchase {
                return "Value (Ex. VAT)"
            } else if AddTransactionFlowManager.shared.transactionSubtype == TransactionTypes.tradeIn {
                return "Valued At"
            } else {
                return "Total Paid"
            }
        }
        label.text = labelText
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let totalPaidTextField: MMBTextField = {
        let textField = MMBTextField(placeholderText: "", isRequiredField: true)
        textField.keyboardType = .decimalPad
        textField.tag = 1
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    let createPaymentNowLabel: UILabel = {
        let label = UILabel()
        label.text = "Create payment now"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let vatDeductableFromSaleLabel: UILabel = {
        let label = UILabel()
        label.text = "VAT Deductable from Sale"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let valueIsEstimatedLabel: UILabel = {
        let label = UILabel()
        label.text = "Value is estimated"
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let vatRateLabel: UILabel = {
        let label = UILabel()
        label.text = "VAT Rate"
        label.isHidden = !AddTransactionFlowManager.shared.needsVATRate
        label.styleAsMMBLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let vatRateTextField: MMBPickerTextField = {
        let field = MMBPickerTextField(placeholderText: "")
        field.tag = 2
        field.isHidden = !AddTransactionFlowManager.shared.needsVATRate
        return field
    }()

    init() {
        super.init(step: .value)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let viewTappedRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(viewTappedRecognizer)

        self.view.addSubview(totalPaidLabel)
        self.view.addSubview(totalPaidTextField)
        self.view.addSubview(vatRateLabel)
        self.view.addSubview(vatRateTextField)

        self.view.addSubview(vatDeductableFromSaleLabel)
        self.view.addSubview(vatDeductableFromVehicleCheckbox)
        self.view.addSubview(createPaymentNowLabel)
        self.view.addSubview(createPaymentNowCheckbox)

        self.view.addSubview(valueIsEstimatedLabel)
        self.view.addSubview(valueIsEstimatedCheckbox)

        self.vatDeductableFromVehicleCheckbox.addTarget(self, action: #selector(vatDeductableFromSaleTapped), for: .touchUpInside)
        self.createPaymentNowCheckbox.addTarget(self, action: #selector(createPaymentNowTapped), for: .touchUpInside)
        self.valueIsEstimatedCheckbox.addTarget(self, action: #selector(valueIsEstimatedTapped), for: .touchUpInside)

        if !AddTransactionFlowManager.shared.needsVATRate {
            self.vatRateLabel.isHidden = true
            self.vatRateTextField.isHidden = true
        }

        setupFields()
        setActiveTextField(nil)
        configureLayout()
        valueIsEstimatedCheckbox.setResizedImage()
        hideUnnecessaryElements()
    }

    func setSelectedOptions() {
        AddTransactionFlowManager.shared.value = self.enteredValue!
//        if self.vatDeductableFromSale {
//            AddTransactionFlowManager.shared.vatDeductableFromSale = self.vatDeductableFromVehicle
//        }
//        if self.createPaymentNow == true {
            AddTransactionFlowManager.shared.createPaymentNow = self.createPaymentNow
//        }
//        if self.valueIsEstimated {
            AddTransactionFlowManager.shared.valueIsEstimated = self.valueIsEstimated
//        }

        AddTransactionFlowManager.shared.vatDeductableFromVehicle = self.vatDeductableFromVehicle

        if self.selectedVATRate != nil {
            AddTransactionFlowManager.shared.vatRate = self.selectedVATRate
        }
    }

    override func nextTapped() {
        if requiredFieldsSatisfied {
            self.setSelectedOptions()
            super.nextTapped()
        } else {
            let ac = UIAlertController.forUnsatisfiedFields()
            present(ac, animated: true)
        }
    }

    private func hideUnnecessaryElements() {
        guard let transaction = AddTransactionFlowManager.shared.transaction, let subtype = AddTransactionFlowManager.shared.transactionSubtype else {
            print("<<<<\(#function)>>>>")
            print("<<<< Transaction Type or Subtype not set. Failed to hide elements >>>>")
            return
        }
        if !(transaction.transactionType.name == TransactionTypes.purchase.name && subtype.name == TransactionTypes.vehicleExpense.name) || transaction.isProcessed {
            self.vatDeductableFromSaleLabel.isHidden = true
            self.vatDeductableFromVehicleCheckbox.isHidden = true
        }

        if [TransactionTypes.purchase.name, TransactionTypes.sale.name].contains(transaction.transactionType.name) || transaction.isProcessed {
            self.createPaymentNowLabel.isHidden = true
            self.createPaymentNowCheckbox.isHidden = true
        }
    }

    @objc func vatDeductableFromSaleTapped() {
        self.vatDeductableFromVehicleCheckbox.isChecked.toggle()
//        self.createPaymentNow = self.createPaymentNowCheckbox.isChecked
    }

    @objc func createPaymentNowTapped() {
        self.createPaymentNowCheckbox.isChecked.toggle()
//        self.createPaymentNow = self.createPaymentNowCheckbox.isChecked
    }

    @objc func valueIsEstimatedTapped() {
        self.valueIsEstimatedCheckbox.isChecked.toggle()
    }

    // MARK: - Gesture Recognizers
    @objc override func viewTapped() {
        print("<<<< VIEW TAPPED!!!! \(#function) >>>>")
//        activeTextField?.resignFirstResponder()
    }

    // MARK: - Textfield Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("<<<<\(#function)>>>>")
        textField.placeholder = nil
        if let pickerField = textField as? MMBPickerTextField {
            activeTextField = pickerField
            if self.selectedVATRate == nil {
                pickerView(pickerField.pickerView, didSelectRow: 0, inComponent: 0)
            }
        }
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
    }

    override func textFieldDidEndEditing(_ textField: UITextField) {
        print("<<<<\(#function)>>>>")
        guard let activeField = activeTextField else {
            print("<<<< NO ACTIVE TEXT FIELD >>>>")
            self.view.endEditing(true)
            return
        }

        if let text = activeField.text, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            setActiveFieldText(text)
            if activeField.tag == 1 {
                guard let value = Double(text) else {
                    print("<<<< FAILED TO CREATE DOUBLE FROM ENTERED VALUE. Entered value: \(text) >>>>")
                    self.enteredValue = nil
                    return }
                self.enteredValue = value
            }
            // See if you need this
//            setActiveFieldSelectedOption()
        } else {
            print("<<<< SETTING PLACEHOLDER TEXT FOR FIELD \(activeField) >>>>")
            setActiveFieldPlaceholder()
        }
        activeTextField = nil
        self.view.endEditing(true)
    }

    func setupFields() {
        super.activeTextField = self.activeTextField
        allTextFields.forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
            if let pickerView = $0.inputView as? UIPickerView {
                pickerView.delegate = self
                pickerView.dataSource = self
                pickerView.reloadAllComponents()
            }
        }
    }

    func resetAllFields() {
        allTextFields.forEach {
            $0.selectedOption = nil
            $0.text = nil
        }
    }

    func setActiveTextField(_ field: MMBTextField?) {
        super.activeTextField = activeTextField
        activeTextField = field
    }

    func configureLayout() {
        totalPaidLabel.translatesAutoresizingMaskIntoConstraints = false
        totalPaidTextField.translatesAutoresizingMaskIntoConstraints = false
        vatDeductableFromSaleLabel.translatesAutoresizingMaskIntoConstraints = false
        vatDeductableFromVehicleCheckbox.translatesAutoresizingMaskIntoConstraints = false
        createPaymentNowLabel.translatesAutoresizingMaskIntoConstraints = false
        createPaymentNowCheckbox.translatesAutoresizingMaskIntoConstraints = false
        valueIsEstimatedLabel.translatesAutoresizingMaskIntoConstraints = false
        valueIsEstimatedCheckbox.translatesAutoresizingMaskIntoConstraints = false
        vatRateLabel.translatesAutoresizingMaskIntoConstraints = false
        vatRateTextField.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.totalPaidLabel.topAnchor.constraint(equalTo: self.stepLabel.bottomAnchor, constant: 20),
            self.totalPaidLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20),

            totalPaidTextField.leadingAnchor.constraint(equalTo: createPaymentNowCheckbox.leadingAnchor),
            totalPaidTextField.widthAnchor.constraint(equalToConstant: 120),
            self.totalPaidTextField.centerYAnchor.constraint(equalTo: self.totalPaidLabel.centerYAnchor),

            self.vatDeductableFromSaleLabel.topAnchor.constraint(equalTo: self.totalPaidLabel.bottomAnchor, constant: 40),
            self.vatDeductableFromSaleLabel.leadingAnchor.constraint(equalTo: self.totalPaidLabel.leadingAnchor),

            self.vatDeductableFromVehicleCheckbox.centerYAnchor.constraint(equalTo: self.vatDeductableFromSaleLabel.centerYAnchor),
            self.vatDeductableFromVehicleCheckbox.leadingAnchor.constraint(equalTo: self.vatDeductableFromSaleLabel.trailingAnchor, constant: 20),
            self.vatDeductableFromVehicleCheckbox.heightAnchor.constraint(equalToConstant: 28),
            self.vatDeductableFromVehicleCheckbox.widthAnchor.constraint(equalToConstant: 28),

            self.createPaymentNowLabel.topAnchor.constraint(equalTo: self.vatDeductableFromSaleLabel.bottomAnchor, constant: 40),
            self.createPaymentNowLabel.leadingAnchor.constraint(equalTo: self.totalPaidLabel.leadingAnchor),

            self.createPaymentNowCheckbox.centerYAnchor.constraint(equalTo: self.createPaymentNowLabel.centerYAnchor),
            self.createPaymentNowCheckbox.leadingAnchor.constraint(equalTo: self.vatDeductableFromVehicleCheckbox.leadingAnchor),
            self.createPaymentNowCheckbox.heightAnchor.constraint(equalToConstant: 28),
            self.createPaymentNowCheckbox.widthAnchor.constraint(equalToConstant: 28),

            self.valueIsEstimatedLabel.topAnchor.constraint(equalTo: self.createPaymentNowLabel.bottomAnchor, constant: 40),
            self.valueIsEstimatedLabel.leadingAnchor.constraint(equalTo: self.totalPaidLabel.leadingAnchor),

            self.valueIsEstimatedCheckbox.centerYAnchor.constraint(equalTo: self.valueIsEstimatedLabel.centerYAnchor),
            self.valueIsEstimatedCheckbox.leadingAnchor.constraint(equalTo: self.vatDeductableFromVehicleCheckbox.leadingAnchor),
            self.valueIsEstimatedCheckbox.heightAnchor.constraint(equalToConstant: 28),
            self.valueIsEstimatedCheckbox.widthAnchor.constraint(equalToConstant: 28),

            self.vatRateLabel.topAnchor.constraint(equalTo: self.valueIsEstimatedLabel.bottomAnchor, constant: 40),
            self.vatRateLabel.leadingAnchor.constraint(equalTo: self.totalPaidLabel.leadingAnchor),

            vatRateTextField.leadingAnchor.constraint(equalTo: vatDeductableFromVehicleCheckbox.leadingAnchor),
            vatRateTextField.widthAnchor.constraint(equalToConstant: 60),
            vatRateTextField.centerYAnchor.constraint(equalTo: vatRateLabel.centerYAnchor),
        ])
    }
}

extension AddTransactionFlowValue: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let vatRates = AddTransactionFlowManager.shared.vatRates else { return 0 }
        return vatRates.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let vatRates = AddTransactionFlowManager.shared.vatRates else { return "" }
        return vatRates[row].name
    }

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let vatRates = AddTransactionFlowManager.shared.vatRates else { return "".asMMBAttributedText(.pickerView) }
        return vatRates[row].name.asMMBAttributedText(.pickerView)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let vatRates = AddTransactionFlowManager.shared.vatRates else { return }
        setActiveFieldText(vatRates[row].name)
        self.selectedVATRate = vatRates[row]
    }
}
