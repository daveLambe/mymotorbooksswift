//
//  VehiclesTableView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/11/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class VehiclesTableView: UITableView {
    var data = [Vehicle]()
    var currentPage: Int = 1
    var totalCount = 0
    var fetchInProgress = false

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.register(VehicleCell.self, forCellReuseIdentifier: MyMotorBooksCellIdentifier.vehicle.rawValue)
        fetchVehicles()
        self.reloadData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func fetchVehicles() {
        guard !fetchInProgress else {
            return
        }

        fetchInProgress = true

        MMBApiService.shared.fetchVehiclesPage(page: currentPage) { [weak self] (result: Result<Vehicles, MMBApiService.MMBApiServiceError>) in
            switch result {
            case .success(let vehiclesResponse):
                self?.fetchInProgress = false
                self?.totalCount = vehiclesResponse.count
                self?.data.append(contentsOf: vehiclesResponse.results)

                if vehiclesResponse.next != nil {
                    self?.currentPage += 1
                } else {
                    print("<<<< All objects fetched! >>>>")
                }
                print("<<<< Number of Vehicles fetched: \(vehiclesResponse.results.count) >>>>")
                self?.reloadData()
            case .failure(let error):
                self?.fetchInProgress = false
                print("<<<< Error fetching vehicles: \(error) >>>>")
            }
        }
    }
}

extension VehiclesTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count == 0 {
            tableView.setEmptyMessage("No Vehicles Found")
        } else {
            tableView.restore()
        }
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == data.count - 1 {
            print("<<<< Last Cell Reached!!! >>>>")
            if totalCount > data.count {
                fetchVehicles()
            }
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: MyMotorBooksCellIdentifier.vehicle.rawValue,
                                                 for: indexPath) as! VehicleCell
        cell.item = data[indexPath.row]
        return cell
    }
}
