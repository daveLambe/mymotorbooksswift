//
//  Vehicle.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/27/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

protocol DescribedAttribute {
    var rawValue: Int { get }
    var description: String { get }
}

enum Countries: Int, CaseIterable, Codable, DescribedAttribute {
    case IRELAND
    case UK

    public var description: String { return String(describing: self) }
}

enum FuelType: Int, CaseIterable, Codable, DescribedAttribute {
    case PETROL = 0
    case DIESEL = 1
    case HYBRID = 2
    case ELECTRIC = 3

    public var description: String { return String(describing: self) }
}

enum TransmissionType: Int, CaseIterable, Codable, DescribedAttribute {
    case MANUAL = 0
    case AUTOMATIC = 1

    public var description: String { return String(describing: self) }
}

enum MileageUnitType: Int, CaseIterable, Codable, DescribedAttribute {
    case KILOMETERS = 0
    case MILES = 1

    public var description: String { return String(describing: self) }
}

enum SchemeType: Int, CaseIterable, Codable, DescribedAttribute {
    case MARGIN = 0
    case QUALIFYING = 1

    public var description: String { return String(describing: self) }
}

enum VehicleStatus: Int, CaseIterable, Codable, DescribedAttribute {
    case AWAITING_ARRIVAL = 0
    case FOR_SALE = 1
    case NOT_FOR_SALE = 2
    case AWAITING_WORK = 3
    case SALE_AGREED = 4
    case DEPOSIT_TAKEN = 5
    case SOLD = 6

    public var description: String { return String(describing: self) }
}

enum IsTradeIn: Int, CaseIterable, Codable, DescribedAttribute {
    case YES
    case NO

    public var description: String { return String(describing: self) }
}

enum ProcessNow: Int, CaseIterable, Codable, DescribedAttribute {
    case YES
    case NO

    public var description: String { return String(describing: self) }
}

struct VehicleDataStaticOptions {
    static let countries: [String] = ["Ireland", "UK"]
    static let fuelTypes: [String] = ["PETROL", "DIESEL", "HYBRID", "ELECTRIC"]
    static let transmissionTypes: [String] = ["MANUAL", "AUTOMATIC"]
    static let milageUnits: [String] = ["MILES", "KILOMETERS"]
    static let schemes: [String] = ["MARGIN", "QUALIFYING"]
    static let isTradeIn: [String] = ["YES", "NO"]
    static let processNow: [String] = ["YES", "NO"]
}

struct VehicleUpdateResponse: Decodable {
    let vehicle: Vehicle
//    let transaction: Transaction
}

struct VehicleData: Codable {
    let boughtFromCountry: String
    let fuelType: Int?
    let make: Int
    let mileageIn: Int?
    let mileageInUnit: Int?
    let model: Int
    let regNumberIn: String
    let regNumberOut: String?
    let scheme: String
    let transmission: String?

    init(boughtFromCountry: String, fuelType: Int? = 0, make: Int,
         mileageIn: Int? = 0, mileageInUnit: Int? = 0, model: Int, regNumberIn: String, regNumberOut: String? = "",
         scheme: String, transmission: String? = "") {
        self.boughtFromCountry = boughtFromCountry
        self.fuelType = fuelType
        self.make = make
        self.mileageIn = mileageIn
        self.mileageInUnit = mileageInUnit
        self.model = model
        self.regNumberIn = regNumberIn
        self.regNumberOut = regNumberOut
        self.scheme = scheme
        self.transmission = transmission
    }
}

public struct VehicleStats: Codable {
    let vehiclesInStock: Int
    let vehiclesSaleAgreed: Int
    let vehiclesDepositTaken: Int
    let totalDaysInStock: Int
    let numberOver30Days: Int
    let numberOver60Days: Int
    let numberOver90Days: Int
    let totalStockValue: Double
}

struct VehicleMake: Decodable, Equatable, DescribedAttribute {
    let pk: Int
    let name: String

    public var rawValue: Int { return pk }
    public var description: String { return name }
}

struct VehicleModel: Decodable, DescribedAttribute {
    let pk: Int
    let make: Int
    let name: String

    public var rawValue: Int { return pk }
    public var description: String { return name }
}

struct Vehicles: Decodable, MMBAPIResponse {
    typealias T = Vehicle

    let results: [Vehicle]
    let count: Int
    let next: String?
    let previous: String?
}

struct Vehicle: Codable, Searchable {
    let pk: Int
    let status: IntOrString?  // default = FOR_SALE
    let objOwner: IntOrString
    let make: IntOrString
    let model: IntOrString
    let modelDerivative: String?
    let fuelType: IntOrString? // needs default (Petrol)
    let transmission: IntOrString? // needs default (Manual)
    let comment: String?
    let stockNumber: Int // needs default (0)
    let dateBought: Date // needs default (now)
    let regNumberIn: String?
    let color: String?
    let numPrevOwners: Int? // needs default (0)
    let engineNum: String?
    let chassisNum: String?
    let mileageIn: Int // needs default (0)
    let mileageInUnit: IntOrString? // default = KILOMETERS
    let boughtFromCountry: IntOrString  // default = IRELAND
    let scheme: IntOrString // default = MARGIN
    let dateSold: Date?
    let mileageOut: Int?
    let mileageOutUnit: IntOrString?
    let regNumberOut: String?
    let bodyStyle: String?
    let engineSize: Int? // needs default (0) Apparently an Int?
    let numberOfDoors: Int? // needs default (0)
    let isTradeIn: Bool // can have default (false)

    var query: String { return self.make.description }

    init(pk: Int,
         status: IntOrString? = .int(VehicleStatus.FOR_SALE.rawValue),
         objOwner: IntOrString,
         make: IntOrString,
         model: IntOrString,
         modelDerivative: String? = nil,
         fuelType: IntOrString? = .int(FuelType.PETROL.rawValue),
         transmission: IntOrString? = .int(TransmissionType.MANUAL.rawValue),
         comment: String? = nil,
         stockNumber: Int = 0,
         dateBought: Date,
         regNumberIn: String,
         color: String? = nil,
         numPrevOwners: Int? = nil,
         engineNum: String? = nil,
         chassisNum: String? = nil,
         mileageIn: Int,
         mileageInUnit: IntOrString? = nil,
         boughtFromCountry: IntOrString,
         scheme: IntOrString,
         dateSold: Date? = nil,
         mileageOut: Int? = nil,
         mileageOutUnit: IntOrString? = nil,
         regNumberOut: String,
         bodyStyle: String? = nil,
         engineSize: Int? = nil,
         numberOfDoors: Int? = nil,
         isTradeIn: Bool) {

        self.pk = pk
        self.status = status
        self.objOwner = objOwner
        self.make = make
        self.model = model
        self.modelDerivative = modelDerivative
        self.fuelType = fuelType
        self.transmission = transmission
        self.comment = comment
        self.stockNumber = stockNumber
        self.dateBought = dateBought
        self.regNumberIn = regNumberIn
        self.color = color
        self.numPrevOwners = numPrevOwners
        self.engineNum = engineNum
        self.chassisNum = chassisNum
        self.mileageIn = mileageIn
        self.mileageInUnit = mileageInUnit
        self.boughtFromCountry = boughtFromCountry
        self.scheme = scheme
        self.dateSold = dateSold
        self.mileageOut = mileageOut
        self.mileageOutUnit = mileageOutUnit
        self.regNumberOut = regNumberOut
        self.bodyStyle = bodyStyle
        self.engineSize = engineSize
        self.numberOfDoors = numberOfDoors
        self.isTradeIn = isTradeIn
    }
}
