//
//  VehicleCell.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/28/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class VehicleCell: BaseTableViewCell<Vehicle> {
    static let identifier: String = MyMotorBooksCellIdentifier.vehicle.rawValue

    override var item: Vehicle? {
        didSet {
            guard let vehicle = item else { return }
            makeLabel.text = "Make: \(vehicle.make.description)"
            modelLabel.text = "Model: \(vehicle.model.description)"
            regLabel.text = "Reg: \(vehicle.regNumberIn ?? "")"
            stockNumberLabel.text = "#\(vehicle.stockNumber)"
        }
    }

    private let makeLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let modelLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let regLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let stockNumberLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let vehicleImageView: UIImageView = {
        let imageView = UIImageView()
        let defaultImage = UIImage(systemName: "car.fill")
        imageView.image = defaultImage
        imageView.contentMode = .scaleAspectFit
        imageView.setCorner(radius: 10)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let cellSeperator: UIView = {
        let seperatorView = UIView()
        seperatorView.backgroundColor = MyMotorBooksColor.pink.rawValue
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        return seperatorView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = MyMotorBooksColor.black.rawValue
        contentView.addSubview(vehicleImageView)
        contentView.addSubview(makeLabel)
        contentView.addSubview(modelLabel)
        contentView.addSubview(regLabel)
        contentView.addSubview(stockNumberLabel)
        contentView.addSubview(cellSeperator)

        configureLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureLayout() {

        NSLayoutConstraint.activate([
            vehicleImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            vehicleImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
            vehicleImageView.heightAnchor.constraint(equalToConstant: 65),
            vehicleImageView.widthAnchor.constraint(equalToConstant: 65),

            makeLabel.leadingAnchor.constraint(equalTo: vehicleImageView.trailingAnchor, constant: 4),
            makeLabel.centerYAnchor.constraint(equalTo: modelLabel.centerYAnchor, constant: -18),

            modelLabel.leadingAnchor.constraint(equalTo: makeLabel.leadingAnchor),
            modelLabel.centerYAnchor.constraint(equalTo: vehicleImageView.centerYAnchor),

            regLabel.leadingAnchor.constraint(equalTo: makeLabel.leadingAnchor),
            regLabel.centerYAnchor.constraint(equalTo: modelLabel.centerYAnchor, constant: 18),

            stockNumberLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),
            stockNumberLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

            cellSeperator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left),
            cellSeperator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -self.separatorInset.right),
            cellSeperator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            cellSeperator.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
}
