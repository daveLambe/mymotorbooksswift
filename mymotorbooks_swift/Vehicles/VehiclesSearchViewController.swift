//
//  VehiclesSearchViewController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

final class VehiclesSearchViewController: MMBTableViewSearchController<VehicleCell, Vehicle> {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Vehicles"
        tableView.estimatedRowHeight = 75
        let addVehicleButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addVehicleTapped))
        navigationItem.rightBarButtonItem = addVehicleButton
        fetchVehicles()
    }

    func fetchVehicles() {
        MMBApiService.shared.fetchVehicles() { [weak self] (result: Result<Vehicles, MMBApiService.MMBApiServiceError>) in
            switch result {
            case .success(let vehiclesResponse):
                print("<<<< Successfully fetched Vehicles! >>>>")
                self?.models = vehiclesResponse.results
                self?.tableView.reloadData()
            case .failure(let error):
                print("<<<< Error fetching vehicles: \(error) >>>>")
            }
        }
    }

    @objc private func addVehicleTapped() {
        let addVehicleView = AddVehicleViewController()
        present(addVehicleView, animated: true)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
