//
//  AddVehicleViewController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/19/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

enum AddVehicleField: Int, CaseIterable {
    case country
    case regNumber
    case make
    case model
    case modelDerivitive
    case bodyStyle
    case fuelType
    case transmissionType
    case color
    case numPreviousOwners
    case engineNumber
    case chassisNumber
    case milageNumber
    case milageUnit
    case numberDoors
    case scheme
    case isTradeIn
    case processNow

    public var placeholder: String {
        switch self {
            case .country: return "Country"
            case .regNumber: return "Registration Number"
            case .make: return "Vehicle Make"
            case .model: return "Vehicle Model"
            case .modelDerivitive: return "Model Derivitive"
            case .bodyStyle: return "Body Style"
            case .fuelType: return "Fuel Type"
            case .transmissionType: return "Transmission Type"
            case .color: return "Colour"
            case .numPreviousOwners: return "Number of previous owners"
            case .engineNumber: return "Engine Number"
            case .chassisNumber: return "Chassis Number"
            case .milageNumber: return "Milage"
            case .milageUnit: return "Milage Unit"
            case .numberDoors: return "Number of Doors"
            case .scheme: return "Scheme"
            case .isTradeIn: return "Is Trade In?"
            case .processNow: return "Process Now?"
        }
    }
}

class AddVehicleViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    var makeToFetch: VehicleMake? {
        didSet {
            guard let make = makeToFetch else {
                print("<<<< makeToFetch is nil. returning >>>>")
                return
            }
            fetchModels(forMake: make)
            modelsField.isEnabled = true
        }
    }
    var makes = [VehicleMake]()
    var models = [VehicleModel]()
    var activeTextField: MMBTextField?

    let countryField = MMBPickerTextField(placeholderText: AddVehicleField.country.placeholder)
    let regNumberField = MMBTextField(placeholderText: AddVehicleField.regNumber.placeholder)
    let makesField = MMBPickerTextField(placeholderText: AddVehicleField.make.placeholder)
    let modelsField = MMBPickerTextField(placeholderText: AddVehicleField.model.placeholder)
    let modelDerivitiveField = MMBTextField(placeholderText: AddVehicleField.modelDerivitive.placeholder)
    let bodyStyleField = MMBTextField(placeholderText: AddVehicleField.bodyStyle.placeholder)
    let fuelTypeField = MMBPickerTextField(placeholderText: AddVehicleField.fuelType.placeholder)
    let transmissionTypeField = MMBPickerTextField(placeholderText: AddVehicleField.transmissionType.placeholder)
    let colorField = MMBTextField(placeholderText: AddVehicleField.color.placeholder)
    let numPreviousOwnersField = MMBTextField(placeholderText: AddVehicleField.numPreviousOwners.placeholder)
    let engineNumberField = MMBTextField(placeholderText: AddVehicleField.engineNumber.placeholder)
    let chassisNumberField = MMBTextField(placeholderText: AddVehicleField.chassisNumber.placeholder)
    let milageNumberField = MMBTextField(placeholderText: AddVehicleField.milageNumber.placeholder)
    let milageUnitField = MMBPickerTextField(placeholderText: AddVehicleField.milageUnit.placeholder)
    let numberDoorsField = MMBTextField(placeholderText: AddVehicleField.numberDoors.placeholder)
    let schemeField = MMBPickerTextField(placeholderText: AddVehicleField.scheme.placeholder)
    let isTradeInField = MMBPickerTextField(placeholderText: AddVehicleField.isTradeIn.placeholder)
    let processNowField = MMBPickerTextField(placeholderText: AddVehicleField.processNow.placeholder)

    let addVehicleLabel: UILabel = {
        let label = UILabel()
        label.text = "Add Vehicle"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let cancelButton: UIButton = {
        let button = UIButton()
        let buttonAttributedText = "Cancel".asAttributedText(withAttributes: AddVehicleViewController.cancelButtonTextAttributes)
        button.setTitle("Cancel", for: .normal)
        button.setAttributedTitle(buttonAttributedText, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let vehicleDataStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .equalSpacing
        sv.spacing = 8
        sv.clipsToBounds = true
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    let submitButton: UIButton = {
        let button = UIButton()
        let buttonAttributedText = "Submit".asAttributedText(withAttributes: AddVehicleViewController.buttonTextAttributes)
        button.setTitle("Submit", for: .normal)
        button.setAttributedTitle(buttonAttributedText, for: .normal)
        button.backgroundColor = MyMotorBooksColor.black.rawValue
        button.setBorder(width: 4, color: MyMotorBooksColor.pink.rawValue)
        button.setShadow(color: UIColor.white, radius: 4)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyMotorBooksColor.black.rawValue
        let viewTappedRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(viewTappedRecognizer)
        submitButton.addTarget(self, action: #selector(submitTapped), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(sheetCancelTapped), for: .touchUpInside)

        getAllTextFields().forEach {
            vehicleDataStackView.addArrangedSubview($0)
        }

        view.addSubview(addVehicleLabel)
        view.addSubview(cancelButton)
        view.addSubview(vehicleDataStackView)
        view.addSubview(submitButton)

        setupFields()
        setupLayout()
        fetchMakes()
    }

    // MARK: - Fetch Vehicle Methods
    func fetchMakes() {
        MMBApiService.shared.fetchVehicleMakes() { [weak self]
            (makesResult) -> Void in
            switch makesResult {
                case let .success(makes):
                    print("<<<< Successfully fetched Vehicle Makes! >>>>")
                    self?.makes = makes
                    self?.makesField.pickerView.reloadAllComponents()
                case let .failure(error):
                    print("<<<< Failed to fetch Vehicle Makes with error: \(error) >>>>")
            }
        }
    }

    func fetchModels(forMake make: VehicleMake) {
        MMBApiService.shared.fetchVehicleModels(vehicleMake: make) { [weak self]
            (modelsResult) -> Void in
            switch modelsResult {
                case let .success(models):
                    print("<<<< Successfully fetched Vehicle Models for Make: \(make.name)! >>>>")
                    self?.models = models
                    self?.modelsField.pickerView.reloadAllComponents()
                case let .failure(error):
                    print("<<<< Failed to fetch Vehicle Models with error: \(error) >>>>")
            }
        }
        makeToFetch = nil
    }

    // MARK: - Submit Methods
    private func highlightUnsatisfiedRequiredFields() {
        unsatisfiedRequiredFields.forEach {
            print("<<<< Unsatisfied Required Field: \($0.placeholderText) >>>>")
            $0.attributedPlaceholder = $0.placeholderText.asAttributedText(withAttributes: AddVehicleViewController.unsatisfiedVehicleDetailTextAttributes)
        }
    }

    private var requiredFields: [MMBTextField] {
        return getAllTextFields().filter { $0.required == true }
    }

    private var unsatisfiedRequiredFields: [MMBTextField] {
        return getAllTextFields().filter { $0.required == true && $0.selectedOption == nil }
    }

    private var areRequiredFieldsSatisfied: Bool {
        return unsatisfiedRequiredFields.isEmpty
    }

    private func createVehicleToSubmit() -> VehicleData {
        let vehicle = VehicleData(boughtFromCountry: countryField.attribute!.description, fuelType: fuelTypeField.attribute!.rawValue,
                                  make: makesField.attribute!.rawValue, mileageIn: 69, mileageInUnit: milageUnitField.attribute!.rawValue,
                                  model: modelsField.attribute!.rawValue, regNumberIn: regNumberField.selectedOption!,
                                  scheme: schemeField.attribute!.description, transmission: transmissionTypeField.attribute!.description)
        return vehicle
    }

    private func submitPurchase() {
        if areRequiredFieldsSatisfied {
            let newVehicle = createVehicleToSubmit()
            // Rename? Should it be purchase?
            MMBApiService.shared.purchaseVehicle(vehicleData: newVehicle) { [weak self]
                (purchaseResponse) -> Void in
                switch purchaseResponse {
                case let .success(response):
                    let successAlertController = UIAlertController(title: "Success!",
                                                                   message: "Vehicle has been added successfully!",
                                                                   preferredStyle: .alert)
                    successAlertController.addAction(UIAlertAction(title: "Ok!", style: .default, handler: { action in
                        self?.sheetCancelTapped()
                    }))
                    self?.present(successAlertController, animated: true)
                    print("<<<< Purchase Success!!! >>>>")
                    print("<<<< response: \(response) \(#function) >>>>")
                case let .failure(error):
                    print("<<<< error: \(error) \(#function) >>>>")
                }
            }
        } else {
            let unsatisfiedFieldsAlertController = UIAlertController(title: "More information required!",
                                                                     message: "Please ensure all required fields have been filled in",
                                                                     preferredStyle: .alert)
            unsatisfiedFieldsAlertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                self.highlightUnsatisfiedRequiredFields()
            }))
            self.present(unsatisfiedFieldsAlertController, animated: true)
        }
    }
    //        let newVehicle = VehicleData(boughtFromCountry: "IRELAND", fuelType: 0, make: 8, mileageIn: 6969, mileageInUnit: 0, model: 66, regNumberIn: "201-D-666", scheme: "QUALIFYING", transmission: "MANUAL")

    // MARK: - Selectors
    @objc func submitTapped() {
        print("<<<< Submit button tapped! >>>>")
        submitPurchase()
    }

    @objc func sheetCancelTapped() {
        print("<<<< Submit button tapped! >>>>")
        self.dismiss(animated: true)

    }

    // MARK: - Gesture Recognizers
    @objc func viewTapped() {
        print("<<<< VIEW TAPPED!!!! \(#function) >>>>")
        activeTextField?.resignFirstResponder()
    }

    // MARK: - Textfield Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = nil
        if let pickerField = textField as? MMBPickerTextField {
            activeTextField = pickerField
            if pickerField.selectedOption == nil {
                pickerView(pickerField.pickerView, didSelectRow: 0, inComponent: 0)
            }
        }
        guard let field = textField as? MMBTextField else {
            print("<<<< FAILED TO SET AS MMBTEXTFIELD. NO ACTIVE TEXT FIELD WILL BE SET >>>>")
            return
        }
        activeTextField = field
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let activeField = activeTextField else {
            print("<<<< NO ACTIVE TEXT FIELD >>>>")
            self.view.endEditing(true)
            return
        }

        if let text = activeField.text, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            setActiveFieldText(text)
            setActiveFieldSelectedOption()
        } else {
            print("<<<< SETTING PLACEHOLDER TEXT FOR FIELD \(activeField) >>>>")
            setActiveFieldPlaceholder()
        }
        activeTextField = nil
        self.view.endEditing(true)
    }

    // MARK: - MMBTextField Methods
    private func getAllTextFields() -> [MMBTextField] {
        return [countryField, regNumberField, makesField, modelsField, modelDerivitiveField,
                bodyStyleField, fuelTypeField, transmissionTypeField, colorField, numPreviousOwnersField,
                engineNumberField, chassisNumberField, milageNumberField, milageUnitField, numberDoorsField,
                schemeField, isTradeInField, processNowField]
         }

    private func getAllNonPickerFields() -> [MMBTextField] {
         return getAllTextFields().filter { !($0 is MMBPickerTextField) }
    }

    private func getAllPickerTextFields() -> [MMBPickerTextField] {
        return getAllTextFields().compactMap { $0 as? MMBPickerTextField }
    }

    private func setRequiredTextFields() {
        getAllPickerTextFields().forEach {
            $0.required = true
        }
        regNumberField.required = true
    }

    private func setActiveFieldText(_ newText: String) {
        activeTextField?.text = newText
    }

    private func setActiveFieldPlaceholder() {
        activeTextField?.placeholder = activeTextField?.placeholderText
    }

    private func setActiveFieldSelectedOption() {
        if let pickerField = activeTextField as? MMBPickerTextField, let attribute = pickerField.attribute {
            pickerField.setSelectedOption(attribute.description)
        } else {
            activeTextField?.setSelectedOption(activeTextField?.text)
        }
    }

    private func setSelectedOption(withText text: String) {
        guard let activeField = activeTextField else {
            print("<<<< No textfield is active >>>>")
            return
        }
        activeField.selectedOption = text
    }

    private func setVehicleAttribute(_ attribute: DescribedAttribute) {
        guard let activeField = activeTextField as? MMBPickerTextField else {
            print("<<<< No textfield is active >>>>")
            return
        }
        activeField.attribute = attribute

    }

    private func setSelectedOption(withAttribute attribute: DescribedAttribute) {
        guard let activeField = activeTextField as? MMBPickerTextField else {
            print("<<<< No textfield is active >>>>")
            return
        }
        activeField.selectedOption = attribute.description
    }

    private func setupFields() {
        modelsField.isEnabled = false
        getAllTextFields().forEach {
            $0.delegate = self
            $0.toolbarDelegate = self
            $0.defaultTextAttributes = AddVehicleViewController.vehicleDetailTextAttributes
            setTextFieldTags()
            setRequiredTextFields()
            if let pickerView = $0.inputView as? UIPickerView {
                pickerView.delegate = self
                pickerView.dataSource = self
                pickerView.tag = $0.tag
                pickerView.reloadAllComponents()
            }
        }
    }

    private func setTextFieldTags() {
        countryField.tag = AddVehicleField.country.rawValue
        regNumberField.tag = AddVehicleField.regNumber.rawValue
        makesField.tag = AddVehicleField.make.rawValue
        modelsField.tag = AddVehicleField.model.rawValue
        modelDerivitiveField.tag = AddVehicleField.modelDerivitive.rawValue
        bodyStyleField.tag = AddVehicleField.bodyStyle.rawValue
        fuelTypeField.tag = AddVehicleField.fuelType.rawValue
        transmissionTypeField.tag = AddVehicleField.transmissionType.rawValue
        colorField.tag = AddVehicleField.color.rawValue
        numPreviousOwnersField.tag = AddVehicleField.numPreviousOwners.rawValue
        engineNumberField.tag = AddVehicleField.engineNumber.rawValue
        chassisNumberField.tag = AddVehicleField.chassisNumber.rawValue
        milageNumberField.tag = AddVehicleField.milageNumber.rawValue
        milageUnitField.tag = AddVehicleField.milageUnit.rawValue
        numberDoorsField.tag = AddVehicleField.numberDoors.rawValue
        schemeField.tag = AddVehicleField.scheme.rawValue
        isTradeInField.tag = AddVehicleField.isTradeIn.rawValue
        processNowField.tag = AddVehicleField.processNow.rawValue
    }

    // MARK: PickerView Methods

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let tag = AddVehicleField.init(rawValue: pickerView.tag) {
            if tag == .make {
                let selectedMake = makes[row]
                self.makeToFetch = selectedMake
                setActiveFieldText(selectedMake.name)
                setVehicleAttribute(selectedMake)
                return
            }
            if tag == .model {
                let selectedModel = models[row]
                setActiveFieldText(selectedModel.name)
                setVehicleAttribute(selectedModel)
                return
            }

            var selectedVehicleAttribute: DescribedAttribute {
                switch tag {
                    case .country:
                        return Countries.init(rawValue: row)!
                    case .fuelType:
                        return FuelType.init(rawValue: row)!
                    case .transmissionType:
                        return TransmissionType.init(rawValue: row)!
                    case .milageUnit:
                        return MileageUnitType.init(rawValue: row)!
                    case .scheme:
                        return SchemeType.init(rawValue: row)!
                    case .isTradeIn:
                        return IsTradeIn.init(rawValue: row)!
                    case .processNow:
                        return ProcessNow.init(rawValue: row)!
                    default:
                        print("<<<< Looks like a case was missed in \(#function) >>>>")
                        return ProcessNow.init(rawValue: row)!
                }
            }
            setActiveFieldText(selectedVehicleAttribute.description)
            setVehicleAttribute(selectedVehicleAttribute)
        }
        print("<<<< Selected row: \(row) >>>>")
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let tag = AddVehicleField.init(rawValue: pickerView.tag) {
            switch tag {
                case .country:
                    return Countries.allCases.count
                case .make:
                    return makes.count
                case .model:
                    return models.count
                case .fuelType:
                    return FuelType.allCases.count
                case .transmissionType:
                    return TransmissionType.allCases.count
                case .milageUnit:
                    return MileageUnitType.allCases.count
                case .scheme:
                    return SchemeType.allCases.count
                case .isTradeIn:
                    return VehicleDataStaticOptions.isTradeIn.count
                case .processNow:
                    return VehicleDataStaticOptions.processNow.count
                default:
                    return 0
            }
        }
        return 1
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let tag = AddVehicleField.init(rawValue: pickerView.tag) {
            switch tag {
                case .country:
                    return Countries.init(rawValue: row)!.description
                case .make:
                    return makes[row].name
                case .model:
                    return models[row].name
                case .fuelType:
                    return FuelType.init(rawValue: row)!.description
                case .transmissionType:
                    return TransmissionType.init(rawValue: row)!.description
                case .milageUnit:
                    return MileageUnitType.init(rawValue: row)!.description
                case .scheme:
                    return SchemeType.init(rawValue: row)!.description
                case .isTradeIn:
                    return VehicleDataStaticOptions.isTradeIn[row]
                case .processNow:
                    return VehicleDataStaticOptions.processNow[row]
                default:
                    return ""
            }
        }
        return "Failed to load data."
    }

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if let tag = AddVehicleField.init(rawValue: pickerView.tag) {
            switch tag {
                case .country:
                    return Countries.init(rawValue: row)!.description.asMMBAttributedText(.pickerView)
                case .make:
                    return makes[row].name.asMMBAttributedText(.pickerView)
                case .model:
                    return models[row].name.asMMBAttributedText(.pickerView)
                case .fuelType:
                    return FuelType.init(rawValue: row)!.description.asMMBAttributedText(.pickerView)
                case .transmissionType:
                    return TransmissionType.init(rawValue: row)!.description.asMMBAttributedText(.pickerView)
                case .milageUnit:
                    return MileageUnitType.init(rawValue: row)!.description.asMMBAttributedText(.pickerView)
                case .scheme:
                    return SchemeType.init(rawValue: row)!.description.asMMBAttributedText(.pickerView)
                case .isTradeIn:
                    return VehicleDataStaticOptions.isTradeIn[row].asMMBAttributedText(.pickerView)
                case .processNow:
                    return VehicleDataStaticOptions.processNow[row].asMMBAttributedText(.pickerView)
                default:
                    return "".asMMBAttributedText(.pickerView)
            }
        }
        return "Failed to load data.".asMMBAttributedText(.pickerView)
    }

    // MARK: - Layout
    func setupLayout() {
        NSLayoutConstraint.activate([
            addVehicleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 40),
            addVehicleLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            addVehicleLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20),

            cancelButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
            cancelButton.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10),

            vehicleDataStackView.topAnchor.constraint(equalTo: addVehicleLabel.bottomAnchor, constant: 18),
            vehicleDataStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20),
            vehicleDataStackView.trailingAnchor.constraint(equalTo: submitButton.trailingAnchor),
            vehicleDataStackView.bottomAnchor.constraint(equalTo: submitButton.topAnchor, constant: -18),

            submitButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            submitButton.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 80),
            submitButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -80)
        ])
    }
}

extension AddVehicleViewController: MMBToolbarDelegate {

    func doneTapped() {
        print("<<<< Toolbar Done button tapped! >>>>")
        activeTextField?.resignFirstResponder()
    }

    func cancelTapped() {
        print("<<<< Toolbar Cancel button tapped! >>>>")
        activeTextField?.resignFirstResponder()
    }
}

extension AddVehicleViewController {
    static let buttonTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                                       NSAttributedString.Key.foregroundColor: UIColor.white]
    static let cancelButtonTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),
                                             NSAttributedString.Key.foregroundColor: MyMotorBooksColor.pink.rawValue]
    static let vehicleDetailTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                                        NSAttributedString.Key.foregroundColor: UIColor.white]
    static let unsatisfiedVehicleDetailTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                                              NSAttributedString.Key.foregroundColor: UIColor.black]
    static let pickerTitleAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 25),
                                        NSAttributedString.Key.foregroundColor: UIColor.white]
}

//fileprivate extension UITextField {
//    convenience init(withVehicleDetailPlaceholder detailText: String) {
//        self.init()
//        backgroundColor = MyMotorBooksColor.pink.rawValue
//        placeholder = detailText
//        attributedPlaceholder = detailText.asAttributedText(withAttributes: AddVehicleViewController.vehicleDetailTextAttributes)
//        self.styleAsDrillableField()
//    }
//}
