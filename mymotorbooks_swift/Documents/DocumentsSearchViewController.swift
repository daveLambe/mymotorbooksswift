//
//  DocumentsSearchViewController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/18/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit


final class DocumentsSearchViewController: MMBTableViewSearchController<DocumentCell, Document> {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Documents"
        tableView.estimatedRowHeight = 65
        fetchDocuments()
    }

    func fetchDocuments() {
        MMBApiService.shared.fetchDocuments() { [weak self] (documentsResponse) in
            switch documentsResponse {
            case .success(let documents):
                print("<<<< Successfully fetched Documents! >>>>")
                self?.models = documents.results
            case .failure(let error):
                print("<<<< Error fetching Documents: \(error.description) >>>>")
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
