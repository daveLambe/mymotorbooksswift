//
//  Document.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/4/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

struct Documents: Decodable, MMBAPIResponse {
    typealias T = Document

    let results: [Document]
    let count: Int
    let next: String?
    let previous: String?
}

struct Document: Decodable, Searchable {
    let pk: Int
    let objOwner: Int
    let documentType: String
    let transactions: [Int]
    let trader: Trader
    let isDepositInvoice: Bool
    let showDepositPaid: Bool
//    let deposit_amount
    let showDueDate: Bool
    let dueDate: Date
    let showSchemeForVehicles: Bool
    let showVrtForVehicles: Bool
    let createdDate: Date
    let number: Int
    let isVrtPurchaseOrder: Bool

    var query: String { return self.documentType }
}
