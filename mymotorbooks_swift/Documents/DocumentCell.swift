//
//  DocumentCell.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/5/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class DocumentCell: BaseTableViewCell<Document> {
    override var item: Document? {
        didSet {
            guard let document = item else { return }
            documentTypeLabel.text = "\(document.documentType)"
            traderNameLabel.text = "Trader: \(document.trader.name)"
            dueDateLabel.text = "Due: \(document.dueDate.asString(dateStyle: .medium))"
            numberLabel.text = "# \(document.number)"
        }
    }

    private let documentTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let traderNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let dueDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let numberLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let documentImageView: UIImageView = {
        let imageView = UIImageView()
        let defaultImage = UIImage(systemName: "doc.fill")
        imageView.image = defaultImage
        imageView.contentMode = .scaleAspectFit
        imageView.setCorner(radius: 10)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let cellSeperator: UIView = {
        let seperatorView = UIView()
        seperatorView.backgroundColor = MyMotorBooksColor.pink.rawValue
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        return seperatorView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = MyMotorBooksColor.black.rawValue
        contentView.addSubview(documentImageView)
        contentView.addSubview(documentTypeLabel)
        contentView.addSubview(traderNameLabel)
        contentView.addSubview(dueDateLabel)
        contentView.addSubview(numberLabel)
        contentView.addSubview(cellSeperator)
        configureLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureLayout() {

        NSLayoutConstraint.activate([
            documentImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            documentImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
            documentImageView.heightAnchor.constraint(equalToConstant: 45),
            documentImageView.widthAnchor.constraint(equalToConstant: 45),

            documentTypeLabel.leadingAnchor.constraint(equalTo: documentImageView.trailingAnchor, constant: 4),
            documentTypeLabel.centerYAnchor.constraint(equalTo: traderNameLabel.centerYAnchor, constant: -16),

            traderNameLabel.leadingAnchor.constraint(equalTo: documentTypeLabel.leadingAnchor),
            traderNameLabel.centerYAnchor.constraint(equalTo: documentImageView.centerYAnchor),

            dueDateLabel.leadingAnchor.constraint(equalTo: documentTypeLabel.leadingAnchor),
            dueDateLabel.centerYAnchor.constraint(equalTo: traderNameLabel.centerYAnchor, constant: 16),

            numberLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),
            numberLabel.centerYAnchor.constraint(equalTo: documentImageView.centerYAnchor),

            cellSeperator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left),
            cellSeperator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -self.separatorInset.right),
            cellSeperator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            cellSeperator.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
}
