//
//  LoginViewController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/20/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    var loginButtonBottomConstraint: NSLayoutConstraint?

    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "MyMotorBooks"
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 40, weight: .bold)
        return label
    }()

    var emailField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textAlignment = .left
        return tf
    }()

    var passwordField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textAlignment = .left
        tf.isSecureTextEntry = true
        tf.tag = 1
        return tf
    }()

    var loginButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Log In!", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()

    var demoButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Demo!", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyMotorBooksColor.black.rawValue
        view.addSubview(titleLabel)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        view.addSubview(demoButton)
        loginButton.addTarget(self, action: #selector(loginPressed), for: .touchUpInside)
        demoButton.addTarget(self, action: #selector(demoButtonPressed), for: .touchUpInside)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                         action: #selector(self.dropKeyboard(_:))))

        emailField.delegate = self
        passwordField.delegate = self
        configureLayout()
        styleLayout()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }

    @objc func loginPressed() {
        guard let email = emailField.text, email != "", let password = passwordField.text, password != "" else {
            let ac = UIAlertController(title: "Enter Email & Password",
                                       message: "Please enter both your email address and password.",
                                       preferredStyle: .alert)

            let okButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)

            ac.addAction(okButton)

            present(ac, animated: true)
            return
        }
        loginButton.isEnabled = false
        demoButton.isEnabled = false
        let credentials = AuthManager.Credentials(email: email, password: password)

        MMBApiService.shared.auth(credentials: credentials) { [weak self]
            (authResult) -> Void in
            switch authResult {
            case let .success(tokens):
                AuthManager.shared.logIn(withTokens: tokens)
            case let .failure(error):
                let loginFailureAlertController = UIAlertController(title: "Login Failed", message: "We were unable to log you in. Please try again", preferredStyle: .alert)
                loginFailureAlertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self?.present(loginFailureAlertController, animated: true)
                print("<<<< Failed to Auth with error: \(error) >>>>")
            }
        }
        loginButton.isEnabled = true
        demoButton.isEnabled = true
    }

    @objc func demoButtonPressed() {
        loginButton.isEnabled = false
        demoButton.isEnabled = false

        MMBApiService.shared.auth(credentials: AuthManager.Credentials(email: "Mark+CarSales@MarkLambe.com", password: "K7D+aJ?44ur4?UVD8]B&")) { [weak self]
            (authResult) -> Void in
            switch authResult {
            case let .success(tokens):
                AuthManager.shared.logIn(withTokens: tokens)
            case let .failure(error):
                let loginFailureAlertController = UIAlertController(title: "Login Failed", message: "We were unable to log you in. Please try again", preferredStyle: .alert)
                loginFailureAlertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self?.present(loginFailureAlertController, animated: true)
                print("<<<< Failed to Auth with error: \(error) >>>>")
            }
        }
        loginButton.isEnabled = true
        demoButton.isEnabled = true
    }

    // MARK: - TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
            case 1:
                textField.resignFirstResponder()
                loginPressed()
            default:
                textField.resignFirstResponder()
        }
        return true
    }

    @objc func dropKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = getKeyboardHeightFrom(notification: notification) {
            loginButtonBottomConstraint?.isActive = false
            loginButtonBottomConstraint = loginButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                                                                              constant: -keyboardHeight)
            loginButtonBottomConstraint?.isActive = true
        }
    }

    @objc func keyboardDidHide() {
        loginButtonBottomConstraint?.isActive = false
        loginButtonBottomConstraint = loginButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                                                                          constant: -40)
        loginButtonBottomConstraint?.isActive = true
    }

    func getKeyboardHeightFrom(notification: NSNotification) -> CGFloat? {
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            print("<<<< Failed to get keyboard height from notification >>>>")
            return nil
        }
        return keyboardFrame.height
    }
}

// MARK: - View Extensions
extension LoginViewController {
    func configureLayout() {
        loginButtonBottomConstraint = loginButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                                                                          constant: -40)
        loginButtonBottomConstraint?.isActive = true

        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 40),

            emailField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            emailField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 80),
            emailField.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 40),
            emailField.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -40),

            passwordField.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            passwordField.topAnchor.constraint(equalTo: emailField.bottomAnchor, constant: 20),
            passwordField.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 40),
            passwordField.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -40),

            loginButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            loginButton.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 80),
            loginButton.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -80),

            demoButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            demoButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            demoButton.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor, constant: 120),
            demoButton.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -120),
        ])
    }
}

extension LoginViewController {
    func styleLayout() {
        emailField.borderStyle = .roundedRect
        emailField.setCorner(radius: 5)
        emailField.setBorder(width: MyMotorBooksBorderWidth.button.rawValue, color: MyMotorBooksColor.pink.rawValue)
        emailField.backgroundColor = MyMotorBooksColor.darkPink.rawValue
        emailField.textColor = UIColor.white
        emailField.setPlaceholderText("Email address...", textColor: UIColor.white)

        passwordField.borderStyle = .roundedRect
        passwordField.setCorner(radius: 5)
        passwordField.setBorder(width: MyMotorBooksBorderWidth.button.rawValue, color: MyMotorBooksColor.pink.rawValue)
        passwordField.backgroundColor = MyMotorBooksColor.darkPink.rawValue
        passwordField.textColor = UIColor.white
        passwordField.setPlaceholderText("Password...", textColor: UIColor.white)

        loginButton.setCorner(radius: 5)
        loginButton.setBorder(width: MyMotorBooksBorderWidth.button.rawValue, color: MyMotorBooksColor.pink.rawValue)
        loginButton.backgroundColor = MyMotorBooksColor.darkPink.rawValue
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)

        demoButton.setCorner(radius: 5)
        demoButton.setBorder(width: MyMotorBooksBorderWidth.button.rawValue, color: MyMotorBooksColor.pink.rawValue)
        demoButton.backgroundColor = MyMotorBooksColor.darkPink.rawValue
        demoButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
    }
}
