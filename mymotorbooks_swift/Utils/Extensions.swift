//
//  Extensions.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/21/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

// MARK: - View Extensions
extension UIView {
    func setCorner(radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }

    func circleCorner() {
        superview?.layoutIfNeeded()
        setCorner(radius: frame.height / 2)
    }

    func setBorder(width: CGFloat, color: UIColor) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }

    func setShadow(color: UIColor, radius: CGFloat) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowRadius = radius
    }
}

// Allows changing of scroll indicators
extension UIScrollView {
    // https://stackoverflow.com/questions/12005187/ios-changing-uiscrollview-scrollbar-color-to-different-colors/58415249#58415249

    var scrollIndicators: (horizontal: UIView?, vertical: UIView?) {

        guard self.subviews.count >= 2 else {
            return (horizontal: nil, vertical: nil)
        }

        func viewCanBeScrollIndicator(view: UIView) -> Bool {
            let viewClassName = NSStringFromClass(type(of: view))
            if viewClassName == "_UIScrollViewScrollIndicator" || viewClassName == "UIImageView" {
                return true
            }
            return false
        }

        let horizontalScrollViewIndicatorPosition = self.subviews.count - 2
        let verticalScrollViewIndicatorPosition = self.subviews.count - 1

        var horizontalScrollIndicator: UIView?
        var verticalScrollIndicator: UIView?

        let viewForHorizontalScrollViewIndicator = self.subviews[horizontalScrollViewIndicatorPosition]
        if viewCanBeScrollIndicator(view: viewForHorizontalScrollViewIndicator) {
            horizontalScrollIndicator = viewForHorizontalScrollViewIndicator.subviews[0]
        }

        let viewForVerticalScrollViewIndicator = self.subviews[verticalScrollViewIndicatorPosition]
        if viewCanBeScrollIndicator(view: viewForVerticalScrollViewIndicator) {
            verticalScrollIndicator = viewForVerticalScrollViewIndicator.subviews[0]
        }
        return (horizontal: horizontalScrollIndicator, vertical: verticalScrollIndicator)
    }
}

// MARK: Image Extensions
extension UIImage {
    func resizedImage(size sizeImage: CGSize) -> UIImage? {
        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: sizeImage.width, height: sizeImage.height))
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        self.draw(in: frame)
        let resizedImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.withRenderingMode(.alwaysOriginal)
        return resizedImage
    }
}

extension UIAlertController {
    static func forUnsatisfiedFields() -> UIAlertController {
        let alertController = UIAlertController(title: "Unsatisfied Fields", message: "Please enter all necessary data", preferredStyle: .alert)

        let alertButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)

        alertController.addAction(alertButton)

        return alertController
    }
}

// MARK: TextField Extensions
extension UITextField {
    enum ViewType {
        case left, right
    }

    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }

    func setView(_ type: ViewType, with view: UIView, userInteractionEnabledForNewView: Bool = false) {
        switch type {
            case .left:
                leftView = view
                leftViewMode = .always
                leftView?.isUserInteractionEnabled = userInteractionEnabledForNewView
            case .right:
                rightView = view
                rightViewMode = .always
                rightView?.isUserInteractionEnabled = userInteractionEnabledForNewView
        }
    }

    func addPadding(_ padding: PaddingSide) {
        self.leftViewMode = .always
        self.layer.masksToBounds = true

        switch padding {
            case .left(let spacing):
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
                self.leftView = paddingView
                self.rightViewMode = .always
            case .right(let spacing):
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
                self.rightView = paddingView
                self.rightViewMode = .always
            case .both(let spacing):
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
                self.leftView = paddingView
                self.leftViewMode = .always
                self.rightView = paddingView
                self.rightViewMode = .always
        }
    }


    @discardableResult
    func setView(_ view: ViewType, title: String, space: CGFloat = 0) -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: frame.height))
        button.setTitle(title, for: UIControl.State())
        button.contentEdgeInsets = UIEdgeInsets(top: 4, left: space, bottom: space, right: 4)
        button.sizeToFit()
        setView(view, with: button)
        return button
    }

    @discardableResult
    func setView(_ view: ViewType, image: UIImage, space: CGFloat = 0) -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: frame.height))
        button.setImage(image, for: UIControl.State())
        button.contentEdgeInsets = UIEdgeInsets(top: space, left: 4, bottom: space, right: 4)
        button.sizeToFit()
        button.imageView?.isUserInteractionEnabled = false
        setView(view, with: button)
        return button
    }
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = MyMotorBooksColor.pink.rawValue
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 20)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UILabel {
    func styleAsMMBLabel() {
        self.textColor = MyMotorBooksColor.pink.rawValue
        self.font = UIFont.boldSystemFont(ofSize: 18)
        self.sizeToFit()
    }
}

extension UITextField {
    func setPlaceholderText(_ text: String, textColor: UIColor) {
        attributedPlaceholder = NSAttributedString(string: text,
                                                   attributes: [NSAttributedString.Key.foregroundColor: textColor,
                                                                NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 15)])
    }

    func styleAsMMBField(isDrillable: Bool) {
        clipsToBounds = true
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.white.cgColor
        self.textAlignment = .left
        self.addPadding(.left(4))

        if isDrillable {
            if let chevronImage = UIImage.init(systemName: "chevron.right")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate) {
                self.setView(.right, image: chevronImage)
                self.tintColor = UIColor.white
            } else {
                print("<<<< Failed to create UIImage for TextField! >>>>")
            }
        }
    }
}

extension String {
    public enum MyMotorBooksTextAttributes {
        case button
        case cancelButton
        case textField
        case unsatisfiedTextField
        case pickerView
    }

    func asMyMotorBooksURL(forEnvironment environment: String = MyMotorBooksBaseURL.develop.rawValue, secureConnection: Bool = false) -> URL {
        let parameters: [String: String] = ["Content-Type": "application/json"]
        var components = URLComponents(string: "\(environment)\(self)")!
        components.scheme = secureConnection ? "https" : "http"

        var queryItems = [URLQueryItem]()

        for (key, value) in parameters {
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }

        components.queryItems = queryItems

        return components.url!
    }

    func asAttributedText(withAttributes attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: attributes)
    }

    func asMMBAttributedText(_ attributes: MyMotorBooksTextAttributes) -> NSAttributedString {
        var attributesToSet = [NSAttributedString.Key: Any]()
        switch attributes {
        case .button: attributesToSet = [NSAttributedString.Key.font: MyMotorBooksFont.button,
                                         NSAttributedString.Key.foregroundColor: UIColor.white]

        case .cancelButton: attributesToSet = [NSAttributedString.Key.font: MyMotorBooksFont.cancelButton,
                                               NSAttributedString.Key.foregroundColor: MyMotorBooksColor.pink.rawValue]

        case .textField: attributesToSet = [NSAttributedString.Key.font: MyMotorBooksFont.textField,
                                            NSAttributedString.Key.foregroundColor: UIColor.white]

        case .unsatisfiedTextField: attributesToSet = [NSAttributedString.Key.font: MyMotorBooksFont.textField,
                                                       NSAttributedString.Key.foregroundColor: UIColor.white]

        case .pickerView: attributesToSet = [NSAttributedString.Key.font: MyMotorBooksFont.pickerView,
                                             NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        return self.asAttributedText(withAttributes: attributesToSet)
    }
}

// MARK: - Other Extensions

extension Formatter {
    static let iso8601WithFractionalSeconds: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return formatter
    }()
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime]
        return formatter
    }()
    static let yearMonthDay: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    static let yearMonthDaySingleDigitAllowed: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        return formatter
    }()
}

extension JSONDecoder.DateDecodingStrategy {
    static let customISO8601 = custom {
        let container = try $0.singleValueContainer()
        let dateString = try container.decode(String.self)
        if let date = Formatter.iso8601WithFractionalSeconds.date(from: dateString) ?? Formatter.iso8601.date(from: dateString) {
            return date
        } else if let date = Formatter.yearMonthDay.date(from: dateString) {
            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid Date: \(dateString)")
    }
}

extension JSONEncoder.DateEncodingStrategy {
    static let customISO8601 = custom {
        var container = $1.singleValueContainer()
        try container.encode(Formatter.iso8601WithFractionalSeconds.string(from: $0))
    }
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }

    static var tomorrow: Date { return Date().dayAfter }

    static var today: Date { return Date() }

    var dayAfter: Date { return Calendar.current.date(byAdding: .day, value: 1, to: Date())! }

    var dayBefore: Date { return Calendar.current.date(byAdding: .day, value: -1, to: Date())! }

    func asString(dateStyle style: DateFormatter.Style) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = style
        return dateformatter.string(from: self)
    }

    func formatDateForAPI() -> String {
        let dateFormatter: DateFormatter = .yearMonthDaySingleDigitAllowed
        return dateFormatter.string(from: self)
    }
}

extension Either: Encodable {
    enum CodingKeys: CodingKey {
        case left
        case right
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
            case .left(let value):
                try container.encode(value, forKey: .left)
            case .right(let value):
                try container.encode(value, forKey: .right)
        }
    }
}

extension Either: Decodable {
    init(from decoder: Decoder) throws {
        print("<<<< IN EITHER DECODABLE >>>>")
        print("<<<< CodingKeys.self: \(CodingKeys.self) >>>>")
        let container = try decoder.container(keyedBy: CodingKeys.self)
        print("<<<< CodingKeys.self: \(CodingKeys.self) >>>>")
        do {
            let leftValue = try container.decode(A.self, forKey: .left)
            print("<<<< A.self: \(A.self) >>>>")
            self = .left(leftValue)
        } catch {
            let rightValue = try container.decode(B.self, forKey: .right)
            self = .right(rightValue)
        }
    }
}

extension IntOrString {
    public func asInt() -> Int {
        switch self {
            case let .int(intValue):
                return intValue
            case let .string(stringValue):
                guard let intValue = Int(stringValue) else {
                    fatalError("<<<< Failed to convert IntOrString value to Int >>>>")
                }
                return intValue
        }
    }

    public func asString() -> String {
        switch self {
            case let .int(intValue):
                return String(intValue)
            case let .string(stringValue):
                return stringValue
        }
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Data {
    var prettyPrintedJSONString: NSString? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }

    func prettyPrint() {
        if let pretty = self.prettyPrintedJSONString {
            print("<<<< prettyPrint: \(pretty) >>>>")
        } else {
            print("<<<< stringPrint: \(String(describing: self )) >>>>")
        }
    }
}
