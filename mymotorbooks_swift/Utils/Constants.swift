//
//  Constants.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/27/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

public enum MyMotorBooksBaseURL: String {
    case develop = "0.0.0.0:8000/"
    case production = "https://api.mymotorbooks.com/api/"
}

public enum MyMotorBooksCellIdentifier: String {
    case account = "AccountCell"
    case transaction = "TransactionCell"
    case vehicle = "VehicleCell"
    case document = "DocumentCell"
    case trader = "TraderCell"
}

public enum DashboardStatTitle: String {
    case vehiclesInStock = "Vehicles in stock"
    case over30DaysInStock = "Vehicles over 30 days in stock"
    case over60DaysInStock = "Vehicles over 60 days in stock"
    case totalDaysInStock = "Total days in stock"
    case vehiclesSaleAgreed = "Vehicles Sale Agreed"
    case vehiclesDepositTaken = "Vehicles Deposit Taken"
}
