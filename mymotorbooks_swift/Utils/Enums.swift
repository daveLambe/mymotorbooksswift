//
//  Enums.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 8/9/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

enum AddTransactionStep: Int, CaseIterable {
    case details = 1
    case vehicle
    case parent
    case value
    case bank

    public var description: String { return String(describing: self).capitalized }
    public var type: String { return String(describing: AddTransactionStep.self) }
}

enum ExpenseType: Int, CaseIterable, Codable {
    case other
    case transport
    case valeting
    case servicing
    case phone
    case parts
    case wages
    case utilities

    var description: String { return String(describing: self ).uppercased() }
    var name: String { return String(describing: self ).uppercased() }
    var label: String { return String(describing: self ).capitalized }
}

enum Either<A: Codable, B: Codable> {
    case left(A)
    case right(B)
}

enum IntOrString: Codable {
    case int(Int)
    case string(String)

    public var description: String {
        switch self {
        case .int(let intValue):
            return String(intValue)
        case .string(let stringValue):
            return String(stringValue)
        }
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            self = try .int(container.decode(Int.self))
        } catch DecodingError.typeMismatch {
            do {
                self = try .string(container.decode(String.self))
            } catch DecodingError.typeMismatch {
                throw DecodingError.typeMismatch(IntOrString.self, DecodingError.Context(codingPath: decoder.codingPath,
                                                                                         debugDescription: "<<<< Failed to encode as type IntOrString >>>>"))
            }
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
            case .int(let int):
                try container.encode(int)
            case .string(let string):
                try container.encode(string)
        }
    }
}
