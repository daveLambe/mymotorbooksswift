//
//  Styles.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 6/28/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

public enum MyMotorBooksBorderWidth: CGFloat {
    case button = 2.0
}

public enum MyMotorBooksColor {
    case darkPink
    case pink
    case black
    case green
}

extension MyMotorBooksColor: RawRepresentable {
    public typealias RawValue = UIColor

    public init?(rawValue: RawValue) {
        switch rawValue {
        case #colorLiteral(red: 0.5529411765, green: 0, blue: 0.2784313725, alpha: 1): self = .darkPink
        case #colorLiteral(red: 0.94, green: 0.08, blue: 0.57, alpha: 1): self = .pink
        case UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.0): self = .black
        case UIColor(red: 0, green: 255, blue: 0, alpha: 1.0): self = .green
        default: return nil
        }
    }

    public var rawValue: RawValue {
        switch self {
        case .darkPink: return #colorLiteral(red: 0.5529411765, green: 0, blue: 0.2784313725, alpha: 1)
        case .pink: return UIColor(red: 0.94, green: 0.08, blue: 0.57, alpha: 1.0)
        case .black: return UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1.0)
        case .green: return UIColor(red: 0, green: 255, blue: 0, alpha: 1.0)
        }
    }
}

public enum MyMotorBooksFont: RawRepresentable {
    case label
    case button
    case cancelButton
    case textField
    case pickerView

}

extension MyMotorBooksFont {
    public typealias RawValue = UIFont

    public init?(rawValue: RawValue) {
        switch rawValue {
        case UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body): self = .label
        case UIFont.boldSystemFont(ofSize: 20): self = .button
        case UIFont.systemFont(ofSize: 18): self = .cancelButton
        case UIFont.boldSystemFont(ofSize: 18): self = .textField
        case UIFont.boldSystemFont(ofSize: 25): self = .pickerView
        default: return nil
        }
    }

    public var rawValue: RawValue {
        switch self {
        case .label: return UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        case .button: return UIFont.boldSystemFont(ofSize: 20)
        case .cancelButton: return UIFont.systemFont(ofSize: 18)
        case .textField: return UIFont.boldSystemFont(ofSize: 18)
        case .pickerView: return UIFont.boldSystemFont(ofSize: 25)
        }
    }
}

public struct MyMotorBooksTextAttributes {
    static let textFieldTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                                   NSAttributedString.Key.foregroundColor: UIColor.white]
    static let pickerTitleAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 25),
                                        NSAttributedString.Key.foregroundColor: UIColor.white]
}
