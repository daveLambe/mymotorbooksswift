//
//  Auth.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 9/11/21.
//  Copyright © 2021 Dave Lambe. All rights reserved.
//

import Foundation
import UIKit

class AuthManager {
    fileprivate let userDefaults = UserDefaults.standard
    public var isUserLoggedIn: Bool = false
    public static let shared = AuthManager()
    
    private init() {}
    
    private enum UserState: String {
        case isUserLoggedIn = "isUserLoggedIn"
        case authRetryCount = "authRetryCount"
    }
    
    enum TokenType: String {
        case access = "accessToken"
        case refresh = "refreshToken"
    }

    enum CredentialType: String {
        case email = "userEmail"
        case password = "userPassword"
    }
    
    struct Tokens: Codable {
        var refresh: String?
        var access: String
        
        init(refreshToken: String? = nil, accessToken: String) {
            self.refresh = refreshToken
            self.access = accessToken
        }
    }
    
    public struct Credentials: Encodable {
        let email: String
        let password: String
        
        init(email: String, password: String) {
            self.email = email
            self.password = password
        }
    }

    public func storeCredentials(credentials: Credentials) {
        KeychainWrapper.standard.set(credentials.email, forKey: CredentialType.email.rawValue)
        KeychainWrapper.standard.set(credentials.password, forKey: CredentialType.password.rawValue)
    }

    public func getCredentials() -> Credentials? {
        guard let email = KeychainWrapper.standard.string(forKey: CredentialType.email.rawValue), let password = KeychainWrapper.standard.string(forKey: CredentialType.password.rawValue) else {
            print("<<<< Failed to retrieve User Credentials >>>>")
            return nil
        }
        return Credentials(email: email, password: password)
    }

    public func storeAccessToken(_ accessToken: String) {
        KeychainWrapper.standard.set(accessToken, forKey: TokenType.access.rawValue)
    }

    public func storeRefreshToken(_ refreshToken: String) {
        KeychainWrapper.standard.set(refreshToken, forKey: TokenType.refresh.rawValue)
    }

    public func getAccessToken() -> String? {
        guard let access = KeychainWrapper.standard.string(forKey: TokenType.access.rawValue) else { return nil }
        return access
    }

    public func getRefreshToken() -> String? {
        guard let refresh = KeychainWrapper.standard.string(forKey: TokenType.refresh.rawValue) else { return nil }
        return refresh
    }

    private func setUserLoggedIn(_ isLoggedIn: Bool) {
        self.isUserLoggedIn = isLoggedIn
    }

    public func setAuthRetryCount(_ count: Int) {
        userDefaults.set(count, forKey: UserState.authRetryCount.rawValue)
    }

    public func getAuthRetryCount() -> Int? {
        guard let retryCount = userDefaults.object(forKey: UserState.authRetryCount.rawValue) as? Int else {
            return nil
        }
        return retryCount
    }

    public func logIn(withTokens tokens: Tokens) {
        if let refresh = getRefreshToken() {
            storeRefreshToken(refresh)
        }
        storeAccessToken(tokens.access)
        setUserLoggedIn(true)
        print("<<<< user has been logged in >>>>")
        let mainTabbedVC = MMBTabBarController()
        mainTabbedVC.modalPresentationStyle = .fullScreen
        AppDelegate.shared().window?.rootViewController = mainTabbedVC
        AppDelegate.shared().window?.makeKeyAndVisible()
    }
    
    public func logOut() {
        print("<<<< Logging user out >>>>")
        clearUserTokens()
        setUserLoggedIn(false)
        print("<<<< User has been logged out >>>>")
        let loginVC = LoginViewController()
        AppDelegate.shared().window?.rootViewController = loginVC
        AppDelegate.shared().window?.makeKeyAndVisible()
    }

    private func clearUserTokens() {
        KeychainWrapper.standard.removeObject(forKey: TokenType.refresh.rawValue)
        KeychainWrapper.standard.removeObject(forKey: TokenType.access.rawValue)
    }
}
