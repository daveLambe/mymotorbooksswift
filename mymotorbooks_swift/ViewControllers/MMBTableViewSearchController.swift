//
//  MMBTableViewSearchController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

extension UITableView {

    func register<T: UITableViewCell>(_ : T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("<<<< Failed to deque Reusable cell >>>>")
        }
        return cell
    }
}

class MMBTableViewSearchController<T: BaseTableViewCell<V>, V>: UITableViewController, UISearchBarDelegate where V: Searchable {

    private var strongDataSource: MMBTableViewDataSource<T, V>?

    private let searchController = UISearchController(searchResultsController: nil)

    var models: [V] = [] {
        didSet {
            DispatchQueue.main.async {
                self.strongDataSource = MMBTableViewDataSource(models: self.models, configureCell: { cell, model in
                    cell.item = model
                    return cell
                })
                self.tableView.dataSource = self.strongDataSource
                self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(T.self)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        setUpSearchBar()
    }

    private func setUpSearchBar() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        strongDataSource?.search(query: searchText)
        self.tableView.reloadData()
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DispatchQueue.main.async {
            scrollView.scrollIndicators.vertical?.backgroundColor = MyMotorBooksColor.pink.rawValue
        }
    }
}
