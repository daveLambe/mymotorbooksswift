//
//  MMBTableViewDatasource.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

final class MMBTableViewDataSource<V, T: Searchable>: NSObject, UITableViewDataSource where V: BaseTableViewCell<T> {

    private var models: [T]

    private let configureCell: CellConfiguration

    typealias CellConfiguration = (V, T) -> V

    private var searchResults: [T] = []

    private var isSearchActive: Bool = false

    init(models: [T], configureCell: @escaping CellConfiguration) {
        self.models = models
        self.configureCell = configureCell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if models.isEmpty {
            let dataTypeName = String(describing: T.self) + "s"
            tableView.setEmptyMessage("No \(dataTypeName) found.")
        } else {
            tableView.restore()
        }
        return isSearchActive ? searchResults.count : models.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: V = tableView.dequeReusableCell(forIndexPath: indexPath)
        let model = getModelAt(indexPath)
        return configureCell(cell, model)
    }

    private func getModelAt(_ indexPath: IndexPath) -> T {
        return isSearchActive ? searchResults[indexPath.item] : models[indexPath.item]
    }

    func search(query: String) {
        isSearchActive = !query.isEmpty

        searchResults = models.filter {
            let queryToFind = $0.query.range(of: query, options: NSString.CompareOptions.caseInsensitive)
            return (queryToFind != nil)
        }
    }
}
