//
//  MMBTabBarController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/1/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class MMBTabBarController: UITabBarController {
    var dashboardVC: DashboardViewController
    var accountsVC: AccountsSearchViewController
    var transactionsVC: TransactionsSearchViewController
    var vehiclesListVC: VehiclesSearchViewController
    var documentsListVC: DocumentsSearchViewController
    var tradersListVC: TradersSearchViewController

    init() {
        // Dashobard
        self.dashboardVC = DashboardViewController()
        // Accounts
        self.accountsVC = AccountsSearchViewController()
        // Transactions
        self.transactionsVC = TransactionsSearchViewController()
        // Vehicles
        self.vehiclesListVC = VehiclesSearchViewController()
        // Documents
        self.documentsListVC = DocumentsSearchViewController()
        // Traders
        self.tradersListVC = TradersSearchViewController()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let dashboardTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Dashboard",
                                      image: UIImage(systemName: "house"),
                                      selectedImage: UIImage(systemName: "house.fill"))
        return tabBarItem
    }()

    let accountsTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Accounts",
                                      image: UIImage(systemName: "book"),
                                      selectedImage: UIImage(systemName: "book.fill"))
        return tabBarItem
    }()

    let transactionsTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Transactions",
                                      image: UIImage(systemName: "creditcard"),
                                      selectedImage: UIImage(systemName: "creditcard.fill"))
        return tabBarItem
    }()

    let vehiclesTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Vehicles",
                                      image: UIImage(systemName: "car"),
                                      selectedImage: UIImage(systemName: "car.fill"))
        return tabBarItem
    }()

    let documentsTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Documents",
                                      image: UIImage(systemName: "doc"),
                                      selectedImage: UIImage(systemName: "doc.fill"))
        return tabBarItem
    }()

    let tradersTabBarItem: UITabBarItem = {
        let tabBarItem = UITabBarItem(title: "Contacts",
                                      image: UIImage(systemName: "person.crop.circle"),
                                      selectedImage: UIImage(systemName: "person.crop.circle.fill"))
        return tabBarItem
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "MyMotorBooks"

        dashboardVC.tabBarItem = dashboardTabBarItem
        accountsVC.tabBarItem = accountsTabBarItem
        transactionsVC.tabBarItem = transactionsTabBarItem
        vehiclesListVC.tabBarItem = vehiclesTabBarItem
        documentsListVC.tabBarItem = documentsTabBarItem
        tradersListVC.tabBarItem = tradersTabBarItem

        let tabBarItems = [dashboardVC,
                           accountsVC,
                           transactionsVC,
                           vehiclesListVC,
                           documentsListVC,
                           tradersListVC]

        viewControllers = tabBarItems.map { UINavigationController(rootViewController: $0) }
    }

    // Override 5 TabBarItem limit
    override var traitCollection: UITraitCollection {
        let realTraits = super.traitCollection
        let lieTrait = UITraitCollection.init(horizontalSizeClass: .regular)
        return UITraitCollection(traitsFrom: [realTraits, lieTrait])
    }
}
