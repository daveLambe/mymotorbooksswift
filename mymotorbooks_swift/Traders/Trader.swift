//
//  Trader.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/4/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import Foundation

struct Traders: Decodable, MMBAPIResponse {
    typealias T = Trader

    let results: [Trader]
    let count: Int
    let next: String?
    let previous: String?
}

struct Trader: Codable, Searchable {
    let pk: Int
    let objOwner: Int
    let name: String
    let email: String?
    let addressLine1: String?
    let addressLine2: String?
    let addressLine3: String?
    let addressLine4: String?
    let country: String
    let isSupplier: Bool
    let isCustomer: Bool

    var query: String { return self.name }

    init(pk: Int, objOwner: Int, name: String, email: String?, addressLine1: String?, addressLine2: String?, addressLine3: String?, addressLine4: String?, country: String, isSupplier: Bool, isCustomer: Bool) {
        self.pk = pk
        self.objOwner = objOwner
        self.name = name
        self.email = email
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.addressLine3 = addressLine3
        self.addressLine4 = addressLine4
        self.country = country
        self.isSupplier = isSupplier
        self.isCustomer = isCustomer
    }
}
