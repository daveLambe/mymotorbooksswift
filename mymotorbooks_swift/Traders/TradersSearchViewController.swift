//
//  TradersSearchViewController.swift
//  mymotorbooks_swift
//
//  Created by David Lambe on 6/19/22.
//  Copyright © 2022 Dave Lambe. All rights reserved.
//

import UIKit

final class TradersSearchViewController: MMBTableViewSearchController<TraderCell, Trader> {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 55
        navigationItem.title = "Contacts"
        fetchTraders()
    }

    func fetchTraders() {
        MMBApiService.shared.fetchTraders() { [weak self] (result: Result<Traders, MMBApiService.MMBApiServiceError>) in
            switch result {
            case .success(let tradersResult):
                print("<<<< Successfully fetched Traders! >>>>")
                self?.models = tradersResult.results
            case .failure(let error):
                print("<<<< Error fetching Traders: \(error.localizedDescription) >>>>")
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
