//
//  TraderCell.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/4/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class TraderCell: BaseTableViewCell<Trader> {
    static let reuseIdentifier = MyMotorBooksCellIdentifier.trader.rawValue

    override var item: Trader? {
        didSet {
            guard let trader = item else { return }
            nameLabel.text = "\(trader.name)"
            emailLabel.text = trader.email != nil ? "\(trader.email!)" : "Email address unknown"
            countryLabel.text = "\(trader.country)"
        }
    }

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let emailLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let countryLabel: UILabel = {
        let label = UILabel()
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let traderImageView: UIImageView = {
        let imageView = UIImageView()
        let defaultImage = UIImage(systemName: "book.fill")
        imageView.image = defaultImage
        imageView.contentMode = .scaleAspectFit
        imageView.setCorner(radius: 10)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let cellSeperator: UIView = {
        let seperatorView = UIView()
        seperatorView.backgroundColor = MyMotorBooksColor.pink.rawValue
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        return seperatorView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = MyMotorBooksColor.black.rawValue
        contentView.addSubview(traderImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(emailLabel)
        contentView.addSubview(countryLabel)
        contentView.addSubview(cellSeperator)
        configureLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureLayout() {

        NSLayoutConstraint.activate([
            traderImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            traderImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
            traderImageView.heightAnchor.constraint(equalToConstant: 45),
            traderImageView.widthAnchor.constraint(equalToConstant: 45),

            nameLabel.leadingAnchor.constraint(equalTo: traderImageView.trailingAnchor, constant: 4),
            nameLabel.centerYAnchor.constraint(equalTo: traderImageView.centerYAnchor, constant: -10),

            emailLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),
            emailLabel.centerYAnchor.constraint(equalTo: traderImageView.centerYAnchor, constant: 10),

            countryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),
            countryLabel.centerYAnchor.constraint(equalTo: traderImageView.centerYAnchor),

            cellSeperator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left),
            cellSeperator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -self.separatorInset.right),
            cellSeperator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            cellSeperator.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
}
