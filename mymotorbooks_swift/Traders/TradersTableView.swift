//
//  TradersTableView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 11/11/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class TradersTableView: UITableView {
    private(set) var datasource: MMBTableViewDataSource<TraderCell, Trader>?
    public var models: [Trader] = [] {
        didSet {
            DispatchQueue.main.async {
                self.datasource = MMBTableViewDataSource(models: self.models, configureCell: { cell, model in
                    cell.item = model
                    return cell
                })
                self.dataSource = self.datasource
                self.reloadData()
            }
        }
    }

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = 55
        self.register(TraderCell.self)
        self.reloadData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
