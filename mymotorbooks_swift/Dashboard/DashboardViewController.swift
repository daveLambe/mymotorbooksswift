//
//  DashboardViewController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/8/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    let vehiclesInStockView = DashboardStatView(statTitle: DashboardStatTitle.vehiclesInStock.rawValue)
    let vehiclesOver30DaysView = DashboardStatView(statTitle: DashboardStatTitle.over30DaysInStock.rawValue)
    let vehiclesOver60DaysView = DashboardStatView(statTitle: DashboardStatTitle.over60DaysInStock.rawValue)
    let totalDaysView = DashboardStatView(statTitle: DashboardStatTitle.totalDaysInStock.rawValue)
    let saleAgreedView = DashboardStatView(statTitle: DashboardStatTitle.vehiclesSaleAgreed.rawValue)
    let depositsTakenView = DashboardStatView(statTitle: DashboardStatTitle.vehiclesDepositTaken.rawValue)

    let stackViewOfStackViews: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 20
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    let leftStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 70
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    let rightStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 70
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyMotorBooksColor.black.rawValue
        navigationItem.title = "Dashboard"
        let optionsButton = UIBarButtonItem(image: UIImage(systemName: "wrench"), style: .plain, target: self, action: #selector(optionsPressed))
        optionsButton.tintColor = .white
        navigationItem.rightBarButtonItem = optionsButton

        leftStackView.addArrangedSubview(vehiclesInStockView)
        leftStackView.addArrangedSubview(vehiclesOver30DaysView)
        leftStackView.addArrangedSubview(vehiclesOver60DaysView)

        rightStackView.addArrangedSubview(totalDaysView)
        rightStackView.addArrangedSubview(saleAgreedView)
        rightStackView.addArrangedSubview(depositsTakenView)

        stackViewOfStackViews.addArrangedSubview(leftStackView)
        stackViewOfStackViews.addArrangedSubview(rightStackView)

        view.addSubview(stackViewOfStackViews)

        fetchStats()
        setupLayout()
    }

    private func setStatViewValuesWith(_ stats: VehicleStats) {
        vehiclesInStockView.setValueTo(stats.vehiclesInStock)
        vehiclesOver30DaysView.setValueTo(stats.numberOver30Days)
        vehiclesOver60DaysView.setValueTo(stats.numberOver60Days)
        totalDaysView.setValueTo(stats.totalDaysInStock)
        saleAgreedView.setValueTo(stats.vehiclesSaleAgreed)
        depositsTakenView.setValueTo(stats.vehiclesDepositTaken)
    }

    func fetchStats() {
        MMBApiService.shared.fetchVehicleStats() { [weak self]
            (statsResult) -> Void in
            switch statsResult {
                case let .success(stats):
                    print("<<<< Successfully got Vehicle Stats in DashboardViewController >>>>")
                    self?.setStatViewValuesWith(stats)
                case let .failure(error):
                    print("<<<< Failed to get Vehicle Stats with error: \(error) >>>>")
            }
        }
    }

    @objc func optionsPressed() {
        let optionsVC = OptionsViewController()
        optionsVC.modalPresentationStyle = .fullScreen
        show(optionsVC, sender: self)
    }

    func setupLayout() {
        NSLayoutConstraint.activate([
            stackViewOfStackViews.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20),
            stackViewOfStackViews.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            stackViewOfStackViews.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            stackViewOfStackViews.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
        ])
    }
}
