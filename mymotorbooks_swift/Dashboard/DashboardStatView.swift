//
//  DashboardStatView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/11/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class DashboardStatView: UIView {
    var statTitle: String
    let titleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
                               NSAttributedString.Key.foregroundColor: UIColor.white]
    let valueTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),
                               NSAttributedString.Key.foregroundColor: MyMotorBooksColor.green.rawValue]

    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    required init(statTitle: String) {
        self.statTitle = statTitle
        super.init(frame: .zero)
        initCommon()
    }

    func initCommon() {
        self.addSubview(titleLabel)
        self.addSubview(valueLabel)
        self.backgroundColor = MyMotorBooksColor.black.rawValue
        self.setBorder(width: 4, color: MyMotorBooksColor.pink.rawValue)
        self.setShadow(color: UIColor.white, radius: 8)
        self.titleLabel.attributedText = statTitle.asAttributedText(withAttributes: titleTextAttributes)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureLayout()
    }

    public func setValueTo(_ value: Int) {
        valueLabel.attributedText = String(value).asAttributedText(withAttributes: valueTextAttributes)
    }

    func configureLayout() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            titleLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 2),
            titleLabel.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: -2),

            valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            valueLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 2),
            valueLabel.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: -2),
        ])
    }
}
