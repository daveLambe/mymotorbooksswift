//
//  OptionsViewController.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/12/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {
    var userDetailsStack = UserDetailsStackView()
    var companyDetailsStack = CompanyDetailsStackView()
    var user: User? {
        didSet {
            userDetailsStack.user = user
            companyDetailsStack.user = user
        }
    }

    let userLabel: UILabel = {
        let label = UILabel()
        label.text = "User"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.textAlignment = .center
        label.backgroundColor = MyMotorBooksColor.black.rawValue
        return label
    }()

    let companyLabel: UILabel = {
        let label = UILabel()
        label.text = "Company"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = MyMotorBooksColor.pink.rawValue
        label.textAlignment = .center
        label.backgroundColor = MyMotorBooksColor.black.rawValue
        return label
    }()

    let optionsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 30
        stack.distribution = .fill
        stack.backgroundColor = MyMotorBooksColor.black.rawValue
        return stack
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = MyMotorBooksColor.black.rawValue
        navigationItem.title = "Options"
        let logOutButton = UIBarButtonItem(title: "Log out", style: .done, target: self, action: #selector(logOut))
        logOutButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = logOutButton

        optionsStackView.addArrangedSubview(userLabel)
        optionsStackView.addArrangedSubview(userDetailsStack)
        optionsStackView.addArrangedSubview(companyLabel)
        optionsStackView.addArrangedSubview(companyDetailsStack)
        view.addSubview(optionsStackView)
        fetchUsers()
        configureLayout()
    }

    @objc private func logOut() {
        let logOutAlert = UIAlertController(title: "Log out?", message: "Are you sure you want to log out?", preferredStyle: .alert)
        let logOutButton = UIAlertAction(title: "Log Out", style: .destructive, handler: { _ in
            AuthManager.shared.logOut()
        })
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        logOutAlert.addAction(logOutButton)
        logOutAlert.addAction(cancelButton)
        present(logOutAlert, animated: true)
    }

    func fetchUsers() {
        MMBApiService.shared.fetchUser() { [weak self]
            (userResult) -> Void in
            switch userResult {
                case let .success(user):
                    self?.user = user
                case let .failure(error):
                    print("<<<< Error fetching User!: \(error) >>>>")
            }
        }
    }

    func configureLayout() {
        optionsStackView.translatesAutoresizingMaskIntoConstraints = false
        let topTextField = userDetailsStack.emailStack.textField

        NSLayoutConstraint.activate([
            optionsStackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
            optionsStackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            optionsStackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            optionsStackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20),


            topTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 150),
            userDetailsStack.firstNameStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            userDetailsStack.lastNameStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.nameStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.addressLine1Stack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.addressLine2Stack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.addressLine3Stack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.addressLine4Stack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.vatNumberStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.bankAccountNumberStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
            companyDetailsStack.emailStack.textField.leadingAnchor.constraint(equalTo: topTextField.leadingAnchor),
        ])
    }
}
