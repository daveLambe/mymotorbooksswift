//
//  OptionsTextField.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/18/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class OptionsTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: .zero)
        textColor = .white
        self.backgroundColor = MyMotorBooksColor.black.rawValue
        setBorder(width: 2, color: MyMotorBooksColor.pink.rawValue)
        addPadding(.left(5))
        self.setContentCompressionResistancePriority(UILayoutPriority.init(rawValue: 749), for: NSLayoutConstraint.Axis.horizontal)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        self.layer.borderWidth = 8
        return true
    }

    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        self.layer.borderWidth = 2
        return true
    }
}
