//
//  OptionsDetailStackView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/18/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class OptionsDetailStackView: UIStackView {
    var textField = OptionsTextField()
    var labelText: String

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    required init(labelText: String) {
        self.labelText = labelText
        super.init(frame: .zero)
        initCommon()
    }

    func initCommon() {
        axis = .horizontal
        spacing = 20
        let label = UILabel(text: "\(labelText):", textColor: .white)
        addArrangedSubview(label)
        addArrangedSubview(textField)
    }
}

extension UILabel {
    fileprivate convenience init(text: String = "Unknown", textColor: UIColor = .white) {
        self.init()
        self.textColor = textColor
        self.text = text
        font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)
        setContentHuggingPriority(UILayoutPriority.defaultHigh, for: NSLayoutConstraint.Axis.horizontal)
    }
}
