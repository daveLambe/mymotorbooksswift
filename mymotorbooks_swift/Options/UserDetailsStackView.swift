//
//  UserDetailsView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/18/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class UserDetailsStackView: UIStackView {
    var userImage: UIImageView
    var emailStack: OptionsDetailStackView
    var firstNameStack: OptionsDetailStackView
    var lastNameStack: OptionsDetailStackView
    var user: User? {
        didSet {
            guard let _user = user else { return }
            updateTextFields(forUser: _user)
        }
    }

    let userImageView: UIImageView = {
        let frame = CGRect(x: 1, y: 1, width: 1, height: 1)
        let imageView = UIImageView(frame: frame)
        imageView.image = UIImage(systemName: "person.circle.fill")
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = MyMotorBooksColor.pink.rawValue
        imageView.setContentHuggingPriority(UILayoutPriority(248), for: NSLayoutConstraint.Axis.vertical)
        imageView.setContentCompressionResistancePriority(UILayoutPriority(749), for: NSLayoutConstraint.Axis.vertical)
        return imageView
    }()

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    required init() {
        userImage = userImageView
        emailStack = OptionsDetailStackView(labelText: "Email")
        firstNameStack = OptionsDetailStackView(labelText: "First Name")
        lastNameStack = OptionsDetailStackView(labelText: "Last Name")
        super.init(frame: .zero)
        initCommon()
    }

    func initCommon() {
        axis = .vertical
        spacing = 10
        backgroundColor = MyMotorBooksColor.black.rawValue
        addArrangedSubview(userImageView)
        addArrangedSubview(emailStack)
        addArrangedSubview(firstNameStack)
        addArrangedSubview(lastNameStack)
    }

    private func updateTextFields(forUser user: User) {
        emailStack.textField.text = user.email
        firstNameStack.textField.text = user.firstName
        lastNameStack.textField.text = user.lastName
    }
}
