//
//  CompanyDetailsView.swift
//  mymotorbooks_swift
//
//  Created by Dave Lambe on 7/18/20.
//  Copyright © 2020 Dave Lambe. All rights reserved.
//

import UIKit

class CompanyDetailsStackView: UIStackView {
    var nameStack: OptionsDetailStackView
    var addressLine1Stack: OptionsDetailStackView
    var addressLine2Stack: OptionsDetailStackView
    var addressLine3Stack: OptionsDetailStackView
    var addressLine4Stack: OptionsDetailStackView
    var vatNumberStack: OptionsDetailStackView
    var bankAccountNumberStack: OptionsDetailStackView
    var emailStack: OptionsDetailStackView
    var user: User? {
        didSet {
            guard let _user = user else { return }
            updateTextFieldText(forUser: _user)
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    required init() {
        nameStack = OptionsDetailStackView(labelText: "Name")
        addressLine1Stack = OptionsDetailStackView(labelText: "Address Line 1")
        addressLine2Stack = OptionsDetailStackView(labelText: "Address Line 2")
        addressLine3Stack = OptionsDetailStackView(labelText: "Address Line 3")
        addressLine4Stack = OptionsDetailStackView(labelText: "Address Line 4")
        vatNumberStack = OptionsDetailStackView(labelText: "VAT Number")
        bankAccountNumberStack = OptionsDetailStackView(labelText: "Bank Acc Num")
        emailStack = OptionsDetailStackView(labelText: "Email")
        super.init(frame: .zero)
        initCommon()
    }

    func initCommon() {
        axis = .vertical
        spacing = 10
        backgroundColor = MyMotorBooksColor.black.rawValue
        addArrangedSubview(nameStack)
        addArrangedSubview(addressLine1Stack)
        addArrangedSubview(addressLine2Stack)
        addArrangedSubview(addressLine3Stack)
        addArrangedSubview(addressLine4Stack)
        addArrangedSubview(vatNumberStack)
        addArrangedSubview(bankAccountNumberStack)
        addArrangedSubview(emailStack)
    }

    private func updateTextFieldText(forUser user: User) {
        nameStack.textField.text = user.company.name
        addressLine1Stack.textField.text = user.company.addressLine1
        addressLine2Stack.textField.text = user.company.addressLine2
        addressLine3Stack.textField.text = user.company.addressLine3
        addressLine4Stack.textField.text = user.company.addressLine4
        vatNumberStack.textField.text = user.company.vatNumber
        bankAccountNumberStack.textField.text = user.company.bankAccountNumber
        emailStack.textField.text = user.company.email
    }
}
